<?php
/**
 * Template Name: New to Brew
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package byo
 */

get_header(); ?>

<div  class="col-xs-12">
	<div  class="row">
		<header class="entry-header">
			<?php get_template_part( 'template-parts/fragment', 'ad-units-sponsor-newbrew'); ?>
		</header>
	</div>
</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main col-xs-12 col-md-8 col-lg-9">


			<div class="row">


				<?php 	if ( have_posts() ) : the_post(); ?>
					<div class="col-xs-12">
						<h1><?php the_title() ?></h1>
						<?php echo wpautop( get_the_content()); ?>
					</div>
			<?php endif; ?>

				<div class="col-xs-12">
					<h4>More New to Brew</h4>
					<?php

					$args['tax_query'] = array(
						array(
							'taxonomy' => 'topic',
							'field' => 'slug',
							'terms' => 'new-to-brew',
						)
					);

					$args['nopaging'] = true;

					$query = new WP_Query( $args );

					// Pagination fix
					global $wp_query;
					$temp_query = $wp_query;
					$wp_query   = $query;
					?>
					<?php

					while ( $query->have_posts() ) : $query->the_post();

						get_template_part( 'template-parts/content', 'article-list' );

					endwhile; // End of the loop.

					// Reset main query object
					$wp_query = $temp_query;

					/* Restore original Post Data */
					wp_reset_postdata();

					?>
				</div>

		</div>

		</main><!-- #main -->
		<div class="col-xs-12 col-md-4 col-lg-3">
			<div class="ad-units">
				<?php get_template_part( 'template-parts/fragment', 'ad-units-right-rail'); ?>
			</div>
		</div>
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
