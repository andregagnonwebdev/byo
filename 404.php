<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package byo
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main col-xs-12 col-md-8">

			<section class="error-404 not-found">
				<header class="page-headerx">
					<h1 class="page-title"><?php esc_html_e( 'Page not found.', 'byo' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php esc_html_e( 'The page or file you are trying to access does not exist, much like the Yeti.', 'byo' ); ?></p>
					<p><?php esc_html_e( 'Maybe try  a search?', 'byo' ); ?></p>

					<?php
						get_search_form();

					?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
		<div class="col-xs-12 col-md-4">
				<img src="<?php echo get_field('image_for_404_page', 'option') ?>" />
		</div>
	</div><!-- #primary -->

<?php
get_footer();
