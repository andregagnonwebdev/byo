<?php
/**
 * Template Name: Free Trial
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main col-xs-12 col-md-12">
			<div class="row">

			<?php if ( 1 ||  byo_has_subscription() == false ): ?>
				<div class="col-xs-12 col-md-12">
					<?php if ( byo_user_logged_in_product_already_bought()): ?>
						<?php	echo do_shortcode( get_field( 'payup_2', 'option'));	?>
					<?php else: ?>
						<?php	echo do_shortcode( get_field( 'free_trial_2', 'option'));	?>
					<?php endif; ?>
				</div>
			<?php endif; ?>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
