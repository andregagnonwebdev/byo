<?php
/*
 * Search form
 *
 * @package mind
 */

$search_post_type = '';

if (!empty($post_type)) {
  $search_post_type = $post_type;
}
elseif (!empty($_GET['post_type'])) {
  $search_post_type = $_GET['post_type'];
}

?>
<div class="form-page swg-search-form-wrapper">
  <form id="swg-browse-search-main-search-form" class="form-inline search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="form-group">
      <span class="screen-reader-text">Search for:</span>
      <input type="search" class="form-control search-field" id="search" placeholder="What are you looking for? Search here" value="<?php echo is_search()? get_search_query() : ''; ?>" name="s">
      
      <?php if (!empty($search_post_type)): ?>
        <input type="hidden" name="post_type" value="<?php echo $search_post_type; ?>">
      <?php endif; ?>
      
      <!-- sort by -->
      <input type="hidden" class="swg-orderby-auto-pop" name="orderby" value="relevance" />
    </div><!-- /.form-group -->
    
    <button type="submit" class="btn btn-default search-submit"><i class="fa fa-search"></i> SEARCH </button>
  </form>
  
  <div class="swg-browse-button"><a href="<?php echo get_permalink(143); ?>" class="browse-button">Index</a></div>
</div><!-- /.form-page -->
