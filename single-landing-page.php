<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package SWG
 */

?>

<?php
$secure = get_field( 'log_in_required');
$this_page = get_permalink();

if ( $secure && !is_user_logged_in() && ! is_page( 'login' )) {
	//redirect
	wp_safe_redirect( wp_login_url( $this_page) );
}
else {
	// add this_page url to redirect_to for Lost Password
	//add_filter( 'lostpassword_redirect', 'byo_lostpassword_filter', 10);

}
// remove coupon in landing page.
remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );

get_header( 'landing-page' );
?>

<?php

function byo_lostpassword_filter ( $redirect_to) {
	//var_dump( $redirect_to);
	return $redirect_to;
}

function byo_landing_page_apply_discount( $coupon_code) {
	// Apply the coupon to the cart if necessary.
	if ( $coupon_code && ! WC()->cart->has_discount( $coupon_code ) ) {
		// WC_Cart::add_discount() sanitizes the coupon code.
		 WC()->cart->add_discount( $coupon_code );
		 wc_print_notices();
	 }
}

function byo_landing_page_add_product_to_cart( $product_id, $coupon_code ) {
	if ( ! is_admin() ) {
		$found = false;
		$cart = WC()->cart;
		if ( $cart ) {
			$cart->empty_cart();
			$size = 0;
			// $size = sizeof( $cart->get_cart() );
			// //check if product already in cart
			// if ( $size > 0 ) {
			// 	foreach ( $cart->get_cart() as $cart_item_key => $values ) {
			// 		//var_dump( $values);
			// 		$this_id = $values['product_id'];
			// 		if ( $this_id == $product_id )
			// 			$found = true;
			// 	}
			// }

			if ( $size == 0 || ! $found ) {
				// add product
				$cart->add_to_cart( $product_id );
				// Calculate totals
				$cart->calculate_totals();
				// Save cart to session
				$cart->set_session();
				// Maybe set cart cookies
				$cart->maybe_set_cart_cookies();
			}
		}


    // wc_clear_notices();
	}
}


?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main col-xs-12 col-md-12">

			<div class="row">
				<div class="col-xs-12 col-md-12">
					<?php 	if ( have_posts() ) : the_post(); ?>

					<div class="landing-page">
						<?php get_template_part( 'template-parts/content', 'landing-page' ); ?>
					</div>


					<div>
						<?php
						// add
						global $byo_product_id;
						global $byo_shortcode;
						global $byo_coupon;

						?>
						<!-- <p><?php echo 'product id: '. $byo_product_id; ?></p>
						<p><?php echo 'coupon: '. $byo_coupon; ?></p>
						<p><?php echo 'shortcode id: '. $byo_shortcode; ?></p> -->
						<?php
							if ( $byo_product_id) {
								if ( 1) {
									byo_landing_page_add_product_to_cart( $byo_product_id, $byo_coupon);
									byo_landing_page_apply_discount( $byo_coupon);
								}
								else {
									//byo_landing_page_add_product_to_cart( $byo_product_id, $byo_coupon);
									$checkout = do_shortcode( '[woocommerce_one_page_checkout product_ids="69708"]' );
									echo $checkout;
								}
							}
							else if ( $byo_shortcode)
								echo do_shortcode( $byo_shortcode );

							?>
					</div>

				<?php endif; ?>

				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer( 'landing-page' );
// $a = new WC_AJAX;
// $_POST[ 'coupon_code'] = 'janpromo10';
// $a->apply_coupon();
