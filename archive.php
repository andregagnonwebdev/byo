<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package byo
 */

get_header(); ?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main col-xs-12 col-md-12">

		<?php
		if ( have_posts() ) : ?>
			<div class="row">
			<div class="col-xs-12">
				<header class="page-headerx">
					<h1><?php echo post_type_archive_title( '', false );?></h1>
					<?php //the_archive_title( '<h1 class="page-title">', '</h1>' ); ?>
					<?php if (is_tax()) {
						$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
						echo '<h2>'.ucwords($term->taxonomy).': '.$term->name.'</h2>';
					}
					?>
					<?php	the_archive_description( '<div class="archive-description">', '</div>' );	?>

				</header><!-- .page-header -->
			</div>
			<div class="col-xs-12 col-md-12 search-pagination">
				<?php
					global $wp_query;
					echo $wp_query->found_posts.' result(s) found.';
				?>
				<?php
				if ( function_exists('byo_wp_bootstrap_pagination') )
					byo_wp_bootstrap_pagination();
				?>
			</div>
			<div class="col-xs-12 col-md-12">

				<?php
				/* Start the Loop */
				while ( have_posts() ) : the_post();

					// set up yelow tag
					global $post_type;
					if ( $post->post_type != 'mr-wizard') {
						$post_type = 'all';
					} else
						$post_type = '';
					get_template_part( 'template-parts/content', 'search' );

				endwhile;

				// the_posts_navigation(array( 'prev_text' =>'More', 'next_text' => 'Previous'));
				global $wp_query;
				echo $wp_query->found_posts.' result(s) found.';
				if ( function_exists('byo_wp_bootstrap_pagination') )
					byo_wp_bootstrap_pagination();
				?>
			</div>
			<?php endif; ?>
		</div> <!-- end row -->
		</main><!-- #main -->

		<div class="col-xs-12 col-md-3 hidden">
			<div class="search-ad-units ad-units">
				<?php //get_template_part( 'template-parts/fragment', 'ad-units-right-rail'); ?>
			</div>
		</div>

	</div><!-- #primary -->


<?php
//get_sidebar();
get_footer();
