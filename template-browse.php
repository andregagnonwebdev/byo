<?php
/**
 * Template Name: Browse
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

get_header(); ?>

<div class="swg-spinner">
  <div class="rect3"></div>
  <div class="rect2"></div>
  <div class="rect1"></div>
</div>

<div id="primary" class="content-area">
	<main id="main" class="site-main col-xs-12 col-md-12">
		<?php get_template_part( 'searchform', 'all' ); ?>
		<?php $is_browse_page = TRUE; ?>
		<?php include_once(get_template_directory() . '/swg-browse-search/includes/swg-icons.php'); ?>
    
		<?php get_template_part( 'template-parts/content', 'browse' ); ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
