<?php
/**
 * BYO functions and definitions
 *
 * @package BYO
 */

define( 'LOCAL_IP_ADDR', '10.0.2.15');
define( 'DISALLOW_FILE_EDIT', true);

include_once(get_template_directory() . '/swg-browse-search/browse-search-functions.php');


if ( ! function_exists( 'byo_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function byo_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on BYO, use a find and replace
	 * to change 'byo' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'byo', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'article-thumb', 300, 250, array( 'left', 'top'));

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'mobile' => __( 'Mobile Menu', 'byo' ),
		'tophat-left' => __( 'TopHat Left Menu', 'byo' ),
		'tophat-right' => __( 'TopHat Right Menu', 'byo' ),
		'primary-left' => __( 'Primary Menu Left', 'byo' ),
		'primary-right' => __( 'Primary Menu Right', 'byo' ),
		'footer-contact' => __( 'Footer Menu Contact', 'byo' ),
		'footer-navigate' => __( 'Footer Menu Navigate', 'byo' ),
		'footer-subscribers' => __( 'Footer Menu Subscribers', 'byo' ),
		'footer-advertisers' => __( 'Footer Menu Advertisers', 'byo' ),
		'footer-bottom' => __( 'Footer Menu Bottom', 'byo' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'byo_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// custom logo
	add_theme_support( 'custom-logo', array(
  	'width'       => 245,
		'height'      => 120,
	) );
}
endif; // byo_setup
add_action( 'after_setup_theme', 'byo_setup' );


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
	 * @global int $content_width
	 */
	function byo_content_width() {
		$GLOBALS['content_width'] = apply_filters( 'byo_content_width', 640 );
	}
	add_action( 'after_setup_theme', 'byo_content_width', 0 );


/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function byo_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'byo' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'byo_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function byo_scripts() {

	if ( WP_DEBUG)
		$version = '4'.date( '.YmdGi'); // for DEBUG
	else
		$version = null;

	// WP required CSS
  wp_enqueue_style( 'byo-style', get_stylesheet_uri(), array(), $version ); // style.css

	// bootstrap css framework
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css-dist/bootstrap.min.css', array(), $version);
	wp_enqueue_style( 'font-awesome', 'https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), null);
	// site CSS
	$css = (  WP_DEBUG) ? '/css/main.css' : '/css-dist/main.min.css';
	wp_enqueue_style( 'main', get_template_directory_uri() . $css, array( 'byo-style', 'bootstrap'), $version);

	$css = ( WP_DEBUG) ? '/css/comps.css' : '/css-dist/comps.min.css';
	wp_enqueue_style( 'comps', get_template_directory_uri() . $css, array( 'byo-style', 'bootstrap'), $version);


	// load google fonts

	$fonts = byo_get_theme_fonts();
	global $byo_all_fonts;
	foreach ($fonts as $f) {
		if ( array_key_exists( $f, $byo_all_fonts) && $byo_all_fonts[ $f][ 1]) {
			$l = 'https://fonts.googleapis.com/css?family=';
			global $byo_all_fonts;
			$l .= str_replace( ' ', '+', $f) . ':' . $byo_all_fonts[ $f][ 1];
			$l .= '';
			wp_enqueue_style( 'google-font-'.str_replace( ' ', '-', $f), $l, array( 'byo-style', 'bootstrap'), null);
		}
	}

  	// move jquery to footer
  if (!is_admin()) {
        wp_deregister_script('jquery');
        // Load the copy of jQuery that comes with WordPress in to footer
        wp_register_script('jquery', '/wp-includes/js/jquery/jquery.js', false, $version, true);
        wp_enqueue_script('jquery');
	}

	// for watch in Gruntfile.js
	if ( WP_DEBUG && ( in_array( $_SERVER['SERVER_ADDR'], array( '127.0.0.1', LOCAL_IP_ADDR)) || pathinfo($_SERVER['SERVER_NAME'], PATHINFO_EXTENSION) == 'test')) {
    	wp_enqueue_script( 'livereload', '//localhost:35729/livereload.js', '', false, true );
	}

	// use minified js
	$js = ( WP_DEBUG) ? 'bootstrap.js' : 'bootstrap.min.js';
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/'.$js, array('jquery'), $version, true);

	$js = ( WP_DEBUG) ? '/js/theme.js' : '/js/dist/theme.min.js';
  wp_enqueue_script( 'byo-theme-js', get_template_directory_uri() . $js, array('jquery'), $version, true );

	// new fontawesome
	//wp_enqueue_script( 'font-awesome-js', 'https://use.fontawesome.com/c88288cbb6.js', array(), $version, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}
add_action( 'wp_enqueue_scripts', 'byo_scripts' );

/**
 * Remove emoji support
 */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
//require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
//require get_template_directory() . '/inc/jetpack.php';
/**
 * Custom google fonts
 */
//require get_template_directory() . '/inc/google-font.php';

/**
 * Custom theme options
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Custom theme options
 */
//require get_template_directory() . '/inc/custom-theme-options.php';

/**
 * Utility functions
 */
// set up this global for some functions
// $sep = '/';
// $dl_filepath = dirname(dirname(dirname(dirname(__FILE__)))) . $sep . 'wp-content' . $sep . 'uploads';
// require get_template_directory() . '/inc/util.php';

/**
 * Custom post types
 */
//require get_template_directory() . '/post-types/video.php';

/**
 * Custom Log in
*/
function byo_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo-brew-your-own.png);
		height:65px;
		width:320px;
		height:121px;
		width:245px;
		background-size: 245px 121px;
		background-repeat: no-repeat;
				padding-bottom: 0px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'byo_login_logo' );

function byo_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'byo_login_logo_url' );

function byo_login_logo_url_title() {
    return 'Brew Your Own';
}
add_filter( 'login_headertitle', 'byo_login_logo_url_title' );

/**
 * ACF
 *
 */
 if( function_exists('acf_add_options_page') ) {
	 $args = array(

 	/* (string) The title displayed on the options page. Required. */
 	'page_title' => 'Options',

 	/* (string) The title displayed in the wp-admin sidebar. Defaults to page_title */
 	// 'menu_title' => '',

 	/* (string) The slug name to refer to this menu by (should be unique for this menu).
 	Defaults to a url friendly version of menu_slug */
 	 'menu_slug' => 'acf-options',

 	/* (string) The capability required for this menu to be displayed to the user. Defaults to edit_posts.
 	Read more about capability here: http://codex.wordpress.org/Roles_and_Capabilities */
 	'capability' => 'edit_posts',

 	/* (int|string) The position in the menu order this menu should appear.
 	WARNING: if two menu items use the same position attribute, one of the items may be overwritten so that only one item displays!
 	Risk of conflict can be reduced by using decimal instead of integer values, e.g. '63.3' instead of 63 (must use quotes).
 	Defaults to bottom of utility menu items */
 	'position' => 40,

 	/* (string) The slug of another WP admin page. if set, this will become a child page. */
 	'parent_slug' => '',

 	/* (string) The icon class for this menu. Defaults to default WordPress gear.
 	Read more about dashicons here: https://developer.wordpress.org/resource/dashicons/ */
 	'icon_url' => false,

 	/* (boolean) If set to true, this options page will redirect to the first child page (if a child page exists).
 	If set to false, this parent page will appear alongside any child pages. Defaults to true */
 	'redirect' => true,

 	/* (int|string) The '$post_id' to save/load data to/from. Can be set to a numeric post ID (123), or a string ('user_2').
 	Defaults to 'options'. Added in v5.2.7 */
 	'post_id' => 'options',

 	/* (boolean)  Whether to load the option (values saved from this options page) when WordPress starts up.
 	Defaults to false. Added in v5.2.8. */
 	'autoload' => false,

 );
 	acf_add_options_page( $args);
// 	acf_add_options_page();
 }

add_filter('body_class','byo_body_class_names');
function byo_body_class_names( $classes ) {
    if (! ( is_user_logged_in() ) ) {
        $classes[] = 'logged-out';
    }

		if ( byo_has_subscription() ) {
				$classes[] = 'subscribed';
		}
		if ( $status = byo_custom_membership_status()) {
			$classes[] = $status;

		}


    return $classes;
}

function byo_custom_membership_status() {

	// bail if Memberships isn't active
	if ( ! function_exists( 'wc_memberships' ) ) {
					return '';
	}

	// Get current user ID
	$user_id = get_current_user_id();
	$m = wc_memberships_get_user_membership( $user_id);
	$args = array(
			'status' => array( 'active', 'complimentary', 'pending', 'paused', 'expired', 'cancelled' ),
	);
	$memberships = wc_memberships_get_user_memberships( $user_id, $args);

	if ( ! empty( $memberships ) ) {
		// do something for this active member
		switch ( $memberships[0]->status) {
			case 'wcm-active':
			case 'wcm-complimentary':
			case 'wcm-pending':
			case 'wcm-paused':
				$status = 'active';
			break;
			case 'wcm-cancelled':
			case 'wcm-expired':
				$status = 'expired';
			break;
			default:
				$status = '';
				break;
		}

		return $status;
	}

}
/******************************************************************************
 * WooCommerce Support
 */

 add_action( 'after_setup_theme', 'byo_woocommerce_support' );
 function byo_woocommerce_support() {
     add_theme_support( 'woocommerce' );
 }

// add_action( 'init', 'byo_woocommerce_search_exclude', 99 );
 add_action( 'pre_get_posts', 'byo_woocommerce_search_exclude', 99 );
 /**
  * update_my_custom_type
  */
 function byo_woocommerce_search_exclude( $q) {
 	global $wp_post_types;

 	if ( post_type_exists( 'product' ) ) {
 		// exclude from search results
		if ( is_search())
 			$wp_post_types['product']->exclude_from_search = true;
 	}
	// exclude product category (advertising)
	// if ( is_shop() &&  ! is_admin() )	{
	// 	$q->set( 'tax_query', array( array(
	// 	'taxonomy' => 'product_cat',
	// 	'field' => 'slug',
	// 	'terms' => 'advertising',
	// 	'operator' => 'NOT IN'
	// 	)));
	// }
 }

 add_action( 'woocommerce_product_query', 'byo_wc_prefix_custom_pre_get_posts_query' );
/**
 * Hide Product Cateories from targetted pages in WooCommerce
 * @link https://gist.github.com/stuartduff/bd149e81d80291a16d4d3968e68eb9f8#file-wc-exclude-product-category-from-shop-page-php
 *
 */
function byo_wc_prefix_custom_pre_get_posts_query( $q ) {

	if( is_shop()  ) { // set conditions here

	    $tax_query = (array) $q->get( 'tax_query' );

	    $tax_query[] = array(
	           'taxonomy' => 'product_cat',
	           'field'    => 'slug',
	           'terms'    => array( 'advertising', '2017-byo-boot-camp-indianapolis-in' ), // set product categories here
	           'operator' => 'NOT IN'
	    );
//	    $q->set( 'tax_query', $tax_query );
	}
}

add_action( 'woocommerce_before_checkout_form', 'byo_landing_page_checkout', 20 );
function byo_landing_page_checkout( $checkout) {


	//echo 'byo_landing_page_checkout here';
	//var_dump( $checkout);
	global $post;
	//var_dump( $post);
}

/**
 * Pre-populate Woocommerce checkout fields
 */
add_filter('woocommerce_checkout_get_value', function($input, $key ) {

	if ( isset( $_GET['om_email'])) {
		// get email address from OIM
		$email = $_GET['om_email'];

		global $current_user;
		switch ($key) :
			case 'billing_first_name':
			case 'shipping_first_name':
			case 'billing_last_name':
			case 'shipping_last_name':
			case 'billing_phone':
			break;
			case 'billing_email':
				return $email;
				//return $current_user->user_email;
			break;
		endswitch;
	}

}, 10, 2);

//
// Customize WC templates within theme
//

//
// Shop page (archive-product) customizations
//
// remove WC wrapper
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

// add bootstrap wrapper
add_action('woocommerce_before_main_content', 'byo_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'byo_theme_wrapper_end', 10);

function byo_theme_wrapper_start() {
    echo '<div id="primary" class="content-area">';
    echo '<div id="main" class="site-main col-xs-12 col-md-8" role="main">';
}

function byo_theme_wrapper_end() {
    echo '</div>'; // end column
		echo '<div class="col-xs-12 col-md-4">';
		get_sidebar('shop');
		echo '</div>';
    echo '</div>'; // end row
}

// remove titles
remove_action( 'woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10);
remove_action( 'woocommerce_archive_description', 'woocommerce_product_archive_description', 10);

// remove sort order
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
// removes  results
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );

// remove pagination
if ( 1 ||  is_shop())
	remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );
if( function_exists( 'is_product_category') && is_product_category())
	add_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );

// add bootstrap row markup
add_action('woocommerce_before_shop_loop', 'byo_wc_shop_loop_wrapper_start', 10);
function byo_wc_shop_loop_wrapper_start() {
	echo '<div class="clearfix"></div>';
	echo '<div class="row products">';
	$col_count = 1;
}
add_action('woocommerce_after_shop_loop', 'byo_wc_shop_loop_wrapper_end', 10);
function byo_wc_shop_loop_wrapper_end() {
	echo '</div> <!-- end row -->';
}

remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );

//
// shop sidebar: cart w/Ajax
//
add_filter('woocommerce_add_to_cart_fragments', 'byo_woocommerce_header_add_to_cart_fragment');

function byo_woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;

	ob_start();

	?>
	<a class="cart-customlocation" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></a>
	<?php

	$fragments['a.cart-customlocation'] = ob_get_clean();

	return $fragments;

}

// filter shop query

//add_action( 'woocommerce_product_query', 'byo_wc_product_query_on_sale' );

function byo_wc_product_query_on_sale( $q ){


    $product_ids_on_sale = wc_get_product_ids_on_sale();

    $q->set( 'post__in', (array) $product_ids_on_sale );

}

add_filter( 'loop_shop_per_page', 'byo_new_loop_shop_per_page', 20 );

function byo_new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
	if ( is_shop())
  	$cols = 4;
  return $cols;
}


/**
 * Disable autofocus at checkout.
 */
function byo_wc_disable_autofocus_firstname( $fields ) {
	$fields['billing']['billing_first_name']['autofocus'] = false;
	if ( is_array( $fields['shipping'] ))
  $fields['shipping']['shipping_first_name']['autofocus'] = false;

    return $fields;
}

add_filter( 'woocommerce_checkout_fields', 'byo_wc_disable_autofocus_firstname' );



// Nav Menu link for cart

add_filter('woocommerce_add_to_cart_fragments', 'byo_woocommerce_header_add_to_cart_fragment_tophat');
function byo_woocommerce_header_add_to_cart_fragment_tophat( $fragments ) {
	global $woocommerce;

	ob_start();

	?>
	<a class="cart-customlocation-tophat" href="/cart">Cart (<?php echo WC()->cart->get_cart_contents_count(); ?>)</a>
	<?php

	$fragments['a.cart-customlocation-tophat'] = ob_get_clean();

	return $fragments;

}

function byo_add_cart_link( $items, $args ) {
    // Check is user logged in
		if ( $args->theme_location == 'tophat-right' ) {

			if ( defined('WC_VERSION') && ( 1 || is_user_logged_in() )) {
				//global $woocommerce;
				//$total = WC()->cart->get_cart_contents_count();
				$link = '<a class="cart-customlocation-tophat" href="'. wc_get_cart_url() .'">Cart ('. WC()->cart->get_cart_contents_count() .')</a>';

				$items = $items . '<li class="menu-item">'.$link.'</li>';
    	}
		}
		return $items;
}
add_filter( 'wp_nav_menu_items', 'byo_add_cart_link', 10, 2 );



//
// Restricted content rules
//
// WC membership, restricted content rules
function byo_is_restricted( $post_id=null) {
	if ( function_exists( 'wc_memberships_is_post_content_restricted' ) &&
			wc_memberships_is_post_content_restricted( $post_id) === true)
		return( true);
	else {
		return( false);
	}
}
// WC is a member, or an admin
function byo_has_subscription() {
	if ( ( function_exists( 'wc_memberships_is_user_active_member' ) &&
			wc_memberships_is_user_active_member() === true ) ||
			 current_user_can('administrator') )
		return( true);
	else {
		return( false);
	}
}

// Add optinmonster shortcode / form; replace WC message, see CSS
add_filter( 'wc_memberships_content_restricted_message', 'byo_content_restricted_message');
function byo_content_restricted_message( $message) {
	if ( 1 && $f = get_field( 'free_trial_1', 'option')) {
			$f2 = get_field( 'payup_1', 'option');

			if ( byo_user_logged_in_product_already_bought())
				$message = do_shortcode( $f2);
			else
				$message = do_shortcode( $f);

			// Log In link with redirect
			global $post;
			$post_id = $post->ID;
			$message .=
			'<p class="restricted-log-in-message">Already a member? <a href="/my-account/?wcm_redirect_to=post&amp;wcm_redirect_id='.$post_id.'">Log In</a></p>';

	}
	//$message = 'Test';
	return( $message);
}


function byo_wc_memberships_filter_restricted_content( $content, $restricted, $message, $post ) {
	if ( is_search() == false
		&& is_archive() == false
		// && is_page_template( 'template-new-to-brew.php') == false
		// && get_page_template_slug( $post->ID ) != 'template-new-to-brew.php'
		&& true == $restricted && byo_has_subscription() == false) {
		$length = ($post->post_type == 'mr-wizard') ? 100 : 120;
		//$length = ($post->post_type == 'mr-wizard') ? 25 : 12;
		//return wp_trim_words( $content, $length ) . '   ' . $message;
		return get_the_excerpt( $post ) . '   ' . $message;
		return wp_trim_words( $post->post_content, $length ) . '   ' . $message;
	}
	return $content;
}
add_filter( 'wc_memberships_the_restricted_content', 'byo_wc_memberships_filter_restricted_content', 10, 4 );

//
// business logic: don't allow multiple free trial purchaes
//
function byo_user_logged_in_product_already_bought( $product_id=57065) {
	if ( is_user_logged_in() ) {
		global $product;
		$product_id = get_field( 'product_id_free_trial', 'option');
		//$product_id = 44426; // free trial subscription
		$current_user = wp_get_current_user();
		if ( wc_customer_bought_product( $current_user->user_email,
		// $current_user->ID, $product->id ) )
																			$current_user->ID, $product_id ) )
			echo '<div class="user-bought">Hey ' . $current_user->first_name . ', you\'ve already purchased product ID '. $product_id .'</div>';
			return true;
		}
		return false;
}

// remove woocommerce nag message
add_filter( 'woocommerce_helper_suppress_admin_notices', '__return_true' );

// Custom redirect for users after logging in
function byo_wc_login_redirect( $redirect) {

		// if on a landing page, redirect to current page
		$redirect = home_url();
     return $redirect;
}
//add_filter('woocommerce_login_redirect', 'byo_wc_login_redirect');

// if a landing page ...
//remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );


/*
 * Hide products based on membership status
 */

add_action( 'woocommerce_product_query', 'byo_hide_products_category_shop' );
add_action( 'pre_get_posts', 'byo_hide_products_category_shop' );

function byo_hide_products_category_shop( $q ) {

	if ( ! $q->is_main_query() || is_admin() ) {
			return;
	}
	// hide digital subscription products
	//if ( byo_has_subscription()) {
	if ( byo_custom_membership_status() == 'active') {
		$exclude = array( 69708, 69710 ); // digital, and combo USA
		$q->set( 'post__not_in', $exclude);
	}
	return;

}


/*****************************************************************************/

function byo_add_username_link( $items, $args ) {
    // Check is user logged in
		if ( $args->theme_location == 'tophat-right' ) {

			if ( is_user_logged_in() ) {

        $user = wp_get_current_user();
        $email = $user->user_email;
				$email = substr( $email, 0, 8). '&hellip;';
				$link = '<a href="/my-account"><i class="fa fa-user"></i> Welcome, <span class="name">'.$email.'</span></a>';

				$items = '<li class="tophat-log-out logged-in menu-item">'.$link.'</li>' . $items;
    	}
			else {
				$link = '<a href="/wp-login.php"><i class="fa fa-user"></i> Log In</a>';

				$items = '<li class="tophat-log-in logged-out menu-item">'.$link.'</li>' . $items;
			}
		}
		return $items;
}
add_filter( 'wp_nav_menu_items', 'byo_add_username_link', 10, 2 );

/*
 * Excerpt functions, the_excerpt()
*/
function byo_custom_excerpt_length( $length ) {
	$length = 32;	// default
	//$length = 100;	// default
	$length = ( is_search()) ? 72 : $length;
  return $length;
}
add_filter( 'excerpt_length', 'byo_custom_excerpt_length', 999 );

function byo_excerpt_more( $more ) {
    return '';
}
add_filter( 'excerpt_more', 'byo_excerpt_more' );


/*
 ** Transform WC text
 * @link http://codex.wordpress.org/Plugin_API/Filter_Reference/gettext
 */
 function byo_wc_text_strings( $translated_text, $text, $domain ) {
 	switch ( $translated_text ) {
 		case 'Proceed to checkout' :
 			$translated_text = __( 'Checkout', 'woocommerce' );
 		break;
		case 'Cart totals' :
	 		$translated_text = __( 'Total', 'woocommerce' );
	 	break;
 	}
 	return $translated_text;
 }
 //add_filter( 'gettext', 'byo_wc_text_strings', 20, 3 );


/*
 * remove Woo css/js if not in use
 */
add_action( 'wp_enqueue_scripts', 'swg_dequeue_woocommerce_styles_scripts', 99 );

function swg_dequeue_woocommerce_styles_scripts() {
    if ( function_exists( 'is_woocommerce' ) ) {
        if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
            # Styles
            wp_dequeue_style( 'woocommerce-general' );
            wp_dequeue_style( 'woocommerce-layout' );
            wp_dequeue_style( 'woocommerce-smallscreen' );
            wp_dequeue_style( 'woocommerce_frontend_styles' );
            wp_dequeue_style( 'woocommerce_fancybox_styles' );
            wp_dequeue_style( 'woocommerce_chosen_styles' );
            wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
            # Scripts
            wp_dequeue_script( 'wc_price_slider' );
            wp_dequeue_script( 'wc-single-product' );
            wp_dequeue_script( 'wc-add-to-cart' );
            wp_dequeue_script( 'wc-cart-fragments' );
            wp_dequeue_script( 'wc-checkout' );
            wp_dequeue_script( 'wc-add-to-cart-variation' );
            wp_dequeue_script( 'wc-single-product' );
            wp_dequeue_script( 'wc-cart' );
            wp_dequeue_script( 'wc-chosen' );
            wp_dequeue_script( 'woocommerce' );
            wp_dequeue_script( 'prettyPhoto' );
            wp_dequeue_script( 'prettyPhoto-init' );
            wp_dequeue_script( 'jquery-blockui' );
            wp_dequeue_script( 'jquery-placeholder' );
            wp_dequeue_script( 'fancybox' );
            wp_dequeue_script( 'jqueryui' );
        }
    }
}


/*****************************************************************************/
/**
 * WordPress Bootstrap Pagination
 */
function byo_wp_bootstrap_pagination( $args = array() ) {

    $defaults = array(
        'range'           => 7,
        'custom_query'    => FALSE,
        'previous_string' => __( 'prev', 'text-domain' ),
        'next_string'     => __( 'next', 'text-domain' ),
        'before_output'   => '<div class="post-nav"><ul class="pager">',
        'after_output'    => '</ul></div>'
    );

    $args = wp_parse_args(
        $args,
        apply_filters( 'wp_bootstrap_pagination_defaults', $defaults )
    );

    $args['range'] = (int) $args['range'] - 1;
    if ( !$args['custom_query'] )
        $args['custom_query'] = @$GLOBALS['wp_query'];
    $count = (int) $args['custom_query']->max_num_pages;
    $page  = intval( get_query_var( 'paged' ) );
    $ceil  = ceil( $args['range'] / 2 );

    if ( $count <= 1 )
        return FALSE;

    if ( !$page )
        $page = 1;

    if ( $count > $args['range'] ) {
        if ( $page <= $args['range'] ) {
            $min = 1;
            $max = $args['range'] + 1;
        } elseif ( $page >= ($count - $ceil) ) {
            $min = $count - $args['range'];
            $max = $count;
        } elseif ( $page >= $args['range'] && $page < ($count - $ceil) ) {
            $min = $page - $ceil;
            $max = $page + $ceil;
        }
    } else {
        $min = 1;
        $max = $count;
    }

    $echo = '';
    $previous = intval($page) - 1;
    $previous = esc_attr( get_pagenum_link($previous) );

    $firstpage = esc_attr( get_pagenum_link(1) );
    if ( 1 && $firstpage && (1 != $page) )
        $echo .= '<li class="previous"><a href="' . $firstpage . '">' . __( 'first', 'text-domain' ) . '</a></li>';
    if ( $previous && (1 != $page) )
        $echo .= '<li><a href="' . $previous . '" title="' . __( 'previous', 'text-domain') . '">' . $args['previous_string'] . '</a></li>';

    if ( !empty($min) && !empty($max) ) {
        for( $i = $min; $i <= $max; $i++ ) {
            if ($page == $i) {
                $echo .= '<li class="active"><span class="active">' . str_pad( (int)$i, 2, '0', STR_PAD_LEFT ) . '</span></li>';
            } else {
                $echo .= sprintf( '<li><a href="%s">%002d</a></li>', esc_attr( get_pagenum_link($i) ), $i );
            }
        }
    }

    $next = intval($page) + 1;
    $next = esc_attr( get_pagenum_link($next) );
    if ($next && ($count != $page) )
        $echo .= '<li class="next"><a href="' . $next . '" title="' . __( 'next', 'text-domain') . '">' . $args['next_string'] . '</a></li>';

    $lastpage = esc_attr( get_pagenum_link($count) );
    if ( 1 && $lastpage ) {
        $echo .= '<li class="last"><a href="' . $lastpage . '">' . __( 'last', 'text-domain' ) . '</a></li>';
    }
    if ( isset($echo) )
        echo $args['before_output'] . $echo . $args['after_output'];
}

//
// Quick related posts tool
//
function byo_get_related_posts_id( $ID) {
	global $post;
	$orig_post = $post;

	$list = array();
	if( !$ID)
		return( $list );

	// use same post type
	$post_type = get_post_type( $ID);
	// pick a primary taxonomy
	$taxonomy = ( $post_type == 'recipe') ? 'beer-style' : 'topic';

	$terms = get_the_terms( $ID, $taxonomy  );

	if ( $terms ) {
		$term_ids = array();
		foreach( $terms as $term)
			$term_ids[] = $term->term_id;
		}
	else {
		// fall back taxonomy
		$taxonomy = 'date';
		$terms = get_the_terms( $ID, $taxonomy  );
		if ( $terms ) {
			$term_ids = array();
			foreach( $terms as $term)
				$term_ids[] = $term->term_id;
			}
	}
	$args = array(
		'post_type' => $post_type,
		'post__not_in' => array( $ID),
		'tax_query' => array(
			array(
				'taxonomy' => $taxonomy ,
				'field' => 'term_id',
				'terms' => $term_ids,
			),
		),

	);
	$i = 0;
	$my_query = new WP_Query( $args );
	if( $my_query->have_posts() ) {
		while( $my_query->have_posts() ) {
			$my_query->the_post();
			$list[] = $post->ID;
			// $i++;
			// if( $i == 2)
				// break;
		}
	}
	$post = $orig_post;
	wp_reset_query();

shuffle( $list);
return( array_slice( $list, 0, 2) );
}

// fix missing beer style images
function byo_beer_style_image( $s) {
	$styles = array(
		'american-amber-ale' =>		'american-amber-ale',
		'american-lager' =>		'american-lager',
		'american-pale-ale' =>		'american-pale-ale',
		'barleywine-and-imperial-stout' =>		'barleywine-and-imperial-stout',
		'belgian-and-french-ale' =>		'belgian-and-french-ale',
		'belgian-blond' =>		'wheat-beer',
		'belgian-lambic-and-sour-ale' =>		'belgian-lambic-and-sour-ale',
		'belgian-strong-ale' =>		'belgian-strong-ale',
		'blended-beers' =>		'blended-beers',
		'bock' =>		'bock',
		'brown-ale' =>		'brown-ale',
		'cider-and-perry' =>		'cider-and-perry',
		'double-ipa' =>		'india-pale-ale',
		'english-and-scottish-strong-ale' =>		'english-and-scottish-strong-ale',
		'english-bitter-and-pale-ale' =>		'english-bitter-and-pale-ale',
		'european-dark-lager' =>		'european-dark-lager',
		'european-pale-lager' =>		'european-pale-lager',
		'fruit-beer' =>		'fruit-beer',
		'german-ale' =>		'german-amber-lager',
		'german-amber-lager' =>		'german-amber-lager',
		'golden-ale-cream-ale' =>		'light-ale',
		'imperial-ipa' =>		'india-pale-ale',
		'india-pale-ale' =>		'india-pale-ale',
		'irish-ales' =>		'scottish-ale',
		'kolsch-and-altbier' =>		'kolsch-and-altbier',
		'light-ale' =>		'light-ale',
		'mead' =>		'mead',
		'pilsner' =>		'pilsner',
		'porter' =>		'porter',
		'saison' =>		'belgian-and-french-ale',
		'sake' =>		'belgian-and-french-ale',
		'scottish-ale' =>		'scottish-ale',
		'smoked-beer' =>		'smoked-beer',
		'soda-pop' =>		'soda-pop',
		'specialty-and-experimental-beer' =>		'specialty-and-experimental-beer',
		'spice-herb-and-vegetable-beer' =>		'spice-herb-and-vegetable-beer',
		'stout' =>		'stout',
		'wheat-beer' =>		'wheat-beer',
		'wildsour' =>		'belgian-lambic-and-sour-ale',
		'winter-beers' =>	'european-dark-lager',
	);
	return( $styles[ $s]);
}

// find the first image in the content, or use a default
function byo_catch_first_image( $p) {
  $first_img = '';
  ob_start();
  ob_end_clean();
	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $p->post_content, $matches);
	//var_dump( $matches);
	//print_r( $matches);
	if( isset( $matches[1][0] ) )
		$first_img = $matches[1][0];
	//return "";

  if( empty( $first_img )) { //Defines a default image

		$type = $p->post_type;
		$default = '';

		if ( 'recipe' == $type) {

			$styles = wp_get_post_terms( $p->ID, 'beer-style');
			if ( !is_wp_error( $styles ) && isset( $styles[0])) {
				$beer_style = $styles[0]->slug;
			} else {
				$beer_style = 'light-ale';
			}
			$beer_style = byo_beer_style_image( $beer_style);
			$default_beer_style_image = 'beer-style-' . $beer_style . '.jpg';

			$first_img = '/wp-content/uploads/beer-style-' . $beer_style . '.jpg';
		}
		else {

			switch( $type) {
				case 'article':
				case 'project':
#				case 'recipe':
					$default = '_'.$type;
				break;
				case 'mr-wizard':
					$default = '_mr_wizard';
				break;
				default:
				break;
			}
			$first_img = get_field( 'image_placeholder'.$default, 'option');
		}
  }
	//return '';
  return $first_img;
}

function byo_faq_custom_query( $query ) {
    if ( $query->is_post_type_archive( 'faq') ) {
        $query->set( 'orderby', 'menu_order' );
        $query->set( 'order', 'ASC' );
    }
}
add_filter( 'pre_get_posts', 'byo_faq_custom_query' );

//
// ACF Advanced Custom Fields
//
function byo_acf_post_object_query( $args, $field, $post_id ) {
    // only show children of the current post being edited
    $args['post_status'] = 'publish';
    return $args;
}
// filter for every field
add_filter('acf/fields/post_object/query', 'byo_acf_post_object_query', 10, 3);

// build an index of links for a taxonomy
function byo_build_index_taxonomy( $taxonomy, $post_type=null) {

	$args = array(
		'taxonomy' => $taxonomy,
		'hide_empty' => true,
	 );

	 if( 'date' == $taxonomy) {
		 $args[ 'orderby' ] = 'term_order';
	 }

		if( $post_type)	{
				$post_filter = '?post_type='.$post_type;
		}
		else {
			$post_filter = '';
		}
	$terms = get_terms( $args );
	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
			$count = count( $terms );
			$i = 0;
			$term_list = '<p class="my_term-archive">';
			foreach ( $terms as $term ) {
					$i++;
					$term_list .= '<a href="' . esc_url( get_term_link( $term ) ) . $post_filter . '" alt="' . esc_attr( sprintf( __( 'View all post filed under %s', 'my_localization_domain' ), $term->name ) ) . '">' . $term->name . '</a>';
					if ( $count != $i ) {
							$term_list .= ' &middot; ';
					}
					else {
							$term_list .= '</p>';
					}
			}
			echo $term_list;
	}

}


// build an index of links for a taxonomy
function byo_build_index_taxonomy_2( $taxonomy, $post_type='all') {

	$args = array(
		'taxonomy' => $taxonomy,
		'hide_empty' => true,
	 );

	 if( 'date' == $taxonomy) {
		 $args[ 'orderby' ] = 'term_order';
	 }

		if( $post_type) {
			if( $post_type == 'all')
				$post_filter = '&post_type[]=article'.
				'&post_type[]=recipe'.
				'&post_type[]=mr-wizard'.
				'&post_type[]=project';
			else
				$post_filter = '&post_type='.$post_type;
		}
		else {
			$post_filter = '';
		}
	$terms = get_terms( $args );
	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
			$count = count( $terms );
			$i = 0;
			$term_list = '<p class="my_term-archive">';
			foreach ( $terms as $term ) {
					$i++;
					if ( $term->count > 0) {
						$s = '/?s=&' . $taxonomy . '%5B' . $term->slug . '%5D=' . $term->slug;
						$term_list .= '<a href="' . $s . $post_filter . '" alt="' . esc_attr( sprintf( __( 'View all post filed under %s', 'my_localization_domain' ), $term->name ) ) . '">' . $term->name . '</a>';
						if ( $count != $i ) {
								$term_list .= ' &middot; ';
						}
						else {
								$term_list .= '</p>';
						}
					}
			}
			echo $term_list;
	}

}

function byo_build_filters( $post_type, $post_types, $taxonomies) {
  // TO DO: Break this out somehow
  $filters_data = swg_browse_search_build_filter_data($post_types, $taxonomies);
  echo swg_browse_search_build_filter_markup($filters_data);

}

//test filter with lostpassword redirect
add_filter( 'lostpassword_url', 'swg_add_redirect', 10, 2);
function swg_add_redirect( $url, $redirect) {
	if ( isset( $_GET["redirect_to"]))
		$url = $url . '&redirect_to='. esc_url( $_GET["redirect_to"]);

	return( $url);
}


add_filter('relevanssi_search_ok', 'byo_rlv_search_trigger');
function byo_rlv_search_trigger( $search_ok ) {
	global $wp_query;

	if ( $wp_query->query &&
			array_key_exists('s', $wp_query->query) &&
			empty($wp_query->query['s']) &&
			!empty($wp_query->query_vars['post_type']) ) {
		$search_ok = true;
	}
	return $search_ok;
}

function byo_rlv_search_query_no_keyword( ) {
	global $wp_query;
	$q = new WP_Query();
	$q->query = array();
	$q->query['post_type'] = $wp_query->query['post_type'];

	return;

}

// build a search query for relevanssi when there is no keyword
add_filter('relevanssi_hits_filter', 'byo_rlv_hits_filter');

function byo_rlv_hits_filter( $hits ) {
	global $wp_query;

	if ( $hits[0] == null && ( !empty( $wp_query->query['post_type']) ) ) {
		$args = array();

		// byo_rlv_search_query_no_keyword();

		// no search hits, so must create new
		$args = array(
				'post_type' => 'article',
	 			'posts_per_page' => -1
		);
		if ( !empty( $wp_query->query['post_type']) ) {
		// if ( isset( $q[ 'post_type']) ) {
			$args[ 'post_type'] = $wp_query->query['post_type'];
		}

		if( empty( $args[ 'tax_query' ] )) {
			$args[ 'tax_query' ] = array( 'relation' => 'AND');
		}

		if ( !empty( $wp_query->query['beer-style']) ) {
			$tax_sub = array( 'relation' => 'OR');
			foreach ( $wp_query->query['beer-style'] as $key => $term) {
				$tax_query = array(
					'taxonomy' => 'beer-style',
					'field' => 'slug',
					'terms' => $term,
					);
				$tax_sub[] = $tax_query;
			}
			$args[ 'tax_query' ][] = $tax_sub;

		}
		if ( !empty( $wp_query->query['recipe-type']) ) {
			$tax_sub = array( 'relation' => 'OR');
			foreach ( $wp_query->query['recipe-type'] as $key => $term) {
				$tax_query = array(
					'taxonomy' => 'recipe-type',
					'field' => 'slug',
					'terms' => $term,
					);
				$tax_sub[] = $tax_query;
			}
			$args[ 'tax_query' ][] = $tax_sub;
		}
		if ( !empty( $wp_query->query['topic']) ) {
			$tax_sub = array( 'relation' => 'OR');
			foreach ( $wp_query->query['topic'] as $key => $term) {
				$tax_query = array(
					'taxonomy' => 'topic',
					'field' => 'slug',
					'terms' => $term,
					);
				$tax_sub[] = $tax_query;
			}
			$args[ 'tax_query' ][] = $tax_sub;
		}
		if ( !empty( $wp_query->query['date']) ) {
			$tax_sub = array( 'relation' => 'OR');
			foreach ( $wp_query->query['date'] as $key => $term) {
				$tax_query = array(
					'taxonomy' => 'date',
					'field' => 'slug',
					'terms' => $term,
					);
				$tax_sub[] = $tax_query;
			}
			$args[ 'tax_query' ][] = $tax_sub;
		}

		$hits[0] = get_posts( $args );
  }
  else {
		// posts available, take only those that match the conditions
		$ok = array();
		foreach ($hits[0] as $hit) {
			if ( 1 )
				array_push($ok, $hit);
		}
	$hits[0] = $ok;
	}

  return $hits;
}

// filter countries in checkout
add_filter( 'woocommerce_countries_allowed_countries' , 'swg_custom_override_allowed_countries' );

function swg_custom_override_allowed_countries( $countries ) {
		// remove unused countries
     unset( $countries[ 'UM'] );
     return $countries;
}

// filter order status
add_filter( 'wc_order_statuses' , 'swg_custom_override_order_statuses' );
function swg_custom_override_order_statuses( $statuses ) {
		// remove unused countries
     $statuses[ 'wc-processing' ] = _x( 'Active', 'Order status', 'woocommerce' );
     return $statuses;
}

/**
 * Disable the notice displayed when an admin is browsing content restricted to non-members.
 * Minimum v1.10.1 of Memberships required.
 */
add_filter( 'wc_memberships_display_restricted_content_admin_notice', '__return_false' );

/*
 * simplify checkout for digital products
 * eliminate required fields
 */
function swg_custom_remove_woo_checkout_fields( $fields ) {
 $digital_only = false;
 // digital membership, 14-day free trial
 //$digital_only_product_ids = array( 135734, 69708, 69709);
 $digital_only_product_slugs = array( 'digital-membership', 'digital-membership-free-14-day-trial', 'gift-digital-membership' );

	$landing_page_product = get_field( 'woocommerce_product');

	// handle one page checkout, do not check cart
	global $product;
	if( $product ) {
		if ( ! is_object( $product))
			$product = wc_get_product( get_the_ID() );
		//$digital_only = in_array( $product->get_id() , $digital_only_product_ids );
		$digital_only = in_array( $product->get_slug() , $digital_only_product_slugs );
	}
	// handle landing page
	else if ( $landing_page_product ) {
		$p = wc_get_product( $landing_page_product->ID );
		$digital_only = in_array( $p->get_slug() , $digital_only_product_slugs );
	}
	else {

		 $cart = WC()->cart;
		 if ( $cart ) {

			 $size = sizeof( $cart->get_cart() );
			 //check if product already in cart
			 if ( $size > 0 ) {
				 foreach ( $cart->get_cart() as $cart_item_key => $values ) {
					 //var_dump( $values);
					 //$this_id = $values['product_id'];
					 //$found = in_array( $this_id , $digital_only_product_ids );
					 $p = wc_get_product( $values['product_id'] );
					 if ( $p )
					 	$found = in_array( $p->get_slug() , $digital_only_product_slugs );

					 if ( !$found)	{
						 $digital_only = false;
						 break;
					 }
					 else {
						 $digital_only = true;
					 }

				 }
			 }
		 }

		}


 if ( $digital_only) {

		// remove billing fields
		//unset($fields['billing']['billing_first_name']);
		//unset($fields['billing']['billing_last_name']);
		unset($fields['billing']['billing_company']);
		unset($fields['billing']['billing_address_1']);
		unset($fields['billing']['billing_address_2']);
		unset($fields['billing']['billing_city']);
		unset($fields['billing']['billing_postcode']);
		unset($fields['billing']['billing_country']);
		unset($fields['billing']['billing_state']);
		unset($fields['billing']['billing_phone']);
		//unset($fields['billing']['billing_email']);

		// remove shipping fields
		unset($fields['shipping']['shipping_first_name']);
		unset($fields['shipping']['shipping_last_name']);
		unset($fields['shipping']['shipping_company']);
		unset($fields['shipping']['shipping_address_1']);
		unset($fields['shipping']['shipping_address_2']);
		unset($fields['shipping']['shipping_city']);
		unset($fields['shipping']['shipping_postcode']);
		unset($fields['shipping']['shipping_country']);
		unset($fields['shipping']['shipping_state']);

		// remove order comment fields
		//unset($fields['order']['order_comments']);
	 ?>
	 <?php
 }

	return $fields;
}
add_filter( 'woocommerce_checkout_fields' , 'swg_custom_remove_woo_checkout_fields' );

// add this filter to one page checkout as well
add_filter( 'woocommerce_default_address_fields', 'swg_custom_remove_woo_checkout_fields', 80, 1 );

//add_filter( 'woocommerce_default_address_fields', 'swg_filter_address_fields', 10, 1 );

function swg_filter_address_fields( $fields ) {

	return $fields;
}
