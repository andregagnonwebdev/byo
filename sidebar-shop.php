<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package byo
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="" class="">
	<div class="cart">
		<h4>Cart</h4>

		<a class="cart-customlocation" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">
			<?php echo sprintf ( _n( '%d item', '%d items', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?> - <?php echo WC()->cart->get_cart_total(); ?>
		</a>

	</div>
<!-- <aside id="secondary" class="widget-area col-xs-12 col-md-4"> -->
	<h4>Product Categories</h4>
	<?php

	// find categories with hidden products, exclude them
	$args = array(
		'hide_empty' => false,
		'hierarchical' => true,
	);
	$product_categories = get_terms( 'product_cat', $args );
	$exclude = array();
	foreach ( $product_categories as $category ) {
		$posts       = get_posts( array( 'post_type' => 'product', 'posts_per_page' => -1, 'product_cat' => $category->slug, 'fields' => 'ids' ) );
		$show_category = false;
		foreach ( $posts as $post ) {
			$product         = new WC_Product( $post );
			$visible_product = $product->is_visible();
			if ( true === $visible_product ) {
				$show_category = true;
				break;
			}
		}
		if ( false === $show_category ) {
			$exclude[] = $category->term_id;
		}
	}

		wp_list_categories( $args = array(
				'taxonomy' => 'product_cat',
				'show_count' => true,
				'exclude' => $exclude,
				'use_desc_for_title' => false,
				'title_li' => '',
		));
	?>
	<ul>
	<?php
	//get_template_part( 'woocommerce/taxonomy', 'product_cat.php' )


	$prod_cat_args = array(
	'taxonomy'     => 'product_cat', //woocommerce
	            'orderby'      => 'name',
	            'empty'        => 0
	            );

	            $terms = get_categories( $prod_cat_args );
	            //$term_id=6;
	            foreach ( $terms as $term ) {
	            $term_link = get_term_link( $term );
	            //echo '<li><a class="shopping-now" href="' . esc_url( $term_link ) . '">' . $term->name . '</a></li>';
	            }
	?>
	</ul>
</aside><!-- #secondary -->

<div class="row shop-subscribe-ad">
	<div class="col-xs-12">
		<a href="https://winemakermag.com/subscribe" target="_blank">
			<img src="<?php echo get_field( 'shop_subscribe_ad_image', 'option'); ?>" >
		</a>
	</div>
</div>
