<?php
/**
 * Template Name: Supplier
 * Template Post Type: 	page, resource
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main col-xs-12 col-md-12">
			<div class="row supplier">

				<?php 	if ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'template-parts/fragment', 'ad-units-sponsor-article'); ?>

					<div class="col-xs-12">
						<h1><?php the_title() ?></h1>
						<?php echo wpautop( get_the_content()); ?>
					</div>
				<?php endif; ?>

			<?php get_template_part( 'template-parts/content', 'suppliers' ); ?>

			</div>
		</main>
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
