<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php //get_template_part( 'template-parts/fragment', 'ad-units-sponsor-article'); ?>

	<div class="row">
	<div class="col-xs-12 col-md-9">
	<header class="entry-header">
		<?php $obj = get_post_type_object( get_post_type() ); ?>
		<span class="eyebrow">Boot Camp</span>

		<?php	the_title( '<h1 class="entry-title">', '</h1>' );	?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php	the_content();	?>
	</div><!-- .entry-content -->
	<?php //echo get_field( 'product_category'); ?>
	<?php $f = get_field( 'product_category_object'); ?>
	<?php if ( $f ): ?>
		<?php
			$slug = $f->slug;
			//$slug = 'advertising';
			echo do_shortcode( '[products category="'.$slug.'"]' );
		?>
	<?php endif; ?>
	</div>
	<div class="col-xs-12 col-md-3 ad-units">

		<?php get_template_part( 'template-parts/content', 'bootcamp-link'); ?>

		<?php get_template_part( 'template-parts/fragment', 'ad-units-right-rail'); ?>
	</div>
</div>

<footer class="entry-footer">
	<?php get_template_part( 'template-parts/fragment', 'publisher-json-ld'); ?>
</footer><!-- .entry-footer -->

</article><!-- #post-<?php the_ID(); ?> -->

<?php get_template_part( 'template-parts/fragment', 'free-content-lead'); ?>
