<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


	<?php if ( byo_has_subscription() == false ): ?>
		<?php get_template_part( 'template-parts/fragment', 'form-free-trial-2' ); ?>
		<?php //get_template_part( 'template-parts/fragment', 'subscribe-3' ); ?>
	<?php endif; ?>


	<div class="clearfix"></div>
	<div class="col-xs-12 col-md-12">
			<?php get_template_part( 'searchform', 'all' ); ?>
	</div>

	<?php if ( byo_has_subscription() == true ): ?>

		<?php get_template_part( 'template-parts/fragment', 'current-issue' ); ?>
		<?php get_template_part( 'template-parts/fragment', 'magazine' ); ?>
	<?php else: ?>
		<?php get_template_part( 'template-parts/fragment', 'magazine-free' ); ?>
	<?php endif; ?>

	<?php //get_template_part( 'template-parts/fragment', 'magazine' ); ?>

	<?php if ( byo_has_subscription() == false ): ?>
		<?php get_template_part( 'template-parts/fragment', 'form-email-1' ); ?>
	<?php endif; ?>

	<?php get_template_part( 'template-parts/fragment', 'mr-wizard' ); ?>
	<?php get_template_part( 'template-parts/fragment', 'form-email-2' ); ?>
	<?php get_template_part( 'template-parts/fragment', 'form-email-3' ); ?>



	<?php if ( byo_has_subscription() == false ): ?>
		<div class="row publisher  front-publisher-and-ads">
			<?php get_template_part( 'template-parts/fragment', 'publisher' ); ?>
			<?php get_template_part( 'template-parts/fragment', 'ad-units-home-btf' ); ?>
		</div>
		<?php get_template_part( 'template-parts/fragment', 'testimonial' ); ?>
	<?php endif; ?>





	<?php if ( byo_has_subscription() == true ): ?>
		<div class="row front-publisher-and-ads">
			<?php get_template_part( 'template-parts/fragment', 'publisher' ); ?>
			<?php get_template_part( 'template-parts/fragment', 'ad-units-home-btf' ); ?>
		</div>
		<?php get_template_part( 'template-parts/fragment', 'form-email-1' ); ?>
		<?php get_template_part( 'template-parts/fragment', 'subscribe-1' ); ?>
	<?php endif; ?>



	<?php if ( byo_has_subscription() == false ): ?>
		<?php get_template_part( 'template-parts/fragment', 'subscribe-3' ); ?>
	<?php endif; ?>

</article><!-- #post-<?php the_ID(); ?> -->
