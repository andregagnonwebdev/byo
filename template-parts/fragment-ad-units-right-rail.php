<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>
<?php if ( 1 || !WP_DEBUG): ?>

<div class="right-rail-container">

  <!-- /52443766/byo_content_right-top_200x200 -->
  <div id='div-gpt-ad-1507727668641-5' class="ad-unit-box" style='height:200px; width:200px;'>
  <script>
  googletag.cmd.push(function() { googletag.display('div-gpt-ad-1507727668641-5'); });
  </script>
  </div>

  <div class="clearfix"></div>
  <!-- /52443766/byo_content_right-middle_200x100 -->
  <div id='div-gpt-ad-1507727668641-4' class="ad-unit-box" style='height:100px; width:200px;'>
  <script>
  googletag.cmd.push(function() { googletag.display('div-gpt-ad-1507727668641-4'); });
  </script>
  </div>

  <div class="clearfix"></div>
  <!-- /52443766/byo_content_right-bottom_200x200 -->
  <div id='div-gpt-ad-1507727668641-3' class="ad-unit-box" style='height:200px; width:200px;'>
  <script>
  googletag.cmd.push(function() { googletag.display('div-gpt-ad-1507727668641-3'); });
  </script>
  </div>

</div>
<?php endif; ?>
