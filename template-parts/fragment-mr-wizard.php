<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>


	<div class="col-xs-12 front-mr-wizard">
		<p class="yellow-heading text-center">Troubleshooting</p>

		<h6 class="text-center clean visible-xs visible-sm">Ask Mr. Wizard</h6>
		<h6 class="text-center hidden-xs hidden-sm">Ask Mr. Wizard</h6>
		<?php
			$p = get_field( 'mr_wizard_content');
			setup_postdata( $p );

			//get_template_part( 'template-parts/content', 'mr-wizard' );
		?>
		<?php if ( $f = get_field( 'question', $p->ID)): ?>
			<h3><?php echo $f; ?></h3>
		<?php endif; ?>

		<p><?php echo get_the_excerpt(); ?></p>
		<a class="answer" href="<?php echo esc_url( get_permalink( $p)); ?>" title="">
				Get full answer
		</a>
		<?php
			?>
		<?php wp_reset_postdata(); ?>

	</div>
