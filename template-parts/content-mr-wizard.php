<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="row">

	<div class="col-xs-12 col-md-8 col-lg-9">
	<div class="mr-wizard troubleshooting">
	<header class="entry-header">

		<?php get_template_part( 'template-parts/fragment', 'ad-units-sponsor-article'); ?>


		<?php $obj = get_post_type_object( get_post_type() ); ?>
		<span class="eyebrow">Ask <?php echo $obj->labels->singular_name; ?></span>
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;
		?>
		<h2 class="heading">TroubleShooting</h2>

		<?php if ( $f = get_field( 'questioner')): ?>
			<h5><?php echo $f; ?> asks,</h5>
		<?php endif; ?>
		<?php if ( $f = get_field( 'question')): ?>
			<div class="question-icon">Q</div>
			<p><?php echo $f; ?></p>
		<?php endif; ?>

		<?php if ( byo_has_subscription() == false ): ?>
			<?php get_template_part( 'template-parts/fragment', 'form-email-1' ); ?>
		<?php endif; ?>

		<?php
		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php //byo_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<div class="answer-icon">A</div>
		<?php
			the_content( sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'swg' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			) );
			?>
			<?php if ( $f = get_field( 'responder')): ?>
				<h5>Response by <?php echo $f; ?>.</h5>
			<?php endif; ?>

			<?php
			// wp_link_pages( array(
			// 	'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'swg' ),
			// 	'after'  => '</div>',
			// ) );
		?>


	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php get_template_part( 'template-parts/fragment', 'publisher-json-ld'); ?>

		<?php //byo_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</div>

</div>
<div class="col-xs-12 col-md-4 col-lg-3 ad-units">
	<?php get_template_part( 'template-parts/fragment', 'ad-units-right-rail'); ?>
</div>

<?php get_template_part( 'template-parts/fragment', 'related-posts'); ?>


</div>

</article><!-- #post-<?php the_ID(); ?> -->

<?php get_template_part( 'template-parts/fragment', 'free-content-lead'); ?>
