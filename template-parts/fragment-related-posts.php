<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<?php
	// if ( function_exists( 'get_crp_posts_id' ) ) {
	// 	$list = get_crp_posts_id( array('postid' => $post->ID,'limit' => 2) );
	// }
	if ( function_exists( 'byo_get_related_posts_id' ) ) {
		$list = byo_get_related_posts_id( $post->ID );
	}
?>

<?php if ( count( $list) > 0): ?>
	<div class="col-xs-12 text-center related-content">
		<?php //var_dump( $list); ?>
		<h4>You'll Also Like</h4>
		<br /><br />
	</div>
	<div class="row related-content">

<?php
foreach ( $list as $id) {

	global $p;
	$p = get_post( $id);
	setup_postdata( $p );
	$ID = $p->ID;
	$title = $p->post_title;
	$content = $p->post_content;
	?>

<div class="col-xs-12 col-md-6 content">
	<?php  // var_dump($p); ?>
	<?php byo_eyebrow_image_tag( $ID) ?>
	<?php if ( $p->post_type == 'mr-wizard'): ?>
		&nbsp;<br />
		&nbsp;<br />
	<?php endif; ?>

	<?php get_template_part( 'template-parts/fragment', 'featured-image' ); ?>

<div class="title">
	<?php echo byo_restricted_tag_text( $ID); ?>
	<a href="<?php echo esc_url( get_permalink( $p)); ?>" title="">
		<h2><?php echo $title; ?></h2>
	</a>
</div>

	<p><?php echo get_the_excerpt( $ID); ?></p>
</div>
<?php wp_reset_postdata(); ?>

<?php
}
?>
	</div>

<?php endif; ?>
