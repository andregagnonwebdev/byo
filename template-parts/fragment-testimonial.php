<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>


<?php if ( 1 || byo_has_subscription() == false ): ?>
	<div class="col-xs-12 front-testimonial">
		<h6 class="text-center clean visible-xs visible-sm"><?php echo get_field( 'testimonial_title'); ?></h6>
		<h6 class="text-center hidden-xs hidden-sm"><?php echo get_field( 'testimonial_title'); ?></h6>

		<blockquote><p><?php echo get_field( 'testimonial_text'); ?></p></blockquote>
		<strong><p class="author"><?php echo get_field( 'testimonial_author'); ?></p></strong>
	</div>
<?php endif; ?>
