<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package byo
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	<div class="browse-card">

	<div class="row">
		<div class="col-xs-12 content">
			<header class="entry-header title">
				<?php $obj = get_post_type_object( get_post_type() ); ?>
				<h5><?php echo $obj->labels->singular_name; ?></h5>
				<?php the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>'.byo_restricted_tag_text() ); ?>
			</header>
		</div>

		<?php if ( has_post_thumbnail() ) : ?>
		<div class="hidden-xs col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin-top: 25px;">
			    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
			        <?php the_post_thumbnail(); ?>
			    </a>
		</div>
	<?php endif; ?>

		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="entry-summary">
				<?php the_excerpt(); ?>
			</div><!-- .entry-summary -->

			<footer class="entry-footer">
				<?php //byo_entry_footer(); ?>
				&nbsp;<br />
				&nbsp;<br />
			</footer><!-- .entry-footer -->
		</div>


	</div>

	</div>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
