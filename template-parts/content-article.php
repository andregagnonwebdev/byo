<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="row" >
		<header class="entry-header">

			<?php get_template_part( 'template-parts/fragment', 'ad-units-sponsor-article'); ?>

			<div class="col-xs-12">
				<?php $obj = get_post_type_object( get_post_type() ); ?>
				<span class="eyebrow"><?php echo $obj->labels->singular_name; ?></span>
				<?php
					the_title( '<h1 class="entry-title">', '</h1>' );
				?>
				<!-- <strong>Written
					<?php if ( $s = get_the_term_list( $post->ID, 'writer', '', ', ' )): ?>
						by <?php echo $s; ?>
					<?php endif; ?>
					<?php if ( 0 && $s = get_the_term_list( $post->ID, 'date', '', ', ' )): ?>
						in <?php echo $s; ?><br /><br />
					<?php endif; ?>
					<br /><br />
					<?php // byo_posted_on() ?>
				</strong> -->


			</div>
		</header>
	</div>

	<div class="row" >
		<div class="col-xs-12 col-md-8 col-lg-9">
		<!-- <div class="col-xs-12 col-md-9"> -->
			<div class="entry-content row">

				<div class="col-xs-12 col-md-12">
					<?php the_content(); ?>
				</div>
				<?php if ( 0 && byo_is_restricted() == false && ( byo_has_subscription() == false ) ): ?>
					<div class="col-xs-12 col-md-12">
						<?php	echo do_shortcode( get_field( 'email_signup_1', 'option'));	?>
					</div>
				<?php endif; ?>


				<div class="col-xs-12 col-md-12">
					<?php get_template_part( 'template-parts/content', 'writer' ); ?>
				</div>


					<?php
					// wp_link_pages( array(
					// 	'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'swg' ),
					// 	'after'  => '</div>',
					// ) );
					?>

				<?php get_template_part( 'template-parts/fragment', 'related-posts'); ?>

			</div><!-- .entry-content -->

			<footer class="entry-footer">
				<?php //byo_entry_footer(); ?>
				<?php get_template_part( 'template-parts/fragment', 'publisher-json-ld'); ?>

			</footer><!-- .entry-footer -->
		</div>


		<div class="col-xs-12 col-md-4 col-lg-3 ad-units">
		<!-- <div class="col-xs-12 col-md-3 ad-units"> -->
			<?php get_template_part( 'template-parts/fragment', 'ad-units-right-rail'); ?>
		</div>
</div>

</article><!-- #post-<?php the_ID(); ?> -->


<?php get_template_part( 'template-parts/fragment', 'free-content-lead'); ?>
