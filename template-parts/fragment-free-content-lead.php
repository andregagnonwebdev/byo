<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>


<?php if ( ( byo_is_restricted() == false ) && ( byo_has_subscription() == false )): ?>
	<div class="row" >

		<?php if ( is_page_template( 'template-thank-you.php' ) == false
			&& ( 'nanocon' != get_post_type() )
			): ?>
			<?php get_template_part( 'template-parts/fragment', 'form-email-1'); ?>
		<?php endif; ?>

		<?php get_template_part( 'template-parts/fragment', 'subscribe-3' ); ?>
</div>

<?php endif; ?>
