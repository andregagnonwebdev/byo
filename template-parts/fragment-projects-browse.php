<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>


	<div class="col-xs-12 articles-popular">
		<h6 class="text-center clean searches">Browse Projects</h6>

		<div class="row">

			<div class="col-xs-12 content">
				<h3>Topic</h3>
				<?php byo_build_index_taxonomy_2( 'topic', 'project'); ?>
			</div>


		</div>

	</div>
