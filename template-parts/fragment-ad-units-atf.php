<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<div class="container-fluid">
	<div class="container text-center ad-units-header" style="">
		<!-- /52443766/byo_content_atf_468x60 -->
		<div id='div-gpt-ad-1507727668641-1' class="ad-unit-box" style='height:60px; width:468px;display:block'>
			<script>
			googletag.cmd.push(function() { googletag.display('div-gpt-ad-1507727668641-1'); });
			</script>
		</div>
	</div>
</div>
