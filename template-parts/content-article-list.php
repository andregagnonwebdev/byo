<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="row" >
		<header class="entry-header">

			<?php //get_template_part( 'template-parts/fragment', 'ad-units-sponsor-article'); ?>

			<div class="col-xs-12">
				<?php $obj = get_post_type_object( get_post_type() ); ?>
				<span class="eyebrow"><?php echo $obj->labels->singular_name; ?></span>
				<?php
				if ( is_singular() ) :
					the_title( '<h1 class="entry-title">', '</h1>' );
				else :
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				endif;
				?>
				<strong>Written
					<?php if ( $s = get_the_term_list( $post->ID, 'writer', '', ', ' )): ?>
						by <?php echo $s; ?>
					<?php endif; ?>
					<?php if ( 0 &&  $s = get_the_term_list( $post->ID, 'date', '', ', ' )): ?>
					in <?php echo $s; ?>
					<?php endif; ?>
					<?php // byo_posted_on() ?></strong>


			</div>
		</header>
	</div>

	<div class="row" >
		<div class="col-xs-12 col-md-12">
			<div class="entry-content row">
				<div class="col-xs-12 col-md-12">
					<?php echo get_the_excerpt( $post->ID); ?>
				</div>

			</div><!-- .entry-content -->

			<footer class="entry-footer">
				<?php //byo_entry_footer(); ?>

			</footer><!-- .entry-footer -->
		</div>

</div>

</article><!-- #post-<?php the_ID(); ?> -->


<?php //get_template_part( 'template-parts/fragment', 'free-content-lead'); ?>
