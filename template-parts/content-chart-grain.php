<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>


	<div class="col-xs-12 col-md-8 col-lg-9">
		<div class="row">
		<?php
		if ( 1 ) : ?>

			<header classx="page-header"></header><!-- .page-header -->

		<form method="GET" action="">
			<div class="col-xs-12 col-md-6 col-lg-4">
			<?php
			// select the post types
			$post_types = array( 'chart-grain' );

			// select the custom taxonomy
	    $taxonomies = array( 'grain-style' );
			$default_term  = '';
	    // select the type of custom post
			$tax_slug = $taxonomies[ 0];
			echo '<select name="'.$tax_slug.'" >';
	            $tax_obj = get_taxonomy($tax_slug);
	            $tax_name = $tax_obj->labels->name;
	            $terms = get_terms($tax_slug);

							if(count($terms) > 0) {
	                foreach ($terms as $key => $term) {
											echo '<option name="'.$tax_slug.'['.$term->slug.']" value="'. $term->slug.'" '. ( (( isset( $_GET[$tax_slug]) && $_GET[$tax_slug] == $term->slug) || ($key == 0 && isset( $_GET[$tax_slug]) == false)) ? ' selected' : '') .' > '.$term->name.'</option>';
											if ( $key == 0)
												$default_term = $term->slug;	 	                }
	            }
			?>
		</select>

		</div>

		<div class="col-xs-12 col-md-6 col-lg-8">
			<input type="submit" name="tax-submit" />
		</div>
		</form>

		<?php //var_dump( $_GET); ?>
			<?php


			$args = array(
					'post_type'=> $post_types,
					'orderby'	=> 'title',
					'order' => 'ASC'
				);


//			 $tax_slug = 'writer';
	 		 $args['tax_query'] = array( 'relation' => 'AND');
			 $taxonomies = array( 'grain-style');
			 foreach( $taxonomies as $tax_slug) {
				 $term = ( isset( $_GET[ $tax_slug]) ) ? $_GET[ $tax_slug] : $default_term;
				 if ( $term ) {
					 $tax = array(
	 						'taxonomy' => $tax_slug,
							'field' => 'slug',
	 						'terms' => $term,
	 					);
					$args['tax_query'][] = $tax;
	 			 }
			 }
			//  var_dump( $args['tax_query']);

			$args['nopaging'] = true;

			$query = new WP_Query( $args );

			// Pagination fix
			global $wp_query;
			$temp_query = $wp_query;
			$wp_query   = $query;

			//var_dump ( $query);
			?>

			<div class="col-xs-12">
				<div class="row">
					<div class="heading">
						<div class="col-xs-3">Malt</div>
						<div class="col-xs-2">L</div>
						<div class="col-xs-2">G</div>
						<div class="col-xs-5">Description</div>
					</div>
				</div>

				<?php
				/* Start the Loop */
				$i = 0;
				while ( $query->have_posts() ) : $query->the_post();

					get_template_part( 'template-parts/content', 'chart-grain-list' );
					$i++;
				endwhile;
				?>

			</div>
			<?php


			// Reset main query object
			$wp_query = NULL;
			$wp_query = $temp_query;

			/* Restore original Post Data */
			wp_reset_postdata();

		endif; ?>

		</div>
	</div>

		<div class="col-xs-12 col-md-4 col-lg-3 ad-units">
			<?php get_template_part( 'template-parts/content', 'resource-link'); ?>
			<?php get_template_part( 'template-parts/fragment', 'ad-units-right-rail'); ?>
		</div>
