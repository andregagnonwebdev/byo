<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>


	<div class="col-xs-12 col-md-8 col-lg-9">
		<div class="row">
		<?php
		if ( 1 ) : ?>

			<header classx="page-headerx"></header><!-- .page-header -->

		<?php //var_dump( $_GET); ?>
			<?php

			$args = array(
					'post_type'=> 'supplier',
				);


//			 $tax_slug = 'writer';
	 		 $args['meta_query'] = array(
				 	'relation' => 'AND',
					'state_clause' => array(
						'key' => 'state',
						'compare' => 'exists'
					),
					'country_clause' => array(
						'key' => 'country',
						'compare' => 'exists'
					),
				);

 			$args['orderby'] = array(
					'country_clause' => 'DESC',
					'state_clause' => 'ASC',
					'title' => 'ASC'
			);

			$args['nopaging'] = true;
//var_dump( $args);
			$query = new WP_Query( $args );

			// Pagination fix
			global $wp_query;
			$temp_query = $wp_query;
			$wp_query   = $query;

			//var_dump ( $query);
			?>

			<div class="col-xs-12">

				<?php
				/* Start the Loop */
				$i = 0;
				while ( $query->have_posts() ) : $query->the_post();

					get_template_part( 'template-parts/content', 'supplier-list' );
					$i++;
				endwhile;
				?>

			</div>
			<?php


			// Reset main query object
			$wp_query = NULL;
			$wp_query = $temp_query;

			/* Restore original Post Data */
			wp_reset_postdata();

		endif; ?>

		</div>
	</div>

		<div class="col-xs-12 col-md-4 col-lg-3 ad-units">
			<?php get_template_part( 'template-parts/content', 'resource-link'); ?>
			<?php get_template_part( 'template-parts/fragment', 'ad-units-right-rail'); ?>
		</div>
