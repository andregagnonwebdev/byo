<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>
<?php
$query = new WP_Query( 'post_type=nanocon&order=ASC&orderby=menu_order');

if ( $query->have_posts() ) :
?>
<h2>NanoCon</h2>
<?php
	/* Start the Loop */
	while ( $query->have_posts() ) : $query->the_post();
		?>
		<div class="row">
			<div class="col-xs-12">
				<?php	the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );	?>
			</div>
		</div>
		<?php
	endwhile;

endif; ?>
&nbsp;<br />
