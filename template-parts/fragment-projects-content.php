<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>


	<div class="col-xs-12 projects">
		<!-- <p class="red-heading text-center"><?php echo get_field( 'issue'); ?></p> -->
		<h6 class="text-center clean">Browse Featured Projects</h6>

		<?php if( have_rows('projects_content') ): ?>

		<div class="row">
		<?php $count = 1; ?>
		<?php while( have_rows('projects_content') ): the_row();

			// vars
			global $p;
			$p = get_sub_field('project_content');
			setup_postdata( $p );
			$ID = $p->ID;
			$title = $p->post_title;
			$content = $p->post_content;
			?>
			<div class="col-xs-12 col-md-6 col-lg-6 content">
				<?php  // var_dump($p); ?>
				<?php byo_eyebrow_image_tag( $ID) ?>

				<?php get_template_part( 'template-parts/fragment', 'featured-image' ); ?>



				<div class="title">
					<?php echo byo_restricted_tag_text( $ID); ?>
					<a href="<?php echo esc_url( get_permalink( $p)); ?>" title="">
						<h2><?php echo $title; ?></h2>
					</a>
				</div>

				<p><?php echo get_the_excerpt( $ID); ?></p>

			</div>
			<?php wp_reset_postdata(); ?>
			<?php if ( $count++ % 2 == 0): ?>
				<div class="visible-md visible-lg">
					<div class="clearfix"></div>
				</div>
			<?php endif; ?>

		<?php endwhile; ?>

		</div>

<?php endif; ?>
	</div>
