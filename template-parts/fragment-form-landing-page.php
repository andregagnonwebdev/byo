<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<?php if ( 1 ): ?>
	<div class="col-xs-12 col-md-12 optinmonster-col">
		<?php	echo do_shortcode( get_field( 'optin_monster_form' ));	?>
	</div>
<?php endif; ?>
