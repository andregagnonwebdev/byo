<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<?php $obj = get_post_type_object( get_post_type() ); ?>

<div class="row">
	<div class="content">
		<div class="col-xs-3">
			<?php the_title(); ?>
		</div>
		<div class="col-xs-2">
			<?php $acid_low = get_field( "acid_low"); ?>
			<?php echo ( ( $acid_low) ? ($acid_low.'-'.get_field( "acid_high").'%') : ( 'N/A') ); ?>
		</div>
		<div class="col-xs-3">
			<?php
				global $post;
				$post_id = $post->ID;
				$taxonomy = 'substitution-hops';
				$args = array( 'orderby' => 'name', 'order' => 'ASC', 'fields' => 'names');
				// $args = array();
				$terms = wp_get_post_terms( $post_id, $taxonomy, $args );
				// var_dump( $terms );
				if ( count( $terms))	{
					$list = implode( ', ', $terms);
					echo $list;
				}
			?>
		</div>
		<div class="col-xs-4">
			<?php the_content(); ?>
		</div>
	</div>
</div>
