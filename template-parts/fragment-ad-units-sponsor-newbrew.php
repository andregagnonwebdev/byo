<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>
<div class="col-xs-12">
	<div class="ad-units-header newbrew">
		<?php
			// newbrew sponsorship
			$adUnitID = '1507727668641-12';

	  ?>
		<!-- /52443766/byo_articles_spons_680x80 -->
		<div id='div-gpt-ad-<?php echo $adUnitID; ?>' class="ad-unit-box" style='height:80px; width:680px;display:block'>
			<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-<?php echo $adUnitID; ?>'); });
			</script>
		</div>
	</div>
</div>
