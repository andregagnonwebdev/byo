<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>


	<div class="col-xs-12 col-md-12 front-latest-issue">

		<div class="row" style="min-height:10px;">
			<div class="col-xs-12 col-md-3 col-lg-2 image">
				<img class="text-center" src="<?php echo get_field( 'latest_issue_image'); ?>" >
			</div>
			<div class="col-xs-12 col-md-9 col-lg-8">

				<h3 class="red-heading text-center">THE LATEST ISSUE</h2>
				<hr />
				<h2><?php echo get_field( 'latest_issue_title'); ?></h2>
				<p><?php echo get_field( 'latest_issue_content'); ?></p>
				<p><?php echo get_field( 'latest_issue_sign_off'); ?></p>
				<p><a href="<?php echo get_permalink(143); ?>#swg-browse-search--browse--magazine-archive" title="All Issues">Browse All Issues ...</a></p>

		</div>
	</div>
</div>
