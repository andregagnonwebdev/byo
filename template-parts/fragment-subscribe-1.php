<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<?php if ( 1 || byo_has_subscription() == false ): ?>

<div class="col-xs-12 subscribe-1">

	<h2 class="text-center"><?php echo get_field( 'subscribe_title', 'option'); ?></h2>

	<?php if( have_rows('subscribe_3_columns', 'option') ): ?>
		<?php $i = 0; ?>

	<div class="row">

	<?php while( have_rows('subscribe_3_columns', 'option') ): the_row();
		// vars
		$p = get_sub_field('product_data', 'option');
		$slug = $p->post_name;
		$id = $p->ID;
		//setup_postdata( $p );
		global $woocommerce;
		if ( !defined('WC_VERSION'))
			$currency = '$';
		else
			$currency = get_woocommerce_currency_symbol();
		$price = get_post_meta( $p->ID, '_regular_price', true);

		$color = get_sub_field( 'color', 'option');
		?>
		<?php if ( $i == 2): ?>
			<div class="hidden-xs hidden-sm col-md-4 text-center">&nbsp;</div>
			<div class="col-xs-12 col-md-4 text-center">
				<img class="text-center" src="<?php echo get_sub_field( 'icon', 'option'); ?>" >
				<h2 class=""><?php echo get_sub_field( 'title', 'option'); ?></h2>
				<?php // get product, price, subscribe link ?>
				<p class="desc text-left"><?php echo get_sub_field( 'description', 'option'); ?></p>
				<p class="price text-center color-<?php echo $color ?>"><?php echo $currency; echo $price; ?></p>

				<a class="button bkgnd-<?php echo $color ?>" href="/checkout/?add-to-cart=<?php echo $id; ?>">
					subscribe
				</a>

			</div>
			<div class="hidden-xs hidden-sm col-md-4 text-center">&nbsp;</div>

			<?php //wp_reset_postdata();
			//var_dump($p);
			?>
		<?php endif; ?>

		<?php $i++; ?>
		<?php endwhile; ?>

		</div>
	<?php endif; ?>

	</div>
<?php endif; ?>
