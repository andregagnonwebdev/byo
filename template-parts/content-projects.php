<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="col-xs-12 col-md-8 col-lg-8">
		<div class="row">
			<?php get_template_part( 'template-parts/fragment', 'projects-content' ); ?>
		</div>
	</div>

	<div class="col-xs-12 col-md-4 col-lg-4">
		<div class="row">
			<?php get_template_part( 'template-parts/fragment', 'projects-popular' ); ?>
		</div>
		<!-- <div class="row">
			<?php //get_template_part( 'template-parts/fragment', 'projects-browse' ); ?>
		</div> -->
	</div>

</article><!-- #post-<?php the_ID(); ?> -->
