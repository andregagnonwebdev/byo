<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>


	<div class="col-xs-12 ad-units">
		<div class="row">

			<div class="col-xs-12 col-md-3">
				ad unit 1
				<!-- /52443766/byo_home_left_200x200 -->
				<div id='div-gpt-ad-1507727668641-9' style='height:200px; width:200px;'>
				<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1507727668641-9'); });
				</script>
				</div>
			</div>

			<div class="col-xs-12 col-md-3">
				ad unit 2
				<!-- /52443766/byo_home_center-left_200x200 -->
				<div id='div-gpt-ad-1507727668641-7' style='height:200px; width:200px;'>
					<script>
					googletag.cmd.push(function() { googletag.display('div-gpt-ad-1507727668641-7'); });
					</script>
				</div>
			</div>

			<div class="col-xs-12 col-md-3">
				ad unit 3
				<!-- /52443766/byo_home_center-right_200x200 -->
				<div id='div-gpt-ad-1507727668641-8' style='height:200px; width:200px;'>
				<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1507727668641-8'); });
				</script>
				</div>
			</div>

			<div class="col-xs-12 col-md-3">
				ad unit 4
				<!-- /52443766/byo_home_right_200x200 -->
				<div id='div-gpt-ad-1507727668641-10' style='height:200px; width:200px;'>
				<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1507727668641-10'); });
				</script>
				</div>
			</div>

		</div>
	</div>
