<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<?php $obj = get_post_type_object( get_post_type() ); ?>

<div class="row">
	<div class="content">
		<div class="col-xs-3">
			<?php the_title(); ?>
		</div>
		<div class="col-xs-2">
			<?php echo get_field( "lovi_low"); ?>-<?php echo get_field( "lovi_high"); ?>°
		</div>
		<div class="col-xs-2">
			<?php echo get_field( "grav_low"); ?>-<?php echo get_field( "grav_high"); ?>
		</div>
		<div class="col-xs-5">
			<?php the_content(); ?>
		</div>
	</div>
</div>
