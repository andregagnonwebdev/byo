<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>
<div class="col-xs-12">
	<div class="ad-units-header">
		<?php
			// article sponsorship
			$adUnitID = '1507727668641-0';

  		// filter for other post_types
			//var_dump( $post);
			switch ( $post->post_type) {
				case 'article';
					$adUnitID = '1507727668641-0';
				break;
				case 'project';
					$adUnitID = '1507727668641-13';
				break;
				case 'recipe';
					$adUnitID = '1507727668641-14';
				break;
				case 'mr-wizard';
					$adUnitID = '1507727668641-17';
				break;
				case 'resource';
					$adUnitID = '1507727668641-0';
				break;
				case 'chart-hops';
					$adUnitID = '1507727668641-11';
				break;
				case 'chart-grain';
					$adUnitID = '1507727668641-6';
				break;
				case 'chart-yeast';
					$adUnitID = '1507727668641-18';
				break;
				default:
					$adUnitID = '1507727668641-0';
				break;
			}
			// filter for topic
			$grains = ( has_term( 'grains', 'topic') ? true : false );
			$hops = ( has_term( 'hops', 'topic') ? true : false );
			$newbrew = ( has_term( 'newbrew', 'topic') ? true : false );
			// $cloning = ( has_term( 'cloning', 'topic') ? true : false );
			// $beer_styles = ( has_term( 'beer-styles', 'topic') ? true : false );
			$yeast = ( has_term( 'yeast', 'topic') ? true : false );

			// prioritize
			$adUnitID = ( $grains) ? '1507727668641-6' : $adUnitID;
			$adUnitID = ( $hops) ? '1507727668641-11' : $adUnitID;
			$adUnitID = ( $newbrew) ? '1507727668641-12' : $adUnitID;
			// $adUnitID = ( $cloning) ? '1507727668641-15' : $adUnitID;
			// $adUnitID = ( $beer_styles) ? '1507727668641-16' : $adUnitID;
			$adUnitID = ( $yeast) ? '1507727668641-18' : $adUnitID;

			//echo $adUnitID.'<br>';
	  ?>
		<!-- /52443766/byo_xxxxxx_spons_680x80 -->
		<div id='div-gpt-ad-<?php echo $adUnitID; ?>' class="ad-unit-box" style='height:80px; width:680px;display:block'>
			<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-<?php echo $adUnitID; ?>'); });
			</script>
		</div>
	</div>
</div>
