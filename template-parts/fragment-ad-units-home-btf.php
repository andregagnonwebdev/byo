<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<?php if ( 1 || !WP_DEBUG): ?>

	<div class="col-xs-12 col-md-3 ad-units-home-btf">
		<div class="row">

			<div class="col-xs-12">
				<!-- /52443766/byo_home_left_200x200 -->
				<div id='div-gpt-ad-1507727668641-9' class="ad-unit-box" style='height:100px; width:200px;'>
				<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1507727668641-9'); });
				</script>
				</div>
			</div>

			<div class="col-xs-12">
				<!-- /52443766/byo_home_center-left_200x200 -->
				<div id='div-gpt-ad-1507727668641-7' class="ad-unit-box" style='height:200px; width:200px;'>
					<script>
					googletag.cmd.push(function() { googletag.display('div-gpt-ad-1507727668641-7'); });
					</script>
				</div>
			</div>

		</div>
	</div>
<?php endif; ?>
