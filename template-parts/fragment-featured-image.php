<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<?php global $p; ?>
<?php
	$type = $p->post_type;
	if ( $type == 'mr-wizard')
		return;
	$default = '';

	switch( $type) {
		case 'article':
		case 'project':
		case 'recipe':
			$default = '_'.$type;
		break;
	case 'mr-wizard':
		$default = '_mr_wizard';
	break;
	default:
	break;
	}

?>

<?php if ( has_post_thumbnail( $p) ) : ?>
	<div class="">
			<a href="<?php echo esc_url( get_permalink( $p)); ?>" title="">
					<?php echo get_the_post_thumbnail( $p, 'article-thumb', array( 'class' => 'text-center' ) ); ?>
			</a>
	</div>
	<?php else: ?>
	<div class="image-placeholder">
			<a href="<?php echo esc_url( get_permalink( $p)); ?>" title="">
					<img src="<?php echo byo_catch_first_image( $p); ?>" />
			</a>

	</div>
<?php endif; ?>
