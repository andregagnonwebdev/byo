<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>
<div class="col-xs-12 col-md-12">
	<?php if ( byo_user_logged_in_product_already_bought()): ?>
		<?php	echo do_shortcode( get_field( 'payup_2', 'option'));	?>
	<?php else: ?>
		<?php	echo do_shortcode( get_field( 'free_trial_2', 'option'));	?>
	<?php endif; ?>
</div>
