<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>


<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "CreativeWork",
		"mainEntityOfPage": {
			"@type": "WebPage",
			"@id": "<?php echo get_permalink() ?>"
		},
		"headline": "<?php echo get_the_title() ?>",
		"image": "<?php echo get_the_post_thumbnail_url() ?>",
		"datePublished": "<?php echo get_the_term_list( $post->ID, 'date', '', ', ' ) ?>",
		"dateModified": "<?php echo get_the_term_list( $post->ID, 'date', '', ', ' ) ?>",
		"author": {
			"@type": "Person",
			"name": "<?php echo get_the_term_list( $post->ID, 'writer', '', ', ' ) ?>",
		},
		"publisher": {
			 "name": "Brew Your Own",
			 "@type": "Organization",
			 "logo": {
					"@type": "ImageObject",
					"url": "http://byostaging.wpengine.com/wp-content/uploads/logo-brew-your-own.png"
			 }
		},
		"description":
			"<?php echo wp_strip_all_tags(get_the_excerpt(), true) ?>",
		"isAccessibleForFree":
			"<?php echo byo_is_restricted() == false ? 'True' : 'False' ?>",
		"hasPart":
			{
			"@type": "WebPageElement",
			"isAccessibleForFree":
				"<?php echo byo_is_restricted() == false ? 'True' : 'False' ?>",
			"cssSelector" : "#paywall"
			}
		}
	</script>
