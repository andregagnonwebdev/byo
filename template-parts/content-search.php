<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>
<?php if (!empty($post)) : ?>
  <div id="post-<?php the_ID(); ?>" <?php post_class('search-result-post'); ?>>
  	<div class="row search-result">
  
  		<div class="hidden-xs col-xs-12 col-md-2" style="margin-top: 25px;">
  			<?php
  				global $post_type;
  				if ( 1 || $post_type == 'all')
  					byo_eyebrow_image_tag($post->ID)
  				?>
  
  			<?php if ( get_post_type() != 'mr-wizard') : ?>
  				<?php if ( has_post_thumbnail() ) : ?>
  				    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
  				        <?php the_post_thumbnail(); ?>
  				    </a>
  				<?php else: ?>
  					<div class="image-placeholder">
  							<a href="<?php echo esc_url( get_permalink( $post)); ?>" title="">
  									<img src="<?php  echo byo_catch_first_image( $post ); ?>" />
  							</a>
  					</div>
  				<?php endif; ?>
  			<?php endif; ?>
  		</div>
  
  		<div class="col-xs-12 col-md-10 content">
  			<header class="entry-header title">
  				<?php $post_type = get_post_type(); ?>
  				<?php the_title( sprintf( '<h2 class="entry-title search-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
  
  				<?php if ( $post) echo byo_restricted_tag_text(); ?>
  
  				<?php if ( 'post' === $post_type ) : ?>
  				<div class="entry-meta">
  					<?php byo_posted_on(); ?>
  				</div><!-- .entry-meta -->
  				<?php endif; ?>
  			</header><!-- .entry-header -->
  
  			<div class="entry-summary">
  				<?php the_excerpt(); ?>
  			</div><!-- .entry-summary -->
  
  			<footer class="entry-footer text-right">
  				<?php //byo_entry_footer(); ?>
  				<?php byo_custom_type_terms(); ?>
  
  			</footer><!-- .entry-footer -->
  		</div>
  	</div> <!-- end row -->
  	<hr />
  </div><!-- #post-<?php the_ID(); ?> -->
<?php endif; ?>
