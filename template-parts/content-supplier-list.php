<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<div class="row">
	<div class="content">
		<?php if ( !is_single()): ?>
			<div class="col-xs-12">
				<hr />
			</div>
			<div class="col-xs-12">
				<?php	the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );	?>
			</div>
		<?php endif; ?>

		<div class="col-xs-12">
			<?php echo get_field( "address"); ?>
			<?php echo ' ' . get_field( "address2"); ?>
		</div>
		<div class="col-xs-12">
			<strong><?php echo get_field( "city"); ?>, <?php echo get_field( "state"); ?></strong>	<?php echo get_field( "country"); ?>&nbsp;&nbsp;<?php echo get_field( "zipcode"); ?>
		</div>
		<div class="col-xs-12">
			<?php echo get_field( "phone"); ?>
			<?php if ( is_single()): ?>
				<?php $f = get_field( "fax"); ?>
				<?php echo ( $f) ? '<br />' . $f . ' FAX': ''; ?>
			<?php endif; ?>
	</div>
		<div class="col-xs-12">
			<a href="http://<?php echo get_field( "link"); ?>" target="_blank"><?php echo get_field( "link"); ?></a>
		</div>
		<div class="col-xs-12">
			<?php the_content(); ?>
		</div>
	</div>
</div>
