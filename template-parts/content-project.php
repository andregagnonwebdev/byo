<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="row" >
		<header class="entry-header">

			<?php get_template_part( 'template-parts/fragment', 'ad-units-sponsor-article'); ?>

			<div class="col-xs-12">
				<?php $obj = get_post_type_object( get_post_type() ); ?>
				<span class="eyebrow"><?php echo $obj->labels->singular_name; ?></span>

				<?php
					the_title( '<h1 class="entry-title">', '</h1>' );
				?>
			</div>
		</header>
	</div>

	<div class="row" >
	<div class="col-xs-12 col-md-8 col-lg-9">

	<div class="col-xs-12 col-md-12">
		<div class="entry-content">
			<?php the_content(); ?>
		</div>
	</div>

	<div class="col-xs-12 col-md-12">
		<!-- steps -->
		<?php if ( !byo_is_restricted() || byo_has_subscription() ): ?>
			<?php if ( have_rows('steps') ): ?>
		<?php //if( byo_has_subscription() == true && have_rows('steps') ): ?>
	    <?php while ( have_rows('steps') ) : the_row(); ?>
					<div class="row">
						<div class="col-xs-12">
							<?php the_sub_field('step'); ?>
						</div>
						<div class="col-xs-12">
							<img src="<?php the_sub_field('image'); ?>" >
						</div>
					</div>
	    <?php endwhile; ?>
		<?php else : ?>
				<p></p>
			<?php endif; ?>
		<?php endif; ?>

	</div>

	<div class="col-xs-12 col-md-12">
		<?php get_template_part( 'template-parts/content', 'writer' ); ?>
	</div>

	<div class="hidden col-xs-12 col-md-4">
		<!-- featured image -->
		<div class="row">
			<div class="col-xs-12">
				<?php if ( has_post_thumbnail() ) : ?>
	        <?php the_post_thumbnail(); ?>
				<?php else: ?>
					<p>photo placeholder</p>
				<?php endif; ?>
			</div>
			<!-- overview -->
			<div class="col-xs-12">
				<?php if ( $f = get_field( 'overview')): ?>
					<p><?php echo $f; ?></p>p>
				<?php elseif ( has_excerpt( )): ?>
						<?php the_excerpt(); ?>
				<?php else: ?>
					<p>overview placeholder</p>
				<?php endif; ?>
			</div>
		</div>
	</div>

			<?php
			// wp_link_pages( array(
			// 	'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'swg' ),
			// 	'after'  => '</div>',
			// ) );
		?>
		<?php get_template_part( 'template-parts/fragment', 'related-posts'); ?>

	<footer class="entry-footer">
		<?php get_template_part( 'template-parts/fragment', 'publisher-json-ld'); ?>

		<?php //byo_entry_footer(); ?>
	</footer><!-- .entry-footer -->
	</div>

	<div class="col-xs-12 col-md-4 col-lg-3 ad-units">
		<?php get_template_part( 'template-parts/fragment', 'ad-units-right-rail'); ?>
	</div>

	</div>


</article><!-- #post-<?php the_ID(); ?> -->

<?php get_template_part( 'template-parts/fragment', 'free-content-lead'); ?>
