<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>


	<div class="col-xs-12 col-md-9 front-publisher">
		<p class="red-heading text-center">FROM THE PUBLISHER</p>
		<h6 class="text-center clean visible-xs visible-sm"><?php echo get_field( 'publisher_title'); ?></h6>
		<h6 class="text-center hidden-xs hidden-sm"><?php echo get_field( 'publisher_title'); ?></h6>

		<div class="row">
			<div class="col-xs-12 col-md-3 col-lg-4">
				<img class="text-center" src="<?php echo get_field( 'publisher_image'); ?>" >
			</div>
			<div class="col-xs-12 col-md-9 col-lg-8">

				<p><?php echo get_field( 'publisher_message'); ?></p>
				<strong><?php echo get_field( 'publisher_name'); ?></strong>
			</div>
		</div>
	</div>
