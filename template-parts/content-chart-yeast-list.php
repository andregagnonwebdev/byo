<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<?php $obj = get_post_type_object( get_post_type() ); ?>
<?php $labNames = array(
' ',
'Brewferm',
'Coopers',
'Danstar',
'Fermentis',
'Muntons',
'Siebel Inst.',
'White Labs',
'Wyeast',
'Lallemand',
'East Coast Yeast',
'Mangrove Jack',
'Real Brewers Yeast',
);
$floccsName = array(
	'',
'Very Low',
'Low',
'Low/Med',
'Medium',
'Med/High',
'High',
'Very High',

);

?>
<div class="row">
	<div class="content">
		<div class="col-xs-2">
			<?php the_title(); ?>
		</div>
		<div class="col-xs-1">
			<?php echo get_field( "type"); ?>
		</div>
		<div class="col-xs-1">
			<?php echo $labNames[ get_field( "labs_id")]; ?>
		</div>
		<div class="col-xs-1">
			<?php echo $floccsName[ get_field( "floccs_id")]; ?>
		</div>
		<div class="col-xs-2">
			<?php $atten_low = get_field( "atten_low"); ?>
			<?php $attenNames = array( '' , 'Low', 'Medium', 'High', 'Very High' ); ?>
			<?php echo (( $atten_low > 0) ? ($atten_low.'-'.get_field( "atten_high").'%') : ( $attenNames[ ( absint( $atten_low )) / 10 ]) ); ?>
		</div>
		<div class="col-xs-2">
			<?php $temp_low = get_field( "temp_low"); ?>
			<?php echo ( ( $temp_low) ? ($temp_low.'-'.get_field( "temp_high").'°') : ( 'N/A') ); ?>
		</div>
		<div class="col-xs-3">
			<?php the_content(); ?>
		</div>
	</div>
</div>
