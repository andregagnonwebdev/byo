<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="col-xs-12 col-md-8 col-lg-8 recipes-content">
		<div class="row">
			<?php get_template_part( 'template-parts/fragment', 'recipes-content' ); ?>
		</div>
	</div>

	<div class="col-xs-12 col-md-4 col-lg-4">
		<div class="row">
			<?php get_template_part( 'template-parts/fragment', 'recipes-browse' ); ?>
			<?php //get_template_part( 'template-parts/fragment', 'recipes-filter' ); ?>
		</div>
		<div class="row">
			<?php get_template_part( 'template-parts/fragment', 'recipes-popular' ); ?>
			<?php //get_template_part( 'template-parts/fragment', 'recipes-filter' ); ?>
		</div>
	</div>

</article><!-- #post-<?php the_ID(); ?> -->
