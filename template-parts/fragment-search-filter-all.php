<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<div class="row swg-search-filter-sidebar filter--all">

	<form method="GET" action="<?php echo esc_url( home_url( '/' ) );?>" id="swg-sidebar-filter-form">
		<input type="hidden" name="s" value="<?php echo is_search()? get_search_query() : ''; ?>">
		<input type="hidden" name="post_type" value="<?php echo !empty($_GET['post_type']) ? $_GET['post_type'] : 'any'; ?>">
		<input type="hidden" name="orderby" class="swg-orderby-auto-pop" value="<?php echo !empty($_GET['orderby']) ? $_GET['orderby'] : 'relevance'; ?>">

  	<div class="col-xs-12 swg-filter-col">
  		<h6 class="clean searches swg-mobile-toggle">Filter</h6>
  		<div id="swg-filter-active-tags"></div>
  		<div class="row swg-filters-row">
  			<?php	$sq = is_search() ? get_search_query() : ''; ?>
  
  			<?php
    			// select the post types and taxonomies
    			$pt = !empty($_GET['post_type']) ? $_GET['post_type'] : '';
    			if (is_array($pt)) {
      			if (in_array('recipe', $pt)) {
        			$pt = 'any';
      			}
    			}
    			
    			$post_types = array();
    			
    			$taxonomies[] = 'beer-style';
    			
    			if ($pt == 'recipe' || empty($pt) || $pt == 'any' || $pt == 'all') {
      			$taxonomies[] = 'recipe-type';
    			}
    			
    			$taxonomies[] = 'topic';
    			
    			if ($pt == 'any' || empty($pt) || $pt == 'all') {
      			$post_types = array('any', 'article', 'recipe', 'mr-wizard', 'project');
    			}
    			else {
      			$post_types = array($pt);
    			}
  	    ?>
  
  			<div class="col-xs-12">
    			<?php byo_build_filters('', $post_types, $taxonomies); ?>
  			</div>
  		</div>
  	</div> <!-- end row -->
  </form>

</div> <!-- end row -->
