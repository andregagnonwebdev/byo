<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>


<div class="row swg-search-filter-sidebar filter--projects">

	<form method="GET" action="<?php echo esc_url( home_url( '/' ) );?>">
		<input type="hidden" name="s" value="<?php echo is_search()? get_search_query() : ''; ?>">
		<input type="hidden" name="post_type" value="project">

	<div class="col-xs-12 swg-filter-col">
		<h6 class="clean searches">Filter</h6>
		<div class="filter-active-tags"></div>
		<div class="row">
			<?php	$sq = is_search() ? get_search_query() : ''; ?>
			<?php if ( $sq): ?>
				<!--<div class="col-xs-12">
					For keyword "<?php echo $sq; ?>".
				</div>-->
			<?php endif; ?>

			<?php
			// select the post types
			$post_types = array('project');
			$taxonomies = array('topic',  );
	    ?>

			<div class="col-xs-12">
				<?php byo_build_filters( $post_type, $post_types, $taxonomies); ?>
			</div>
	</div> <!-- end row -->
</div>
</form>

</div> <!-- end row -->
