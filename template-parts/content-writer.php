<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<?php
  $writers = get_the_terms( $post->ID, 'writer');
  $name = array();
  $desc = array();
  if ( $writers) {
    foreach ($writers as $a) {
      //var_dump( $a);
      $name[] = $a->name;
      if ( $a->description)
        $desc[] = $a->description;
    }
  }
?>

<!-- author -->
<?php if ( $name && $name[ 0]): ?>

  <h4 class="sidebar-heading">Written by <?php echo $name[ 0]; ?></h4>
  <?php foreach ($desc as $d): ?>
    <div class="writer"><?php echo $d; ?></div>
  <?php endforeach; ?>
<?php else: ?>
  <p></p>
<?php endif; ?>
