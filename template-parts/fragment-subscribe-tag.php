<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>
<?php if ( is_page_template( 'template-bootcamp-event.php' ) || 'bootcamp' == get_post_type() ) return; ?>
<?php if ( is_page_template( 'template-nanocon-event.php' ) || 'nanocon' == get_post_type() ) return; ?>

<?php if ( byo_has_subscription() == false ): ?>
	<div class="subscribe-tag">
		<div class="container">
			<a href="/landing-page/byo-digital-membership/">
			  JOIN NOW <span class="arrow"><i class="fa fa-long-arrow-right"></i></span>
			</a>
		</div>
	</div>
<?php endif; ?>
