<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>


	<div class="col-xs-12 troubleshooting-popular">
		<h6 class="text-center clean searches">Popular Searches</h6>

		<?php if( have_rows('troubleshooting_popular') ): ?>

		<div class="row">

		<?php while( have_rows('troubleshooting_popular') ): the_row();

			// vars
			$p = get_sub_field('mr_wizard_content');
			setup_postdata( $p );
			$ID = $p->ID;
			$title = $p->post_title;
			$content = $p->post_content;
			?>
			<div class="col-xs-12 content">
				<?php  // var_dump($p); ?>
				<div class="title">
					<a href="<?php echo esc_url( get_permalink( $p)); ?>" title="">
						<h2><?php echo $title; ?></h2>
					</a>
					<?php echo byo_restricted_tag_text( $ID); ?>
				</div>
				<p><?php echo get_the_excerpt(); ?></p>
			</div>
			<?php wp_reset_postdata(); ?>

		<?php endwhile; ?>

		</div>

<?php endif; ?>
	</div>
