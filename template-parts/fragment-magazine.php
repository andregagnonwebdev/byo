<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>


	<div class="col-xs-12 front-magazine">
		<h6 class="text-center clean visible-sm visible-xs"><?php echo get_field( 'magazine_title'); ?></h6>
		<h6 class="text-center hidden-xs hidden-sm"><?php echo get_field( 'magazine_title'); ?></h6>

		<?php if( have_rows('magazine_content') ): ?>
		<?php $subscriber = byo_has_subscription(); ?>
		<?php $count = 1; ?>

		<div class="row">
		<?php $total = count( get_field('magazine_content')); ?>

		<?php while( have_rows('magazine_content') ): the_row();

			// vars
			global $p;
			$p = get_sub_field('content_type');
			setup_postdata( $p );
			$ID = $p->ID;
			$title = $p->post_title;
			$content = $p->post_content;
			?>
			<div class="col-xs-12 col-md-6 col-lg-3 content">
				<?php  // var_dump($p); ?>
				<?php byo_eyebrow_image_tag( $ID) ?>

				<?php get_template_part( 'template-parts/fragment', 'featured-image' ); ?>

				<div class="title">
					<?php echo byo_restricted_tag_text( $ID); ?>
					<a href="<?php echo esc_url( get_permalink( $p)); ?>" title="">
						<h2 style="display:inline;"><?php echo $title; ?></h2>
					</a>
				</div>

				<p><?php echo get_the_excerpt(); ?></p>
			</div>
			<?php wp_reset_postdata(); ?>

		<?php
			//if ( $count++ % 4 == 0)
			if ( $count++ % 4 == 0 && $count != ( $total + 1 ))
			{
				if ( 0 && $subscriber == false)
					break;
				else {
					echo '</div><div class="clear:both"></div><div class="row">';
				}
			}
		?>
		<?php endwhile; ?>


		<!-- <div class="col-xs-12 col-md-6 col-lg-3">
			<img class="text-center" src="<?php echo get_field( 'ad_block'); ?>" >

		</div> -->
		</div>

<?php endif; ?>
	</div>
