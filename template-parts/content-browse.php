<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<div class="row">
	<div class="col-xs-12">
		<div class="row">
			<header classx="page-header"></header><!-- .page-header -->
      
			<!-- Beer type (recipe type) and style -->
			<?php
  			$filter_data = swg_browse_search_build_filter_data(array('recipe'), array('recipe-type'));
  			$all_recipe_types = array();
  			$selected_recipe_types = array();
  			$swg_browse_beer_type_class = 'row swg-browse-row';
  			if ($filter_data['taxonomies']['recipe-type']['expanded']) {
    			$swg_browse_beer_type_class .= ' expanded';
  			}
  		?>
  		
			<div id="swg-browse-search--browse--beer-type-style" class="<?php echo $swg_browse_beer_type_class; ?>">
  			<div class="row">
    			<div class="col-xs-12 col-md-12 col-lg-12">
            <h3 class="swg-browse-search--browse--section-header">Beer Type &amp; Style</h3>
    			</div>
  			</div>
  			<div class="items">
    		  <div class="col-xs-12 col-md-4 col-lg-4">
      		  <form id="swg-browse-search--browse--recipe-type-select" method="get" action="browse">
        		  <ul>
          		  <?php foreach ($filter_data['taxonomies']['recipe-type']['terms'] as $term_id => $term_data): ?>
          		    <?php $all_recipe_types[] = $term_data['slug']; ?>
          		    <?php if ($term_data['checked']) $selected_recipe_types[$term_id] = $term_data['slug']; ?>
          		    <?php echo _swg_browse_search_filter_checkbox_li('recipe-type', $term_data['slug'], $term_id, $term_data['name'], 0, 'browse-recipe-type-filter'); ?>
          		  <?php endforeach; ?>
        		  </ul>
      		  </form><!-- /#swg-browse-search--browse--recipe-type-select -->
    		  </div><!-- /.col-xs-12.col-md-4.col-lg-4 -->
    		  
    		  <div id="swg-browse-search--browse--beer-styles" class="col-xs-12 col-md-8 col-lg-8">
        		<?php
          		$top_level_beer_styles = swg_browse_search_get_parent_term_ids('beer-style');
          		$beer_styles = array('terms' => array(), 'parents' => array());
          		
          		$post_list = get_posts(
                array(
                  'showposts' => -1,
                  'post_type' => 'recipe',
                  'tax_query' => array(
                    array(
                      'taxonomy' => 'recipe-type',
                      'field' => 'slug',
                      'terms' => $all_recipe_types,
                    )),
                  'orderby' => 'term_order',
                  'order' => 'ASC'
                )
              );
              
              foreach ($post_list as $post) {
                $recipe_type_terms = get_the_terms($post->ID, 'recipe-type');
                $terms = get_the_terms($post->ID, 'beer-style');
                if (empty($terms)) continue;
                
                $rt_term_ids = array();
                if (!empty($recipe_type_terms)) {
                  foreach ($recipe_type_terms as $rt_term) {
                    $rt_term_ids[$rt_term->term_id . $rt_term->slug] = $rt_term->term_id;
                  }
                }
                
                foreach ($terms as $beer_style) {
                  // Add the beer style term to the terms array if it's not already there
                  if (!isset($beer_styles['terms'][$beer_style->term_id])) $beer_styles['terms'][$beer_style->term_id] = $beer_style;
                  
                  if (!isset($beer_styles['terms'][$beer_style->term_id]->rt_term_ids)) {
                    $beer_styles['terms'][$beer_style->term_id]->rt_term_ids = $rt_term_ids;
                  }
                  else {
                    $beer_styles['terms'][$beer_style->term_id]->rt_term_ids = array_merge($beer_styles['terms'][$beer_style->term_id]->rt_term_ids, $rt_term_ids);
                  }
                  
                  // If the beer style term has a parent term ...
                  if (!empty($beer_style->parent)) {
                    // Add the parent term to the parents array if it's missing
                    if (empty($beer_styles['parents'][$beer_style->parent])) {
                      $beer_styles['parents'][$beer_style->parent] = array(
                        'term' => get_term($beer_style->parent, 'beer-style'),
                        'children' => array(),
                      );
                    }
                    // Add the child term ID to the parent's children array
                    if (!in_array($beer_style->term_id, $beer_styles['parents'][$beer_style->parent]['children'])) {
                      $beer_styles['parents'][$beer_style->parent]['children'][] = $beer_style->term_id;
                    }
                  }
                }
              }
              
              foreach ($top_level_beer_styles as $parent_beer_style_id) {
                if (empty($beer_styles['parents'][$parent_beer_style_id])) {
                  //echo '<p>No data for ' . $parent_beer_style_id . '</p>';
                  continue;
                }
                
                $p_term = $beer_styles['parents'][$parent_beer_style_id];
                echo '<div class="swg-browse-search--beer-style-group col-xs-12 col-md-6">';
                
                if (!empty($p_term['children'])) {
                  echo '<strong class="swg-beer-family-name">' . $p_term['term']->name . '</strong>';
                  echo '<ul>';
                  
                  foreach ($p_term['children'] as $child_id) {
                    if (empty($beer_styles['terms'][$child_id])) continue;
                    $c_term = $beer_styles['terms'][$child_id];
                    $rt_term_ids = '';
                    
                    // Recipe type term IDs
                    if (!empty($c_term->rt_term_ids)) {
                      foreach ($c_term->rt_term_ids as $rt_term_id) {
                        $rt_term_ids .= ':' . $rt_term_id . ':';
                      }
                    }
                    
                    $url = '/?s=&beer-style%5B' . $c_term->slug . '%5D=' . $c_term->slug . '&post_type=recipe';
                    
                    echo '<li class="swg-rt-bs-link-item"><a href="' . $url . '" data-original-url="' . $url . '" data-rt-terms="' . $rt_term_ids . '">' . $c_term->name . '</a></li>';
                  }
                  
                  echo '</ul>';
                }
                else {
                  echo '<p>' . $p_term->name . '</p>';
                }
                
                echo '</div>';
              }
            ?>
    		  </div><!-- /#swg-browse-search--browse--beer-styles /.col-xs-12 /.col-md-8 /.col-lg-8 -->
  			</div><!-- /.items -->
			</div><!-- /#swg-browse-search--browse--beer-type-style -->
			
			<?php
  			$swg_browse_mag_class = 'row swg-browse-row';
  			if (!empty($_GET['magazine_year'])) {
    			$swg_browse_mag_class .= ' expanded';
  			}
  		?>
			<div id="swg-browse-search--browse--magazine-archive" class="<?php echo $swg_browse_mag_class ; ?>">
  			<div class="row">
    			<div class="col-xs-12 col-md-12 col-lg-12">
            <h3 class="swg-browse-search--browse--section-header">Magazine Issue Library</h3>
    			</div>
  			</div><!-- /.row -->
  			
  			<div class="row items">
    			<?php
      			$terms_by_year = swg_browse_search_magazine_archive_data();
      			$current_year = date("Y");
      			$year_to_show = !empty($_GET['magazine_year']) ? $_GET['magazine_year'] : 'special-issues';
    			?>
      		
      		<div id="swg-browse--magazine-year-links" class="row swg-desktop-only">
        		<a class="year-nav-arrow year-nav-arrow--prev"><</a>
        		<ul id="swg-browse--magazine-year-links-wrapper">
        			<?php foreach ($terms_by_year as $year => $terms): ?>
        			  <?php $y_class = 'swg-mag-year-slider-link'; ?>
        			  <?php $y_class .= ($year_to_show == $year) ? ' active' : ''; ?>
        			  <li class="swg-slider-link-item"><a href="<?php echo get_permalink(143); ?>?magazine_year=<?php echo $year; ?>#swg-browse-search--browse--magazine-archive" class="<?php echo $y_class; ?>" data-year="<?php echo $year; ?>"><?php echo ($year == 'special-issues') ? 'Special Issues' : $year; ?></a></li>
        			<?php endforeach; ?>
        		</ul>
        		<a class="year-nav-arrow year-nav-arrow--next">></a>
      		</div>
      		
      		<form id="swg-browse-search-magazine-select-form" method="get" action="/browse" class="swg-mobile-only">
        		<label for="swg-browse-search-magazine-year-select">Select a year</label>
        		<select id="swg-browse-search-magazine-year-select" name="magazine_year" class="">
          		<?php foreach ($terms_by_year as $year => $terms): ?>
          		  <?php $selected = ($year_to_show == $year) ? ' selected="selected"' : ''; ?>
          		  <option value="<?php echo $year; ?>"<?php echo $selected; ?>><?php echo $year; ?></option>
          		<?php endforeach; ?>
        		</select>
      		</form>
    			
    			<?php foreach ($terms_by_year as $year => $terms): ?>
    			  <?php
      			  $classes = 'swg-browse--year-container row';
        			if ($year_to_show == $year) $classes .= ' active';
      		  ?>
            
    			  <div class="<?php echo $classes; ?>" data-year="<?php echo $year; ?>">
      			  <?php foreach ($terms as $term_id => $term_data): ?>
      			    <div class="col-xs-6 col-sm-4 col-md-3 swg-browse--magazine-archive-item">
        			    <a href="<?php echo $term_data['url']; ?>" alt="<?php echo $term_data['alt']; ?>">
          			    <div class="swg-browse--magazine-cover">
            			    <?php if (!empty($term_data['description'])): ?>
            			      <?php echo $term_data['description']; ?>
                      <?php else: ?>
                        <img src="<?php echo get_template_directory_uri(); ?>/swg-browse-search/images/placeholder.png" width="263" height="353" alt="A dark grey placeholder image" />
                      <?php endif; ?>
          			    </div>
          			    <span class="swg-browse--magazine-title"><?php echo $term_data['name']; ?></span></a>
        			    </a>
      			    </div>
      			  <?php endforeach; ?>
    			  </div>
    			<?php endforeach; ?>
  			</div><!-- /.row /.items -->
			</div><!-- /#swg-browse-search--browse--magazine-archive /.row -->
			
			<div id="swg-browse-search--browse--topics" class="row swg-browse-row">
  			<div class="row">
    			<div class="col-xs-12 col-md-12 col-lg-12">
            <h3 class="swg-browse-search--browse--section-header">Topic</h3>
    			</div>
  			</div><!-- /.row -->
  			
  			<div class="row items">
    			<?php
      			$col_limit = 14;
      			$counter = -1;
      			
      			$terms = get_terms(
      			  array(
        			  'taxonomy' => 'topic',
                'orderby' => 'name',
                'order' => 'ASC',
              )
      			);
      			
      			$html = '<div class="col-xs-12 col-md-3"><ul>';
      			foreach ($terms as $term) {
        			$posts_count = 0;
        			
        			foreach (array('recipe', 'project', 'article', 'mr-wizard') as $post_type) {
          			$posts_count += _swg_browse_search_post_count_by_term('topic', $term->slug, $post_type);
        			}
        			
        			$counter++;
        			if ($counter == $col_limit) {
        			  $html .= '</ul></div><div class="col-xs-12 col-md-3"><ul>';
        			  $counter = 0;
        			}
        			
        			$html .= '<li><a href="/?s=&topic%5B' . $term->slug . '%5D=' . $term->slug . '&post_type=any">' . $term->name . ' (' . $posts_count . ')</a></li>';
      			}
      			$html .= '</ul></div>';
      			
      			echo $html;
      		?>
  			</div>
			</div>
		</div>
	</div>
</div>
