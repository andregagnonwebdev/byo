<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>


	<div class="col-xs-12 recipes-popular">
		<h6 class="text-center clean searches">Popular Searches</h6>

		<?php if( have_rows('recipes_popular') ): ?>

		<div class="row">

		<?php while( have_rows('recipes_popular') ): the_row();

			// vars
			$p = get_sub_field('recipe_content');
			//setup_postdata( $p );
			$ID = $p->ID;
			$title = $p->post_title;
			$content = $p->post_content;
			$overview = get_field( 'overview', $ID);
			?>
			<div class="col-xs-12 content">
				<?php  // var_dump($p); ?>
				<div class="title">
					<a href="<?php echo esc_url( get_permalink( $p)); ?>" title="">
						<h2><?php echo $title; ?></h2>
					</a>
					<span><?php echo byo_restricted_tag_text( $ID); ?></span>
				</div>
				<?php if( 0 && $overview): ?>
					<p><?php echo $overview; ?></p>
				<?php else: ?>
					<p><?php echo get_the_excerpt( $p ); ?></p>
				<?php endif; ?>
			</div>
			<?php //wp_reset_postdata(); ?>

		<?php endwhile; ?>

		</div>

<?php endif; ?>
	</div>
