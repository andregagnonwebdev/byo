<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<?php if ( 1 || byo_has_subscription() == false ): ?>
	<div class="col-xs-12 col-md-12 optinmonster-col">
		<?php	echo do_shortcode( get_field( 'email_signup_3', 'option'));	?>
	</div>
<?php endif; ?>
