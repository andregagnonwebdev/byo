<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_content();
		?>

		<?php
		global $byo_product_id;
		$byo_product_id = '';
		global $byo_coupon;
		$byo_coupon = '';
		global $byo_shortcode;
		$byo_shortcode = '';
		?>
		<?php $functionality = get_field( 'function_to_use'); ?>
		<!-- <?php echo $functionality; ?>
		<?php $fobj = get_field_object( 'function_to_use'); ?>
		<?php var_dump($fobj); ?> -->

		<?php if ( $functionality == 'woo:WooCommerce Subscription'
			|| $functionality == 'woo'): ?>

			<p><?php //var_dump( get_field( 'woocommerce_product') ); ?></p>
			<?php
				$product = get_field( 'woocommerce_product');
				$coupon = get_field( 'woocommerce_coupon');
				if ( $product) {
					$byo_product_id = $product->ID;
					$byo_coupon = $coupon;
				}
				// else
				// 	$byo_product_id = '';
			?>
		<?php else: ?>
			<?php	$byo_shortcode = get_field( 'optin_monster_form'); ?>
		<?php endif; ?>
		<?php	$byo_shortcode = get_field( 'optin_monster_form'); ?>

		<!-- <p><?php echo 'product id: '. $byo_product_id; ?></p>
		<p><?php echo 'shortcode id: '. $byo_shortcode; ?></p> -->

	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->
