<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package byo
 */

?>

	</div><!-- #content -->
</div> <!-- end container -->
</div> <!-- end container-fluid -->

<footer id="colophon" class="site-footer">

	<div class="container-fluid footer-menu">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-6 col-lg-3">
				<h3>Contact</h3>
				<p class="contact">
					<?php echo get_field( 'contact_info', 'option'); ?>
				</p>
				<?php
				$menu_args = array(
				"theme_location" => "footer-contact",
				"container_class" => "",
				"menu_class" => "nav navbar-nav",
				"menu_id" => "footer-contact-menu",
				);
				wp_nav_menu( $menu_args);
				?>
			</div>

			<div class="col-xs-12 col-md-6 col-lg-3">
				<h3>Navigate</h3>
				<?php
				$menu_args = array(
				"theme_location" => "footer-navigate",
				"container_class" => "",
				"menu_class" => "nav navbar-nav",
				"menu_id" => "footer-1-menu",
				);
				wp_nav_menu( $menu_args);
				?>
			</div>

			<div class="clearfix visible-md-block"></div>

			<div class="col-xs-12 col-md-6 col-lg-3">
				<h3>Customers</h3>
				<?php
				$menu_args = array(
				"theme_location" => "footer-subscribers",
				"container_class" => "",
				"menu_class" => "nav navbar-nav",
				"menu_id" => "footer-subscribe-menu",
				);
				wp_nav_menu( $menu_args);
				?>
				<h3>Advertising</h3>
				<?php
				$menu_args = array(
				"theme_location" => "footer-advertisers",
				"container_class" => "",
				"menu_class" => "nav navbar-nav",
				"menu_id" => "footer-advertise-menu",
				);
				wp_nav_menu( $menu_args);
				?>

			</div>


			<div class="col-xs-12 col-md-6 col-lg-3 ad-units-footer text-center">
				<?php get_template_part( 'template-parts/fragment', 'ad-units-btf'); ?>
			</div>

		</div><!-- .site-info -->
	</div> <!-- end container -->
	</div> <!-- end container -->

	<div class="container-fluid footer-bottom">
	<div class="container">
		<div class="row site-info">
			<div class="visible-xs visible-sm col-xs-12 text-center">
					<?php if ( $s = byo_get_theme_mod( 'byo-copyright-message')): ?>
						<div class="copyright"><?php echo $s ?></div>
					<?php endif; ?>
			</div>
			<div class="hidden-xs hidden-sm col-md-3 col-lg-4 text-right">
					<?php if ( $s = byo_get_theme_mod( 'byo-copyright-message')): ?>
						<div class="copyright"><?php echo $s ?></div>
					<?php endif; ?>
			</div>
			<div class="col-xs-12 col-md-8 col-lg-7 text-center">
					<?php
					$menu_args = array(
					"theme_location" => "footer-bottom",
					"container_class" => "",
					"menu_class" => "nav navbar-nav",
					"menu_id" => "footer-2-menu",
					);
					wp_nav_menu( $menu_args);
					?>
			</div>
			<div class="hidden-xs hidden-sm col-md-1 col-lg-1 text-right">
				&nbsp;
			</div>

		</div>
	</div> <!-- end container -->
	</div> <!-- end container -->

</footer><!-- #colophon -->


</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
