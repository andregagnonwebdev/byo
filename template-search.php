<?php
/**
 * Template Name: Search
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main col-xs-12 col-md-12">

			<div class="row">
				<div class="col-xs-12 col-md-9">
					<div class="form-page">
						<?php get_template_part( 'searchform', $post_type ); ?>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<p><br /></p>
				</div>

				<div class="col-xs-12">
					<p><br />Or if you'd rather browse ... </p>
					<h2>Browse by Type</h2>
					<p class="my_term-archive">
					<a href="/?s=&post_type=article">Articles</a>  &middot;
					<a href="/?s=&post_type=project">Projects</a>  &middot;
					<a href="/?s=&post_type=recipe">Recipes</a>  &middot;
					<a href="/?s=&post_type=mr-wizard">Troubleshooting - Mr. Wizard</a>
					</p>

					<h2>Browse by Category</h2>
					<h3>Beer Style</h3>
						<?php byo_build_index_taxonomy_2( 'beer-style', 'recipe'); ?>
					<h3>Recipe Type</h3>
						<?php byo_build_index_taxonomy_2( 'recipe-type', 'recipe'); ?>
					<h3>Topic</h3>
						<?php byo_build_index_taxonomy_2( 'topic', 'article'); ?>
					<h3 id="source-issue">Source Issue</h3>
						<?php byo_build_index_taxonomy_2( 'date', 'all'); ?>
					<!-- <h3>Writer</h3>
						<?php //byo_build_index_taxonomy( 'writer'); ?> -->

				</div>
			</div>


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
