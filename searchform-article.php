<?php
/*
 * Search form
 *
 * @package mind
 */
?>
<?php	$sq = is_search() ? get_search_query() : ''; ?>
<?php if ( !is_search() || $sq): ?>
  <div class="form-page">
   <form class="form-inline search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
     <div class="form-group">
  		 <span class="screen-reader-text">Search for:</span>
       <input type="search" name="s" class="form-control search-field" id="search" placeholder="Search in articles &hellip;" value="<?php echo is_search()? get_search_query() : ''; ?>">
       <input type="hidden" name="post_type" value="article">
     </div>
  	 	<button type="submit" class="btn btn-default search-submit"><i class="fa fa-search"></i> SEARCH </button>
   </form>
  </div>
<?php endif; ?>
