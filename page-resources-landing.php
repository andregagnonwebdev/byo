<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package byo
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main col-xs-12 col-md-9">
			<div class="row">
				<div class="col-xs-12">
					<span class="eyebrow">Resources</span>
					<h1><?php the_title() ?></h1>
				</div>

		<?php
		if ( have_posts() ) : the_post(); ?>
			<div class="col-xs-12">

				<header class="page-header">
					<?php echo wpautop( get_the_content()); ?>
				</header><!-- .page-header -->
		</div>
		<?php endif; ?>


			<?php
			$query = new WP_Query( 'post_type=resource&order=ASC&orderby=title');

			if ( $query->have_posts() ) :

			/* Start the Loop */
			while ( $query->have_posts() ) : $query->the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				//get_template_part( 'template-parts/content', get_post_type() );
				get_template_part( 'template-parts/content', 'resource-list');

			endwhile;

			//the_posts_navigation(array( 'prev_text' =>'More', 'next_text' => 'Previous'));

			endif; ?>

			</div>
		</main><!-- #main -->
		<div class="col-xs-12 col-md-3">
			<div class="ad-units">
				<?php get_template_part( 'template-parts/fragment', 'ad-units-right-rail'); ?>
			</div>
			<?php get_template_part( 'template-parts/content', 'resource-link'); ?>
		</div>

	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
