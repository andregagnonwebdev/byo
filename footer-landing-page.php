<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package byo
 */

?>

	</div><!-- #content -->
</div> <!-- end container -->
</div> <!-- end container-fluid -->

<footer id="colophon" class="site-footer">

	<div class="container-fluid footer-bottom">
	<div class="container">
		<div class="row site-info">
			<div class="visible-xs visible-sm col-xs-12 text-center">
					<?php if ( $s = byo_get_theme_mod( 'byo-copyright-message')): ?>
						<div class="copyright"><?php echo $s ?></div>
					<?php endif; ?>
			</div>
			<div class="hidden-xs hidden-sm col-md-3 col-lg-4 text-right">
					<?php if ( $s = byo_get_theme_mod( 'byo-copyright-message')): ?>
						<div class="copyright"><?php echo $s ?></div>
					<?php endif; ?>
			</div>
			<div class="col-xs-12 col-md-8 col-lg-7 text-center">
					<?php
					$menu_args = array(
					"theme_location" => "footer-bottom",
					"container_class" => "",
					"menu_class" => "nav navbar-nav",
					"menu_id" => "footer-2-menu",
					);
					wp_nav_menu( $menu_args);
					?>
			</div>
			<div class="hidden-xs hidden-sm col-md-1 col-lg-1 text-right">
				&nbsp;
			</div>

		</div>
	</div> <!-- end container -->
	</div> <!-- end container -->

</footer><!-- #colophon -->


</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
