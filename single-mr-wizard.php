<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package SWG
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main col-xs-12 col-md-12">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			//the_post_navigation();

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
		<?php //get_template_part( 'template-parts/fragment', 'subscribe' ); ?>

	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
