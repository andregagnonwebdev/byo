<?php
/**
 * Template Name: Index
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package byo
 */

get_header(); ?>

<div  class="col-xs-12">
	<div  class="row">
		<header class="entry-header">
			<?php get_template_part( 'template-parts/fragment', 'ad-units-sponsor-article'); ?>
		</header>
	</div>
</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main col-xs-12 col-md-9">
			<div class="row">


				<?php 	if ( have_posts() ) : the_post(); ?>
					<div class="col-xs-12">
						<h1><?php the_title() ?></h1>
						<!-- <?php echo wpautop( get_the_content()); ?> -->
					</div>
				<?php endif; ?>

				<div class="col-xs-12">
					<h2>Browse by Type</h2>
					<p class="my_term-archive">
					<a href="/article">Articles</a>  &middot;
					<a href="/project">Projects</a>  &middot;
					<a href="/recipe">Recipes</a>  &middot;
					<a href="/mr-wizard">Troubleshooting - Mr. Wizard</a>
					</p>

					<h2>Browse by Category</h2>
					<h3>Beer Style</h3>
						<?php byo_build_index_taxonomy( 'beer-style'); ?>
					<h3>Recipe Type</h3>
						<?php byo_build_index_taxonomy( 'recipe-type'); ?>
					<h3>Topic</h3>
						<?php byo_build_index_taxonomy( 'topic'); ?>
					<h3>Source Issue</h3>
						<?php byo_build_index_taxonomy( 'date'); ?>
					<h3>Writer</h3>
						<?php byo_build_index_taxonomy( 'writer'); ?>

				</div>
			</div>
		</main><!-- #main -->
		<div class="col-xs-12 col-md-3">
			<div class="ad-units">
				<?php get_template_part( 'template-parts/fragment', 'ad-units-right-rail'); ?>
			</div>
		</div>
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
