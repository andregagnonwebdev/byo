<?php
  $post = get_post();
  
  if (!empty($post)):
    $data_terms = '';
    foreach (array('recipe-type', 'beer-style', 'topic') as $post_tax) {
      $post_terms = get_the_terms(get_the_ID(), $post_tax);
      if ($post_terms) {
        foreach ($post_terms as $post_term) {
          if (!empty($data_terms)) $data_terms .= ':';
          $data_terms .= $post_term->term_id;
        }
      }
    }
    
    global $post_type;
?>
  <li class="swg-post-terms-data-wrapper" data-terms="<?php echo $data_terms; ?>">
    <div id="post-<?php the_ID(); ?>" <?php post_class('search-result-post'); ?>>
      <div class="swg-alm-relevanssi-count" style="display: none;" data-hits-count="<?php echo $alm_found_posts; ?>"></div>
      <div class="row search-result">
        <div class="hidden-xs col-xs-12 col-md-2">
          <?php
            if ( 1 || $post_type == 'all') byo_eyebrow_image_tag($post->ID);
    		  ?>
    		  <?php if ( get_post_type() != 'mr-wizard') : ?>
    				<?php if ( has_post_thumbnail() ) : ?>
    				    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
    				        <?php the_post_thumbnail(); ?>
    				    </a>
    				<?php else: ?>
    					<div class="image-placeholder">
    							<a href="<?php echo esc_url( get_permalink($post)); ?>" title="">
    									<img src="<?php  echo byo_catch_first_image($post ); ?>" />
    							</a>
    					</div>
    				<?php endif; ?>
    			<?php endif; ?>
        </div>
        <div class="col-xs-12 col-md-10 content">
          <header class="entry-header title">
            <?php $post_type = get_post_type(); ?>
            <?php the_title( sprintf( '<h2 class="entry-title search-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
            
            <?php if ( $post) echo byo_restricted_tag_text($post->ID); ?>
            
            <?php if ( 'post' === $post_type ) : ?>
              <div class="entry-meta">
                <?php byo_posted_on(); ?>
              </div><!-- .entry-meta -->
            <?php endif; ?>
          </header>
          
          <div class="entry-summary">
            <?php the_excerpt(); ?>
          </div>
          
          <footer class="entry-footer text-right">
            <?php
              echo get_the_term_list($post->ID, 'topic', 'Topic(s): ', ', ' , '<br>');
              echo get_the_term_list($post->ID, 'beer-style', 'Beer Style(s): ', ', '  , '<br>');
              echo get_the_term_list($post->ID, 'recipe-type', 'Recipe Type(s): ', ', '  , '<br>');
            ?>
          </footer>
        </div>
      </div>
    </div>
    
    <p>Currently viewing item #<?php echo $alm_item; ?> of <?php echo $alm_found_posts; ?></p>
    
    <hr />
  </li>
<?php endif; ?>