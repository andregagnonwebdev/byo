<?php
/**
 * Template Name: Thank You
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package byo
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main col-xs-12 col-md-9">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->

		<div class="col-xs-12 col-md-3">
			<div class="ad-units">
				<?php get_template_part( 'template-parts/fragment', 'ad-units-right-rail'); ?>
			</div>
		</div>

	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
