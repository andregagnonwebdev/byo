<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package byo
 */

get_header(); ?>
<?php
if ( is_checkout() == false && is_cart() == false && is_woocommerce() == false) {
	$cols = 9;
}
else {
	$cols = 12;
}
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main col-xs-12 col-md-<?php echo $cols ?>">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
		<?php if ( $cols == 9): ?>
		<div class="col-xs-12 col-md-3">
			<div class="ad-units">
				<?php get_template_part( 'template-parts/fragment', 'ad-units-right-rail'); ?>
			</div>
		</div>
		<?php endif; ?>
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
