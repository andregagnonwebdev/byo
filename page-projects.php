<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package byo
 */

$post_type = 'project';
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main col-xs-12">
			<div class="row">
				<div class="col-xs-12">
					<span class="eyebrow">Projects</span>
					<h1>Homebrew Projects</h1>
				</div>
				<div class="col-xs-12">
					<?php include(locate_template('searchform-all.php')); ?>
				</div>
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'projects' );

			endwhile; // End of the loop.
			?>
		</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
