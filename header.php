<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package byo
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>

<?php if ( !WP_DEBUG): ?>
	<!-- WP_DEBUG false -->
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-5522549-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-5522549-2');
</script>
<?php else: ?>
	<!-- WP_DEBUG true -->
<?php endif; ?>

<?php //if ( 1): ?>
<?php if ( !WP_DEBUG): ?>
<!-- Google DoubleClick for Publishers -->

<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>

<?php $post_type = get_post_type(); ?>

<script>
  googletag.cmd.push(function() {

		// home page
		<?php if ( is_front_page()): ?>
			googletag.defineSlot('/52443766/byo_home_center-left_200x200', [200, 200], 'div-gpt-ad-1507727668641-7').addService(googletag.pubads());
	    googletag.defineSlot('/52443766/byo_home_center-right_200x200', [200, 200], 'div-gpt-ad-1507727668641-8').addService(googletag.pubads());
	    googletag.defineSlot('/52443766/byo_home_left_200x200', [200, 200], 'div-gpt-ad-1507727668641-9').addService(googletag.pubads());
	    googletag.defineSlot('/52443766/byo_home_right_200x200', [200, 200], 'div-gpt-ad-1507727668641-10').addService(googletag.pubads());
		<?php endif; ?>

		// atf
		googletag.defineSlot('/52443766/byo_content_atf_468x60', [468, 60], 'div-gpt-ad-1507727668641-1').addService(googletag.pubads());
		// btf
		googletag.defineSlot('/52443766/byo_content_btf_200x100', [200, 100], 'div-gpt-ad-1507727668641-2').addService(googletag.pubads());
		// right sidebar
		googletag.defineSlot('/52443766/byo_content_right-top_200x200', [200, 200], 'div-gpt-ad-1507727668641-5').addService(googletag.pubads());
    googletag.defineSlot('/52443766/byo_content_right-middle_200x100', [200, 100], 'div-gpt-ad-1507727668641-4').addService(googletag.pubads());
		googletag.defineSlot('/52443766/byo_content_right-bottom_200x200', [200, 200], 'div-gpt-ad-1507727668641-3').addService(googletag.pubads());

		// custom post types
		googletag.defineSlot('/52443766/byo_hops_spons_680x80', [680, 80], 'div-gpt-ad-1507727668641-11').addService(googletag.pubads());
		googletag.defineSlot('/52443766/byo_yeast_spons_680x80', [680, 80], 'div-gpt-ad-1507727668641-18').addService(googletag.pubads());
    googletag.defineSlot('/52443766/byo_grains_spons_680x80', [680, 80], 'div-gpt-ad-1507727668641-6').addService(googletag.pubads());
		googletag.defineSlot('/52443766/byo_articles_spons_680x80', [680, 80], 'div-gpt-ad-1507727668641-0').addService(googletag.pubads());
		<?php if ( $post_type == 'recipe'): ?>
			googletag.defineSlot('/52443766/byo_recipes_spons_680x80', [680, 80], 'div-gpt-ad-1507727668641-14').addService(googletag.pubads());
		<?php endif; ?>

		<?php if ( $post_type == 'mr-wizard'): ?>
			googletag.defineSlot('/52443766/byo_wizard_spons_680x80', [680, 80], 'div-gpt-ad-1507727668641-17').addService(googletag.pubads());
		<?php endif; ?>

		<?php if ( $post_type == 'project'): ?>
			googletag.defineSlot('/52443766/byo_projects_spons_680x80', [680, 80], 'div-gpt-ad-1507727668641-13').addService(googletag.pubads());
		<?php endif; ?>

		// pages
    googletag.defineSlot('/52443766/byo_newbrew_spons_680x80', [680, 80], 'div-gpt-ad-1507727668641-12').addService(googletag.pubads());

    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });


</script>
<?php endif; ?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRQP8C2');</script>
<!-- End Google Tag Manager -->

</head>

<body <?php body_class(); ?>>

	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WRQP8C2"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php if ( WP_DEBUG): ?>
<?php 	//error_log( 'test error log'); ?>
<?php endif; ?>

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'byo' ); ?></a>

<div class="container-fluid tophat hidden-xs hidden-sm">
	<div class="container">
		<div class="row">
		<div class="col-md-7 text-left">
			<?php
			$menu_args = array(
			"theme_location" => "tophat-left",
			"container_class" => "",
			"menu_class" => "nav navbar-nav",
			"menu_id" => "tophat-left",
			);
			wp_nav_menu( $menu_args);
			?>
		</div>
		<div class="col-md-5 text-right">
			<?php
			$menu_args = array(
			"theme_location" => "tophat-right",
			"container_class" => "",
			"menu_class" => "nav navbar-nav",
			"menu_id" => "tophat-right",
			);
			wp_nav_menu( $menu_args);
			?>
		</div>
		</div>
	</div>
</div>

<div class="container-fluid primary-menu visible-xs visible-sm">
<div class="container">
	<header id="masthead" class="site-header row ">

		<nav class="navbar navbar-default col-xs-12" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<?php
			$menu_args = array(
			"theme_location" => "mobile",
			"container_class" => "navbar-collapse collapse",
			"menu_class" => "nav navbar-nav",
			"menu_id" => "main-primary-menu",
			);
			wp_nav_menu( $menu_args);
			?>
		</nav>

	</header><!-- #masthead -->

	<div class="row  logo-background-lg">
		<div class="site-branding  col-xs-12 text-center">
			<?php
			the_custom_logo();
			?>
		</div><!-- .site-branding -->
	</div>
</div><!-- end container -->
</div><!-- end container -->

<div class="container-fluid primary-menu visible-md tablet">
<div class="container">
	<header id="masthead" class="site-header row logo-background-lg text-center">

		<div class="site-branding  col-md-12 text-center">
			<?php
			the_custom_logo();
			?>
		</div><!-- .site-branding -->

		<div class="navbar-container">
		<nav class="navbar navbar-default col-md-12 text-center" role="navigation">
			<div class="navbar-header">
			</div>
			<?php
			$menu_args = array(
			"theme_location" => "primary-left",
			"container_class" => "",
			"menu_class" => "nav navbar-nav",
			"menu_id" => "main-primary-menu",
			);
			wp_nav_menu( $menu_args);
			?>
			<?php
			$menu_args = array(
			"theme_location" => "primary-right",
			"container_class" => "",
			"menu_class" => "nav navbar-nav",
			"menu_id" => "main-primary-menu",
			);
			wp_nav_menu( $menu_args);
			?>
		</nav>
		</div>

	</header><!-- #masthead -->
</div><!-- end container -->
</div><!-- end container -->

<div class="container-fluid primary-menu visible-lg desktop">
<div class="container">
	<header id="masthead" class="site-header row logo-background-lg">

		<nav class="navbar navbar-default col-xs-12 col-md-5" role="navigation">
			<div class="navbar-header">
			</div>
			<?php
			$menu_args = array(
			"theme_location" => "primary-left",
			"container_class" => "",
			"menu_class" => "nav navbar-nav",
			"menu_id" => "main-primary-menu",
			);
			wp_nav_menu( $menu_args);
			?>
		</nav>

		<div class="site-branding col-xs-12 col-md-2">
			<?php
			the_custom_logo();
			?>
		</div><!-- .site-branding -->


		<nav class="navbar navbar-default col-xs-12 col-md-5"  role="navigation">
			<div class="navbar-header">
			</div>
			<?php
			$menu_args = array(
			"theme_location" => "primary-right",
			"container_class" => "",
			"menu_class" => "nav navbar-nav primary-menu-right",
			"menu_id" => "main-primary-menu",
			);
			wp_nav_menu( $menu_args);
			?>
		</nav>


	</header><!-- #masthead -->
</div><!-- end container -->
</div><!-- end container -->

<?php get_template_part( 'template-parts/fragment', 'ad-units-atf'); ?>

<?php get_template_part( 'template-parts/fragment', 'subscribe-tag'); ?>

<div class="container-fluid">
<div class="container">
	<div id="content" class="site-content row">
