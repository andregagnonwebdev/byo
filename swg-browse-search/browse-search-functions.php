<?php
/**
 * Include this file in your functions.php file to make these
 * functions available to browse and search templates. Example:
 *   - include_once(get_template_directory() . '/swg-browse-search/browse-search-functions.php');
 * 
*/

// Load the include for the ALM Relevanssi extension (customized for SWG)
include_once(get_template_directory() . '/swg-browse-search/includes/ajax-load-more-swg-relevanssi.php');

// Adding a filter to post_class
function swg_browse_search_slug_post_class($classes, $class, $post_id) {
  // May not be necessary
  //topic-beer-styles beer-style-bock
}

// Gets the number of published posts for a given type.
function swg_browse_search_post_type_count($post_type) {
  global $wpdb;
  //return get_post_type_object('recipe');
  $sql = $wpdb->prepare("SELECT COUNT(*) FROM $wpdb->posts WHERE post_type = %s AND post_status = %s;", $post_type, 'publish');
  return $wpdb->get_var($sql);
}

//
function swg_browse_search_scripts() {
  wp_enqueue_script( 'swg-browse-search-js', get_template_directory_uri() . '/swg-browse-search/js/swg-browse-search.js', ('jquery'), false, true );
  wp_enqueue_style( 'swg-browse-search-css', get_template_directory_uri() . '/swg-browse-search/styles/css/swg-browse-search.css', array());
}

add_action( 'wp_enqueue_scripts', 'swg_browse_search_scripts' );

// Parent values are set in wp_byo2_term_taxonomy
function swg_browse_search_taxonomy_parents() {
  global $wpdb;
  $sql = $wpdb->prepare("SELECT DISTINCT parent FROM $wpdb->term_taxonomy WHERE parent > %d", 0);
  return $wpdb->get_col($sql);
}

// Builds the data for a taxonomy filter list
function swg_browse_search_build_filter_data($post_types, $taxonomies) {
  $output = array(
    'debug' => 0,
    'taxonomies' => array(),
    'children' => array(),
  );
  
  foreach ($taxonomies as $tax_slug) {
    $tax_obj = get_taxonomy( $tax_slug );
		$tax_name = $tax_obj->labels->name;
		$terms = get_terms( $tax_slug );
		
		if (empty(count($terms))) continue;
    
    $cur_tax = array(
  		'name' => $tax_name,
  		'terms' => array(),
  		'expanded' => FALSE,
		);
		
		foreach ($terms as $term) {
  		$posts_count = 0;
  		
  		foreach ($post_types as $pt) {
    		$posts_count += _swg_browse_search_post_count_by_term($tax_slug, $term->slug, $pt);
      }
      
      $checked = (isset($_GET[$tax_slug][$term->slug]) && $_GET[$tax_slug][$term->slug] == $term->slug) ? " checked" : "" ;
      $exist_term = !empty($cur_tax['terms'][$term->term_id]) ? $cur_tax['terms'][$term->term_id] : array();
      
      
      // Flag the taxonomy as expanded if it has checked boxes
      if ($checked && !$cur_tax['expanded']) $cur_tax['expanded'] = TRUE;
      
      $term_data = array(
        'name' => $term->name,
        'slug' => $term->slug,
        'checked' => $checked,
        'children' => !empty($exist_term['children']) ? $exist_term['children'] : array(),
        'parent' => $term->parent,
        'posts_count' => $posts_count,
      );
      
      // Check if the term has a parent or is a parent
  		if (!empty($term->parent)) {
    		$output['children'][$term->parent][] = $term->term_id;
    		
    		if (empty($cur_tax['terms'][$term->parent])) {
      		$cur_tax['terms'][$term->parent] = array(
        		'children' => array(),
      		);
    		}
    		
    		$cur_tax['terms'][$term->parent]['children'][] = $term->term_id;
  		}
  		
  		$cur_tax['terms'][$term->term_id] = $term_data;
		}
		
		if (!empty($cur_tax['terms'])) {
  		$output['taxonomies'][$tax_slug] = $cur_tax;
		}
  }
  
  return $output;
}

/**
  * Builds the HTML markup for a taxonomy filter list
  */
function swg_browse_search_build_filter_markup($filters_data = array()) {
  if (empty($filters_data)) return '';
  $html = '';
  
  if (!empty($filters_data['debug'])) {
    return '<pre>' . print_r($filters_data['children']) . '</pre>';
  }
  
  $s = !empty($_GET['s']) ? $_GET['s'] : '';
  $post_type = !empty($_GET['post_type']) ? $_GET['post_type'] : '';
  
  $html .= '<ul class="swg-sidebar-filter js-only">';
  
  foreach ($filters_data['taxonomies'] as $tax_slug => $tax_item) {
    $show_tax = FALSE;
    $tax_html = '';
    
    $tax_filter_classes = 'taxonomy-list swg-expand-collapse tax-' . $tax_slug;
    if ($tax_item['expanded']) $tax_filter_classes .= ' expanded';
    
    $tax_html .= '<li class="' . $tax_filter_classes .'"><h5><label>' . $tax_item['name'] . '</label>';
    $tax_html .= '<span class="clear" data-tax-slug="' . $tax_slug . '">Clear</span></h5><ul class="items">';
    
    foreach ($tax_item['terms'] as $term_id => $term_data) {
      $is_parent = !empty($term_data['children']);
      $has_parent = !empty($term_data['parent']);
      $has_posts = !empty($term_data['posts_count']);
      
      if ($has_parent) continue;
      
      // If it's an empty parent, ignore it
      if (!$has_posts && !$is_parent) continue;
      
      if ($is_parent) {
        $children_have_posts = FALSE;
        $parent_html = '<li class="parent-term"><strong>' . $term_data['name'] . '</strong><ul>';
        
        foreach ($term_data['children'] as $child_id) {
          $child = $tax_item['terms'][$child_id];
          
          // Make sure there are posts for the term
          if (empty($child['posts_count'])) continue;
          $children_have_posts = TRUE;
          
          $parent_html .= _swg_browse_search_filter_checkbox_li($tax_slug, $child['slug'], $child_id, $child['name'], $child['posts_count']);
        }
        
        if ($children_have_posts) {
          $show_tax = TRUE;
          $tax_html .= $parent_html . '</ul></li>';
        }
      }
      else {
        $show_tax = TRUE;
        $tax_html .= _swg_browse_search_filter_checkbox_li($tax_slug, $term_data['slug'], $term_id, $term_data['name'], $term_data['posts_count']);
      }
    }
    
    if ($show_tax) {
      $html .= $tax_html . '</ul></li>';
    }
  }
  
  $html .= '</ul>';
  
  return $html;
}

/**
  * Gets magazine date data
  */
function swg_browse_search_magazine_archive_data() {
  $output = array(
    'special-issues' => array(),
  );
  
  $args = array(
    'taxonomy' => 'date',
    'hide_empty' => TRUE,
    'orderby' => 'term_order',
  );
  
  $post_filter = '';
  foreach (array('article', 'recipe', 'mr-wizard', 'project') as $pt) {
    $post_filter .= '&post_type[]=' . $pt;
  }
  
  $site_url = get_site_url();
  $prod_url = 'https://byo.com';
  $is_prod = ($site_url == $prod_url);
  
  // Get the terms
  $terms = get_terms($args);
  if (!empty($terms) && !is_wp_error($terms)) {
    foreach ($terms as $term) {
      $special_issue = FALSE;
      
      // Don't include if no posts
      if (empty($term->count)) continue;
      
      // Special issues go first
      if (stripos($term->name, 'Special Issue') !== FALSE) {
        $year = 'special-issues';
        $special_issue = TRUE;
      }
      else {
        $year = substr($term->name, -4);
      }
      
      // Don't include if it doesn't have a year attached
      if (!is_numeric($year) && !$special_issue) continue;
      
      if (!isset($output[$year])) {
        $output[$year] = array();
      }
      
      // Even after find-and-replace on the DB dump I noticed some calls to images on prod, hence this workaround
      if (!$is_prod) {
        $description = str_replace($prod_url, $site_url, $term->description);
      }
      else {
        $description = $term->description;
      }
      
      $output[$year][$term->term_id] = array(
        'name' => $term->name,
        'slug' => $term->slug,
        'description' => $description,
        'url' => '/?s=&date%5B' . $term->slug . '%5D=' . $term->slug . $post_filter,
        'alt' => esc_attr(sprintf(__('View all post filed under %s', 'my_localization_domain'), $term->name)),
      );
    }
  }
  
  return $output;
}

/**
  * Helper function to get all beer-styles for posts of
  * specified recipe types
  */
function _swg_beer_styles_by_recipe_types($recipe_types) {
  global $wpdb;
  
  $output = array(
    'terms' => array(),
  );
  
  //return get_post_type_object('recipe');
  $sql = $wpdb->prepare("SELECT COUNT(*) FROM $wpdb->posts WHERE post_type = %s AND post_status = %s;", $post_type, 'publish');
  //return $wpdb->get_var($sql);
  foreach ($recipe_types as $rt_term_id => $rt_term_slug) {
    
  }
}

/**
  * Helper function to build the filter checkbox markup
  */
function _swg_browse_search_filter_checkbox_li($tax_slug, $slug, $term_id, $name, $posts_count = 0, $class = 'filter-option') {
  $checked = (isset($_GET[$tax_slug][$slug]) && $_GET[$tax_slug][$slug] == $slug) ? " checked" : "" ;
  
  $output = '<li class="filter-term">';
  $output .= '<input name="' . $tax_slug . '[' . $slug . ']" value="' . $slug . '" id="swg-filter-checkbox-' . $term_id .'"';
  $output .= ' type="checkbox" class="' . $class . '" data-term-id="' . $term_id . '"';
  $output .= ' data-term-name="' . $name . '" data-term-slug="' . $slug . '" data-tax-slug="' . $tax_slug . '"';
  $output .= $checked ? ' checked="checked">' : '>';
  $output .= '<label for="swg-filter-checkbox-' . $term_id . '">' . $name . '</label>';
  $output .= '</li>';
  
  return $output;
}

/**
  * Helper function to get parent term IDs ordered by term_order
  */
function swg_browse_search_get_parent_term_ids($taxonomy) {
  global $wpdb;
  
  $query = "SELECT t1.term_id AS term_id FROM $wpdb->terms t1 ";
  $query .= "INNER JOIN $wpdb->term_taxonomy t2 ON t1.term_id = t2.term_id ";
  $query .= "WHERE t2.taxonomy = %s AND t2.parent = %d ";
  $query .= "ORDER BY t1.term_order ASC, t1.name ASC";
  $sql = $wpdb->prepare($query, $taxonomy, 0);
  return $wpdb->get_col($sql);
}

/**
  * Helper function to get the number of posts for a given term
  */
function _swg_browse_search_post_count_by_term($tax_slug, $term_slug, $post_type) {
  $post_list = get_posts(
    array(
      'showposts' => -1,
      'post_type' => $post_type,
      'tax_query' => array(
        array(
          'taxonomy' => $tax_slug,
          'field' => 'slug',
          'terms' => array($term_slug),
        )),
      'orderby' => 'title',
      'order' => 'ASC'
    )
  );
  
  return count($post_list);
}

/**
  * Adding handling to show the loader when pages are loading
  */
function swg_modify_body_class($classes) {
  return array_merge($classes, array('loading'));
}
add_filter('body_class', 'swg_modify_body_class');

/**
  * Order the results
  */
function swg_browse_search_order_results($q) {
  global $_swg_search_order_interruption;
  $_swg_search_order_interruption = 1;
  if (empty($_GET['orderby'])) return $q;
  
  switch ($_GET['orderby']) {
    case 'title': {
    	$q->set('orderby', 'post_title');
    	$q->set('order', 'asc');
    	break;
    }
    
    case 'date': {
      $q->set('orderby', 'post_date');
      $q->set('order', 'desc');
      break;
    }
    
    case 'relevance': {
      // Do nothing
      break;
    }
  }
  
  return $q;
}
add_filter('relevanssi_modify_wp_query', 'swg_browse_search_order_results');

/**
  * Sets counts of the results per post type
  */
function swg_count_post_type_hits($hits) {
  global $_swg_count_post_type_hits;
  
  $_swg_count_post_type_hits = array(
    'recipe' => 0,
    'article' => 0,
    'project' => 0,
    'mr-wizard' => 0,
  );
  
  // Split the post types in array $types
  if (!empty($hits)) {
    foreach ($hits[0] as $hit) {
      if (!isset($_swg_count_post_type_hits[$hit->post_type])) continue;
      $_swg_count_post_type_hits[$hit->post_type]++;
    }
  }
  $_swg_count_post_type_hits['global_count'] = count($hits[0]);
  return $hits;
}
add_filter('relevanssi_hits_filter', 'swg_count_post_type_hits');

/**
  * Needed to get the AJAX search results plugin to work with Relevanssi
  */
function swg_alm_query_args_relevanssi($args) {
  $args = apply_filters('swg_alm_relevanssi', $args);
  return $args;
}
add_filter('alm_query_args_swg_relevanssi', 'swg_alm_query_args_relevanssi');

/**
  * Overriding the text on the more results button
  */
function swg_custom_alm_button_label() {
   return '...'; // <- new button text
}
add_filter('alm_button_label', 'swg_custom_alm_button_label');
