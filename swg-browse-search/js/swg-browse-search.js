// Variable for the loader HTML so it can be easily placed dynamically
var swg_alm_loader = '<div class="swg-spinner"><div class="rect3"></div><div class="rect2"></div><div class="rect1"></div></div>';
    
(function ($) {
  $(document).ready(function() {
    /*
     Function to break out the filtering show/hide so that code
     isn't repeated too much between the checkbox changes and
     the manualy removing tags from .filtered-active-tags
    */
    function swg_browse_search_filter_results() {
      swg_browse_search_build_active_tags();
      swg_browse_search_filter_search_results();
    }
    
    /**
      * Builds the active tags markup
      */
    function swg_browse_search_build_active_tags() {
      var filter_tags_html = '';
      var slugs = '';
      var tax_slugs_array = [];
      var tax_slugs = '';
      
      $('.taxonomy-list.swg-expand-collapse').each(function() {
        var $inputs = $(this).find('input[type="checkbox"]');
        
        $inputs.each(function() {
          var checked = $(this).prop('checked');
          var term_id = $(this).attr('data-term-id');
          var term_name = $(this).attr('data-term-name');
          var term_slug = $(this).attr('data-term-slug');
          var tax_slug = $(this).attr('data-tax-slug');
          var $tax_parent = $(this).parents('.swg-expand-collapse');
          
          var term_divider = ', ';
          
          // Reset the container
          $('#swg-filter-active-tags').html('');
          
          if (checked) {
            var is_new_tax = false;
            if ($.inArray(tax_slug, tax_slugs_array) < 0) {
              is_new_tax = true;
              tax_slugs_array.push(tax_slug);
            }
            
            if (!slugs.length) {
              slugs += term_slug;
            }
            else {
              if (is_new_tax) {
                slugs += ':' + term_slug;
              }
              else {
                slugs += ',' + term_slug;
              }
            }
            
            if (!tax_slugs.length) {
              tax_slugs += tax_slug;
            }
            else if (is_new_tax) {
              tax_slugs += ':' + tax_slug;
            }
            
            filter_tags_html += '<span id="filter-active-tag-' + term_id + '" data-tax-slug="' + tax_slug + '"';
            filter_tags_html += ' class="filter-tag" data-term-id="' + term_id + '">';
            filter_tags_html += term_name + '</span>';
            
            // Expand the parents
            if (!$tax_parent.hasClass('expanded')) {
              $tax_parent.addClass('expanded');
            }
          }
        });
      });
      
      var data_tax_operator = '';
      for (i = 0; i < tax_slugs_array.length; i++) {
        if (data_tax_operator.length) {
          data_tax_operator += ':';
        }
        
        data_tax_operator += 'IN';
      }
      
      $('#swg-ajax-load-more-data').data('taxonomyOperator', data_tax_operator);
      $('#swg-ajax-load-more-data').data('taxonomyTerms', slugs);
      $('#swg-ajax-load-more-data').data('taxonomy', tax_slugs);
      $('#swg-filter-active-tags').html(filter_tags_html);
      
      // Assign the onclick functionality to the filter tags
      $(document).on('click', '#swg-filter-active-tags > .filter-tag', function() {
        var term_id = $(this).attr('data-term-id');
        
        // Uncheck the corresponding box
        $('input[data-term-id="' + term_id + '"]').prop('checked', false);
        
        // Trigger the change event so this function will run again
        $('input[data-term-id="' + term_id + '"]').trigger('change');
      });
    }
    
    /**
      * Update the results count after filtering
      */
    function swg_browse_search_update_result_count() {
      var $counter = $('.swg-alm-relevanssi-count');
      var results_number = $counter.data('hits-count');
      if (results_number) {
        $('.results-count-number').html(results_number);
        $('.search-found').css({'opacity': 1});
      }
    }
    
    $.fn.almComplete = function(alm){
      swg_browse_search_update_result_count();
      if ($('.ajax-load-more-wrap .alm-btn-wrap').length && !$('.ajax-load-more-wrap .alm-btn-wrap .swg-spinner').length) {
        $('.ajax-load-more-wrap .alm-btn-wrap').prepend(swg_alm_loader);
      }
    };
    
    $.fn.almDone = function(){
      if (!$('.swg-post-terms-data-wrapper').length) {
        $('.ajax-load-more-wrap .alm-btn-wrap .swg-spinner').addClass('done');
        $('.no-results').show();
        $('.search-found').css({'opacity': 0});
      }
    };
    
    /**
      * Shows/hides search results based on filter changes
      */
    function swg_browse_search_filter_search_results() {
      swg_browse_search_update_alm_filters();
    }
    
    /**
      * Update the ALM filters
      */
    function swg_browse_search_update_alm_filters() {
      var data = $('#swg-ajax-load-more-data').data();
      $('.ajax-load-more-wrap .alm-btn-wrap .swg-spinner').removeClass('done');
      $('.no-results').hide();
      $('.search-found').css({'opacity': 1});
      $.fn.almFilter('fade', '300', data);
    }
    
    /*
      Shows the loader
    */
    function swg_browse_search_show_loader() {
      $('body').addClass('loader');
    }
    
    /*
      Hides the loader
    */
    function swg_browse_search_hide_loader() {
      $('body').removeClass('loader');
    }
    
    /*
     Function to forcibly reset all filters
    */
    function swg_browse_search_filter_reset() {
      $('.filter-term input.filter-option').prop('checked', false);
      $('#swg-filter-active-tags').html('');
      $('.swg-post-terms-data-wrapper').removeClass('filtered-out');
    }
    
    // Assign the on change functionality to the filter checkboxes
    $(document).on('change', 'input.filter-option', swg_browse_search_filter_results);
    
    // Build the filter tags on document load if they're already selected
    swg_browse_search_build_active_tags(true);
    
    // Filter toggle
    $('.swg-expand-collapse').each(function(index) {
      var $this = $(this);
      var $h5 = $this.find('h5 > label');
      var $content = $this.find('.items');
      var $clear = $this.find('h5 > .clear');
      
      $h5.on('click', function() {
        $content.slideToggle('fast', function() {
          $this.toggleClass('expanded');
          $h5.toggleClass('expanded');
        });
      });
    });
    
    // Clear filters link for each taxonomy
    $('.swg-expand-collapse h5 > .clear').on('click', function() {
      var tax_slug = $(this).attr('data-tax-slug');
      
      $('.swg-expand-collapse.tax-' + tax_slug + ' .items input:checked').each(function(index) {
        $(this).prop('checked', false);
      });
      
      $('#swg-filter-active-tags .filter-tag[data-tax-slug="' + tax_slug + '"]').remove();
      
      if ($('#swg-filter-active-tags > .filter-tag[data-tax-slug="' + tax_slug + '"]').length == $('#swg-filter-active-tags > .filter-tag').length) {
        swg_browse_search_filter_reset();
      }
      
      swg_browse_search_filter_search_results();
    });
    
    // Sort results
    $('#swg-search-results-sort-by').on('change', function() {
      var sort_val = $(this).val();
      var order_val = 'ASC';
      
      if (sort_val == 'date') {
        order_val = 'DESC';
      }
      
      $('.swg-orderby-auto-pop').val(sort_val);
      $('#swg-ajax-load-more-data').data('orderby', sort_val);
      $('#swg-ajax-load-more-data').data('odrer', order_val);
      swg_browse_search_update_alm_filters();
    });
    
    // Show all hidden JS only elements
    $('.js-only').show();
    
    // Show hide the filters
    $('.swg-filter-col > .swg-mobile-toggle').on('click', function() {
      $(this).parent('.swg-filter-col').toggleClass('expanded');
      $('.swg-filters-row').slideToggle();
    });
    
    // Browse page - recipe types
    $(document).on('change', 'input.browse-recipe-type-filter', function() {
      swg_browse_search_show_loader();
      
      var term_id = $(this).attr('data-term-id');
      var term_slug = $(this).attr('data-term-slug');
      var checked = $(this).prop('checked');
      var url_append = '';
      
      var checked_terms = [];
      $('input.browse-recipe-type-filter').each(function(index) {
        if ($(this).prop('checked')) {
          url_append += '&recipe-type%5B' + $(this).attr('data-term-slug') + '%5D=' + $(this).attr('data-term-slug');
          checked_terms[index] = $(this).attr('data-term-id');
        }
      });
      
      // Add recipe type filter to the links
      // Hide any links that aren't relevant
      $('.swg-rt-bs-link-item > a').each(function() {
        var $link = $(this);
        var show_link = false;
        var og_href = $(this).attr('data-original-url');
        
        if (checked_terms.length) {
          for (i = 0; i < checked_terms.length; i++) {
            var re = new RegExp(':' + checked_terms[i] + ':', 'g');
            if (!show_link && $link.attr('data-rt-terms').match(re)) {
              $link.attr('href', og_href + url_append);
              show_link = true;
            }
          }
        }
        else {
          show_link = true;
        }
        
        if (show_link) {
          $(this).removeClass('filtered-out');
        }
        else if (!$(this).hasClass('filtered-out')) {
          $(this).addClass('filtered-out');
        }
      });
      
      $('.swg-browse-search--beer-style-group').each(function() {
        var visible_count = 0;
        var $group_wrapper = $(this);
        var $link_items = $group_wrapper.find('.swg-rt-bs-link-item > a');
        
        $link_items.each(function() {
          if (!$(this).hasClass('filtered-out')) {
            visible_count++;
          }
        });
        
        if (!visible_count && !$group_wrapper.hasClass('filtered-out')) {
          $group_wrapper.addClass('filtered-out');
        }
        else if (visible_count) {
          $group_wrapper.removeClass('filtered-out');
        }
      });
    });
    
    
    $('.swg-browse-row').each(function() {
      var $swg_row = $(this);
      var $swg_row_header = $swg_row.find('.swg-browse-search--browse--section-header');
      var $swg_row_items = $swg_row.find('.items');
      var is_magazine_archive = $swg_row.is('#swg-browse-search--browse--magazine-archive');
      
      $swg_row_header.on('click', function() {
        $swg_row_items.slideToggle('fast', function() {
          $swg_row.toggleClass('expanded');
        });
      });
    });
    
    /**
      * Check if a year is hidden
      */
    function swg_browse_search_is_year_link_hidden(el) {
      var $link = $(el);
      var $li = $link.parent('li');
      var $ul = $li.parent('ul');
      var link_pos = $li.position();
      var data = {
        ul_width: parseInt($('#swg-browse--magazine-year-links-wrapper').width()),
        link_width: parseInt($li.width()),
        link_x: parseInt(link_pos.left),
        margin_left: parseInt($li.css('marginLeft').replace('px', '')),
        link_end_x: 0
      }
      
      $('.year-nav-arrow').css({'opacity': 1});
      
      if ($li.is(':first-child')) {
        $('.year-nav-arrow.year-nav-arrow--prev').css({'opacity': 0.5});
      }
      else if ($li.is(':last-child')) {
        $('.year-nav-arrow.year-nav-arrow--next').css({'opacity': 0.5});
      }
      
      // If moving right ...
      if (data.link_x > 0) {
        data.link_end_x = parseInt(data.link_x + data.link_width + data.margin_left);
        if (data.link_end_x <= data.ul_width) return;
        $ul.find('li').animate({'left': '-=' + parseInt(data.link_end_x - data.ul_width)});
      }
      // If moving left ...
      else if (data.link_x < 0) {
        if ($li.is(':first-child')) {
          $ul.find('li').animate({'left': '0px'});
        }
        else {
          $ul.find('li').animate({'left': '+=' + Math.abs(data.link_x)});
        }
      }
    }
    
    // Magazine archives arrow navigation links
    $('#swg-browse--magazine-year-links a.swg-mag-year-slider-link').on('click', function(event) {
      event.preventDefault();
      
      var year = $(this).data('year');
      
      swg_browse_search_is_year_link_hidden(this);
      
      $('a.swg-mag-year-slider-link, .swg-browse--year-container').removeClass('active');
      $(this).addClass('active');
      $('.swg-browse--year-container[data-year="' + year + '"]').addClass('active');
    });
    
    $('.year-nav-arrow').on('click', function() {
      // Check whether we're going left or right
      var nav_dir = 'prev';
      if ($(this).hasClass('year-nav-arrow--next')){
        nav_dir = 'next';
      }
      
      swg_browse_search_show_loader();
      
      var $active_year_a = $('#swg-browse--magazine-year-links a.active');
      var $active_year_li = $active_year_a.parent('li');
      var $next_link = $active_year_li.next('li').find('a');
      
      if (nav_dir == 'prev') {
        $next_link = $active_year_li.prev('li').find('a');
      }
      
      if ($next_link.length) {
        $next_link[0].click();
      }
    });
    
    // Mobile archive year select form
    $('#swg-browse-search-magazine-year-select').on('change', function() {
      var year = $(this).val();
      
      $('.swg-browse--year-container').removeClass('active');
      $('.swg-browse--year-container[data-year="' + year + '"]').addClass('active');
    });
    
    // Resetting post type filters on search
    if ($('#swg-sidebar-filter-form').length) {
      $('#search-browse--post-types a').on('click', function(event) {
        event.preventDefault();
        var post_type = $(this).data('postType');
        
        if ($(this).hasClass('swg-active')) {
          post_type = 'all';
        }
        
        $('#swg-sidebar-filter-form input[name="post_type"]').val(post_type);
        $('#swg-sidebar-filter-form').trigger('submit');
      });
    }
  });

  $(window).load(function() {
    $('body').removeClass('loading');
    // Replace the loader for AJAX Load More
    if ($('.ajax-load-more-wrap .alm-btn-wrap').length && !$('.ajax-load-more-wrap .alm-btn-wrap .spinner').length) {
      $('.ajax-load-more-wrap .alm-btn-wrap').prepend(swg_alm_loader);
    }
  });
})(jQuery);
