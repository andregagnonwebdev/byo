<?php
  if (!isset($is_browse_page)) $is_browse_page = FALSE;
  $browse_class = ($is_browse_page) ? ' browse-page' : '';
?>

<div class="row swg-browse-search--browse-buttons<?php echo $browse_class; ?>">
  <?php
    $pto_orderby = !empty($sort_value) ? '&orderby=' . $sort_value : '';
    global $_swg_count_post_type_hits;
    
    // Total number of results
    $total_results_num = $wp_query->found_posts;
    
    $post_type_options = array(
      'recipe' => 'Recipes',
      'article' => 'Articles',
      'project' => 'Projects',
      'mr-wizard' => 'Mr. Wizard',
    );
  ?>
  <ul id="search-browse--post-types">
    <?php foreach ($post_type_options as $pto_slug => $pto_name): ?>
      <?php
        // Check if this is the active post type
        $is_active_pt = ($post_type == $pto_slug);
        $pto_class = 'swg-' . $pto_slug;
        $hide_count = TRUE;
        
        if ($is_active_pt) $pto_class .= ' swg-active';
        
        $img = ($pto_slug == 'mr-wizard') ? $pto_slug : $pto_slug . 's';
        $pto_hits = 0;
        
        if (!$is_browse_page) {
          if ($is_active_pt) {
            $pto_hits = $total_results_num;
          }
          else {
            $pto_hits = (isset($_swg_count_post_type_hits[$pto_slug])) ? $_swg_count_post_type_hits[$pto_slug] : 0;
          }
          
          $hide_count = (empty($pto_hits));
        }
        
        if ($is_browse_page) {
          $pto_url = ($pto_slug == 'mr-wizard') ? '/troubleshooting/' : '/' . $pto_slug . 's';
        }
        else {
          $pto_url = '/?s=' . $sq . '&post_type=' . $pto_slug . $pto_orderby;
        }
        
        $hits_count = $is_active_pt ? '<span class="hits-count">(<span class="results-count-number">' . $total_results_num . '</span>)</span>' : '<span class="hits-count">(' . $pto_hits . ')</span>';
      ?>
      <li>
        <a href="<?php echo $pto_url; ?>" class="<?php echo $pto_class; ?>" data-post-type="<?php echo $pto_slug; ?>">
          <img class="swg-default-icon" src="<?php bloginfo('template_url'); ?>/swg-browse-search/images/<?php echo $img; ?>.svg" alt="icon for <?php echo $pto_name; ?>"><img class="swg-active-icon" src="<?php bloginfo('template_url'); ?>/swg-browse-search/images/<?php echo $img; ?>-active.svg" alt="icon for <?php echo $pto_name; ?>">
          <span class="swg-browse-button--text"><?php echo $pto_name; ?><?php if (!$hide_count): ?> <?php echo $hits_count ?><?php endif; ?></span>
        </a>
      </li>
    <?php endforeach; ?>
  </ul>
</div><!-- /.swg-browse-search--browse-buttons -->
