<?php
/*
Taken from Ajax Load More's Relevanssi extension. The only change is that I'm making it allow different orderby values.
Original plugin details follow
---
Plugin Name: Ajax Load More for Relevanssi
Plugin URI: http://connekthq.com/plugins/ajax-load-more/extensions/relevanssi/
Description: An Ajax Load More extension that adds compatibility with Relevanssi
Text Domain: ajax-load-more-for-relevanssi
Author: Darren Cooney
Twitter: @KaptonKaos
Author URI: https://connekthq.com
Version: 1.0
License: GPL
Copyright: Darren Cooney & Connekt Media
---
*/


if(!class_exists('SWG_ALM_Relevanssi')) :   
   
   class SWG_ALM_Relevanssi{	   
      
   	function __construct(){	
         add_filter('swg_alm_relevanssi', array(&$this, 'swg_alm_relevanssi_get_posts'), 10, 1);	
      }
      
      
      
      /*
   	*  alm_relevanssi_get_posts
   	*  Get relevanssi search results and return post ids in post__in wp_query param
   	*
   	*  @return $args
   	*  @since 1.0
   	*/
      function swg_alm_relevanssi_get_posts($args){
 
      	if(function_exists('relevanssi_do_query')){         	
         	
         	$old_posts_per_page = $args['posts_per_page'];
         	$old_offset = $args['offset'];
         	$args['fields'] = 'ids'; 
         	$args['posts_per_page'] = -1; // We need to get all search results for this to work
         	$args['offset'] = 0; // We don't want an offset (ALM handles this)
         	
         	$query = new WP_Query($args);
         	relevanssi_do_query($query);   
         	
      		if ( ! empty( $query->posts ) ) {
        		$orderby = !empty($args['orderby']) ? $args['orderby'] : 'post__in';
        		if (!in_array($orderby, array('post_title', 'post_date', 'post__in'))) $orderby = 'post__in';
        		
        		if ($orderby == 'post_date') {
          		$args['order'] = 'DESC';
        		}
        		
      			$args['post__in'] = $query->posts; // $relevanssi_query->posts array
      			//$args['orderby'] = 'post__in'; // override orderby to relevance
      			$args['orderby'] = $orderby;
      			$args['s'] = ''; // Reset 's' term value
      			$args['posts_per_page'] = $old_posts_per_page; // Reset 'posts_per_page' before sending data back
      			$args['offset'] = $old_offset; // Reset 'offset' before sending data back
      			    			
      		}  
      		    			
      		return $args;	
      	}
      	    	
      }
   }
   
   
   
   /*
   *  ALM_Relevanssi
   *  The main function responsible for returning the one true ALM_Relevanssi Instance.
   *
   *  @since 2.0.0
   */
   function SWG_ALM_Relevanssi(){
      global $SWG_ALM_Relevanssi;
      
      if( !isset($SWG_ALM_Relevanssi) ){
         $SWG_ALM_Relevanssi = new SWG_ALM_Relevanssi();
      }
      
      return $SWG_ALM_Relevanssi;
   }      
   SWG_ALM_Relevanssi(); // initialize
   
endif;