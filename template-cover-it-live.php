<?php
/**
 * Template Name: Cover It Live
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SWG
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main col-xs-12 col-md-12">
			<div class="row">
				<div class="col-xs-12">
					<span class="eyebrow">Cover It Live</span>
					<h1><?php the_title() ?></h1>
				</div>
				<div class="col-xs-12 col-md-12">
					<?php if ( byo_has_subscription() == true ): ?>
							<?php echo get_field( 'cover_it_live'); ?>
					<?php else: ?>
						<?php echo 'Membership required.' ?>
					<?php endif; ?>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
