<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package byo
 */

get_header(); ?>

<section id="primary" class="content-area primary--search">
	<div class="col-xs-12">
		<?php
      $post_type = (!empty($_GET['post_type']) && !is_array($_GET['post_type'])) ? $_GET['post_type'] : 'all';
      $sort_value = !empty($_GET['orderby']) ? $_GET['orderby'] : 'relevance';
      $sort_order = ($sort_value == 'date') ? 'DESC' : 'ASC';
      $num_results = $wp_query->found_posts;
      
      $sq = is_search() ? get_search_query() : '';
      
      $taxonomies = get_taxonomies( array( '_builtin' => false, 'public' => true ));
      $j = 0;
      $type_name = '';
      $data_term_slugs = '';
      $data_tax_slugs = '';
      $data_tax_operator = '';
      $count_taxonomies = 0;
      
      foreach ( $taxonomies as $tax_slug ) {
        $terms = get_query_var( $tax_slug, $default = '' );
        if ( $terms) {
          $count_taxonomies++;
          if (!empty($data_term_slugs)) {
            $data_term_slugs .= ':';
          }
          
          if ( $j) {
            $type_name .= ', and ';
            $data_tax_slugs .= ':';
            $data_tax_operator .= ':';
          }
          else {
            if ($sq == '') {
              $type_name .= ' for ';
            }
            else {
              $type_name .= ' and ';
            }
          }
          
          $data_tax_slugs .= $tax_slug;
          $data_tax_operator .= 'IN';
          
          $j++;
          $i = 0;
          foreach ($terms as $key => $term) {
            if ($i) {
              $data_term_slugs .= ',';
            }
            
            $data_term_slugs .= $term;
            
            
            if ($i) {
              $type_name .= ' or ';
            }
            
            $i++;
            $t = get_term_by( 'slug', $term, $tax_slug );
            $type_name .= '<strong>'. $t->name . '</strong>';
          }
        }
      }
      
      if ($post_type == 'all' || (is_array($post_type) && count($post_type) >= 4)) {
        if (empty($data_term_slugs) && empty($sq)) {
          $post_type = 'project';
        }
      }
      
      $result = ( $post_type == 'all' || $post_type == 'mr-wizard') ? 'result' : $post_type;
      
      $results_str = '<strong class="results-count-number">' . $num_results . '</strong> '. $result . '(s) found';
      if ($sq) {
        $results_str .= ' for keyword <strong>"'. $sq .'"</strong>';
      }
      $results_str .= $type_name . '.';
      
			$tax_relation = 'AND';
		?>
		<div id="swg-ajax-load-more-data-wrapper"><a href="#" id="swg-ajax-load-more-data" data-repeater="default" data-posts-per-page="5" data-taxonomy="<?php echo $data_tax_slugs; ?>" data-taxonomy-terms="<?php echo $data_term_slugs; ?>" data-orderby="<?php echo $sort_value; ?>" data-order="<?php echo $sort_order; ?>" data-taxonomy-operator="<?php echo $data_tax_operator; ?>" data-taxonomy-relation="<?php echo $tax_relation; ?>"></a></div>
    <div class="row">
      <div class="col-xs-12 col-md-12">
        <?php get_template_part( 'searchform', 'all' ); ?>
      </div>
    </div><!-- /.row -->
	</div>


	<?php
		$search_results_name = ( $post_type ) ? 'search' : ('search'.'-'.$post_type);
		$search_filter_name = 'all';
	?>

  <div class="col-xs-12">
    <div class="row">
			<div class="col-sm-12 col-xs-12 col-md-4 col-lg-3">
				<?php get_template_part( 'template-parts/fragment-search-filter', $search_filter_name); ?>

				<div class="hidden-xs hidden-sm search-ad-units ad-units">
					<?php get_template_part( 'template-parts/fragment', 'ad-units-right-rail'); ?>
				</div>
			</div>

  		<main id="main" class="site-main col-xs-12 col-md-8 col-lg-9 search-browse-main">
  		  <?php include_once(get_template_directory() . '/swg-browse-search/includes/swg-icons.php'); ?>
      
  		<?php
  		//if ( have_posts() ) : ?>
  			<div class="results-info">
          <div class="search-found<?php echo ($num_results) ? '' : ' no-results-found'; ?>"><?php echo $results_str; ?></div>
          
          <div class="swg-results-sort js-only">
            <?php $sort_value = !empty($_GET['orderby']) ? $_GET['orderby'] : 'relevance'; ?>
            <label for="swg-search-results-sort-by">Sort by</label>
            <select id="swg-search-results-sort-by" name="swg_search_results_sort_by">
              <option value="relevance"<?php echo ($sort_value == 'relevance') ? ' selected' : ''; ?>>Relevance</option>
              <option value="post_title"<?php echo ($sort_value == 'post_title') ? ' selected' : ''; ?>>Alphabetical</option>
              <option value="post_date"<?php echo ($sort_value == 'post_date') ? ' selected' : ''; ?>>Most Recent</option>
            </select>
          </div><!-- /.swg-results-sort -->
  			</div><!-- /.results-info -->
        
        <?php
  			if (empty($post_type) || $post_type == 'any' || $post_type == 'all') {
    			$alm_pt = 'article, recipe, project, mr-wizard';
  			}
  			else {
    			$alm_pt = $post_type;
  			}
  			
			  echo do_shortcode('[ajax_load_more id="swg_relevanssi" search="'. $sq .'" post_type="' . $alm_pt . '" scroll="true" orderby="' . $sort_value . '" order="' . $sort_order . '" taxonomy="' . $data_tax_slugs . '" taxonomy_terms="' . $data_term_slugs . '" taxonomy_operator="' . $data_tax_operator . '" taxonomy_relation="' . $tax_relation . '" posts_per_page="10"]');
  			?>
  			
  			<div class="row">
    			<p class="no-results"><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'swg' ); ?></p>
  			</div><!-- end row -->

  		<?php //else : ?>
        <!--<p class="swg-alm-no-results"><?php //esc_html_e( 'Sorry, FTW but nothing matched your search terms. Please try again with some different keywords.', 'swg' ); ?></p>-->
  		<?php //endif; ?>
  		</main><!-- #main -->
    </div> <!-- /.row -->
  </div>
</section><!-- #primary -->

<?php
//get_sidebar();
get_footer();
