<?php
/**
 * BYO Theme Customizer
 *
 * @package BYO
 */


function byo_theme_mod_default( $key)
{
    static $byo_theme_mod_defaults = array(
      'byo-color-nav-link' => '#aa4a14',
      'byo-color-text-heading' => '#000',
      'byo-color-text-body' => '#000000',
      'byo-color-text-link' => '#aa4a14',
      'byo-color-text-footer' => '#fff',
      'byo-color-text-footer-link' => '#fff',
      'byo-color-text-footer-rollover' => '#888',
      'byo-color-rule-lines' => '#aa4a14',
      'byo-color-call-to-action' => '#365684',

      'byo-color-background-body' => '#ffffff',
      'byo-color-background-content' => '#efe9e5',

      'byo-font-header-1' =>  'Open Sans',
      'byo-font-header' =>  'PT Sans',
      'byo-font-nav-menu' =>  'Ultra',
      'byo-font-title' =>  'PT Sans',
      'byo-font-heading' =>  'Ultra',
      'byo-font-body-text' =>  'PT Sans',
      'byo-font-button' =>  'PT Sans',
      'byo-font-caption' =>  'PT Sans',
      'byo-font-footer' =>  'PT Sans',
      'byo-font-footer-heading' =>  'Ultra',


      'byo-facebook' => '',
      'byo-instagram' => '',
      'byo-yelp' => '',

      'byo-company-name' => '',
      'byo-address' => '',
      'byo-address-2' => '',
      'byo-town-state-zip' => '',
      'byo-telephone' => '860-542-5761',
      'byo-email' => '',

      'byo-copyright-message' => '© Copyright 2017. All rights reserved.',

      'byo-img-upload' => '',   // footer

    );

    if ( array_key_exists($key, $byo_theme_mod_defaults) )
        return( $byo_theme_mod_defaults[ $key]);
    else
    {
        return( '');
    }
}

function byo_get_theme_mod( $key, $default='unused')
{
    // provide defaults for 2nd parameter
    return( get_theme_mod( $key, byo_theme_mod_default( $key)));
}

// return all google fonts used in theme options, used in functions.php
function byo_get_theme_fonts() {

    $fonts = array();

    $fonts[] = byo_get_theme_mod( 'byo-font-header-1');
    $fonts[] = byo_get_theme_mod( 'byo-font-header');
    $fonts[] = byo_get_theme_mod( 'byo-font-nav-menu');
    $fonts[] = byo_get_theme_mod( 'byo-font-title');
    $fonts[] = byo_get_theme_mod( 'byo-font-heading');
    $fonts[] = byo_get_theme_mod( 'byo-font-body-text');
    $fonts[] = byo_get_theme_mod( 'byo-font-button');
    $fonts[] = byo_get_theme_mod( 'byo-font-caption');
    $fonts[] = byo_get_theme_mod( 'byo-font-footer');
    $fonts[] = byo_get_theme_mod( 'byo-font-footer-heading');
    $fonts = array_unique( $fonts);

    return( $fonts);
}

/////////////////////////////
// new font stuff



static $byo_all_fonts = array(
   'Georgia' => array( 'serif', ''),
   'Arial' => array( 'sans-serif', ''),

   // google
   'Ultra' => array( 'serif', '400,400i,700,700i'),


   'Open Sans'  => array( 'sans-serif', '400,400i,700,700i'),
   'PT Sans'  => array( 'sans-serif', '400,400i,700,700i'),

 );

 static $byo_body_text_fonts = array(
   'Georgia' => array( 'serif', ''),
   'Arial' => array( 'Helvetica, sans-serif', ''),

   // google
   'Ultra' => array( 'serif', '400,400i,700'),

   'PT Sans'  => array( 'sans-serif', '400,400i,700,700i'),

 );

 // Add Google fonts CSS to admin header
 function byo_load_fonts() {

   global $byo_all_fonts;

   $fonts = byo_get_theme_fonts();
   foreach ($fonts as $f) {
     if ( $byo_all_fonts[ $f][ 1]) {
       $l = '//fonts.googleapis.com/css?family=';
       global $fun_all_fonts;
       $l .= str_replace( ' ', '+', $f) . ':' . $fun_all_fonts[ $f][ 1];

       //$font_url = str_replace( ',', '%2C', $l);
       //$font_url = esc_url( $font_url);
       $font_url = esc_url( $l);
       add_editor_style( $font_url );
     }
   }

 }

 // load fonts for admin?
 //add_action('customize_controls_enqueue_scripts', 'stout_oak_load_fonts');
 //add_action('wp_enqueue_scripts', 'byo_load_fonts');
 add_action('wp_default_scripts', 'byo_load_fonts');
 //add_action('admin_enqueue_scripts', 'byo_load_fonts');

 // Add Google font CSS to admin header
 function byo_font_family( $key) {

     global $byo_all_fonts;
     return( "'".$key."'".", ".$byo_all_fonts[ $key][0]);
}




/////////////////////////////
// old font stuff
function byo_google_fonts_filter( $f)
{
//    loco_print_r( $f);
    // remove some fonts
    return( $f['category'] != 'handwriting');
}

function byo_get_google_fonts( $max = 5, $all = true)
{
  $googleAllFontList = array();
  global $byo_all_fonts;
  global $byo_body_text_fonts;

  $fonts = ($all ?  ($byo_all_fonts):($byo_body_text_fonts));
  foreach( $fonts as $key => $f) {
    $googleAllFontList[ $key] = $key;
  }
  return( $googleAllFontList);


  // old

    $standardFontList = array(
            'Andale Mono' => 'andale mono,times',
            'Arial' => 'arial,helvetica,sans-serif',
            'Arial Black' => 'arial black,avant garde',
            'Book Antiqua' => 'book antiqua,palatino',
            'Comic Sans MS' => 'comic sans ms,sans-serif',
            'Courier New' => 'courier new,courier',
            'Georgia' => 'georgia,palatino',
            'Helvetica' => 'helvetica',
            'Impact' => 'impact,chicago',
            'Tahoma' => 'tahoma,arial,helvetica,sans-serif',
            'Terminal' => 'terminal,monaco',
            'Times New Roman' => 'times new roman,times',
            'Trebuchet MS' => 'trebuchet ms,geneva',
            'Verdana' => 'verdana,geneva',
             );
    $standardFontList = array_flip( $standardFontList);
    return( $standardFontList);

//loco_var_dump( $standardFontList);

    $list = array();

    // Use the Google API to get a list of fonts
    //    https://www.googleapis.com/webfonts/v1/webfonts?sort=popularity&key=AIzaSyAdAzzo-XUCV0oBEVkzEx85V24BXkeyimA

    $url = 'https://www.googleapis.com/webfonts/v1/webfonts?sort=popularity&fields=items(category%2Cfamily)&key=AIzaSyDjyeFIVeWHP0t95rWVtX7KO2wZUCJ6gFU';
    $response = wp_remote_get( $url);
    if( is_array($response) ) {
      $header = $response['headers']; // array of http header lines
      $body = $response['body']; // use the content
      $b = json_decode( $body);
//      echo "family0:".$b->items[0]->family;
//      echo '<br />';
      $fonts = array();
      foreach ($b->items as $key => $f) {
//          echo "font-family".$key.": ".$f->family. "<br />";
            $fonts[ $key]['family'] = $f->family;
            $fonts[ $key]['category'] = $f->category;
          if ( $key > $max)
            break;
      }
      $fonts =  array_filter( $fonts, 'byo_google_fonts_filter');
      sort( $fonts);

//      loco_var_dump( $fonts);
//      loco_print_r( $fonts);
//      loco_print_r( get_theme_mods());

    // key is lowercase for code, value is upper case read-able
    foreach ( $fonts as $key => $f) {
        $k = strtolower( $f['family']).','.strtolower($f['category']);
        $list[ $k] = $f['family'];
    }
//    $list = array_merge($list, $standardFontList);

//      loco_print_r( $list);

    }
//    else
//        echo "response: $response";
    return( $list);
}

function byo_sanitize_text( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function byo_customize_register( $wp_customize ) {
    $wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
    $wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
 //   $wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

    // remove some stuff
    $wp_customize->remove_control( 'background_color' );
    $wp_customize->remove_control( 'header_textcolor' );
//    $wp_customize->remove_panel( 'widgets');
    $wp_customize->remove_section( 'static_front_page');

    // TBD make this data driven, for here and CSS below.

    ///////////////////////////////////////////////////////////////////////////
    // Colors
    $wp_customize->add_setting( 'byo-color-nav-link',
        array(
        'type' => 'theme_mod',
        'default' => byo_theme_mod_default( 'byo-color-nav-link'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'byo-color-nav-link',
        array(
        'label' => __( 'Navigation Menu', 'byo_textdomain' ),
        'section' => 'colors',
    ) ) );


    $wp_customize->add_setting( 'byo-color-text-body',
        array(
        'type' => 'theme_mod',
        'default' => byo_theme_mod_default( 'byo-color-text-body'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'byo-color-text-body',
        array(
        'label' => __( 'Body Text', 'byo_textdomain' ),
        'section' => 'colors',
    ) ) );

    $wp_customize->add_setting( 'byo-color-text-link',
        array(
        'type' => 'theme_mod',
        'default' => byo_theme_mod_default( 'byo-color-text-link'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'byo-color-text-link',
        array(
        'label' => __( 'Link', 'byo_textdomain' ),
        'section' => 'colors',
    ) ) );

    $wp_customize->add_setting( 'byo-color-text-footer',
        array(
        'type' => 'theme_mod',
        'default' => byo_theme_mod_default( 'byo-color-text-footer'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'byo-color-text-footer',
        array(
        'label' => __( 'Footer Text', 'byo_textdomain' ),
        'section' => 'colors',
    ) ) );

    $wp_customize->add_setting( 'byo-color-text-footer-link',
        array(
        'type' => 'theme_mod',
        'default' => byo_theme_mod_default( 'byo-color-text-footer-link'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'byo-color-text-footer-link',
        array(
        'label' => __( 'Footer Link', 'byo_textdomain' ),
        'section' => 'colors',
    ) ) );

    $wp_customize->add_setting( 'byo-color-text-footer-rollover',
        array(
        'type' => 'theme_mod',
        'default' => byo_theme_mod_default( 'byo-color-text-footer-rollover'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'byo-color-text-footer-rollover',
        array(
        'label' => __( 'Footer Link Rollover', 'byo_textdomain' ),
        'section' => 'colors',
    ) ) );



    $wp_customize->add_setting( 'byo-color-rule-lines',
        array(
        'type' => 'theme_mod',
        'default' => byo_theme_mod_default( 'byo-color-rule-lines'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'byo-color-rule-lines',
        array(
        'label' => __( 'Rule Lines', 'byo_textdomain' ),
        'section' => 'colors',
    ) ) );

    $wp_customize->add_setting( 'byo-color-call-to-action',
        array(
        'type' => 'theme_mod',
        'default' => byo_theme_mod_default( 'byo-color-call-to-action'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'byo-color-call-to-action',
        array(
        'label' => __( 'Call to Action Button', 'byo_textdomain' ),
        'section' => 'colors',
    ) ) );


    $wp_customize->add_setting( 'byo-color-background',
        array(
        'type' => 'theme_mod',
        'default' => byo_theme_mod_default( 'byo-color-background'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'byo-color-background',
        array(
        'label' => __( 'Background', 'byo_textdomain' ),
        'section' => 'colors',
    ) ) );

    /*
    $wp_customize->add_setting( 'byo-xxx',
        array(
        'type' => 'theme_mod',
        'default' => byo_theme_mod_default( 'byo-xxx'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'byo-xxx',
        array(
        'label' => __( 'XXX Color', 'byo_textdomain' ),
        'section' => 'colors',
    ) ) );
    */


    ///////////////////////////////////////////////////////////////////////////
    // Fonts

    $fontListAll = byo_get_google_fonts( 25);
    $fontListBodyText = byo_get_google_fonts( 25, false);

    // section
    $wp_customize->add_section( 'byo-font-setting', array(
            'title'          => 'Fonts',
            'priority'       => 20,
            'description' => '<b>' . __( 'Select fonts.', 'byo' ) .'</b>' ,
        ) );

    // setting/control
    $wp_customize->add_setting( 'byo-font-nav-menu', array(
            'type'    => 'theme_mod',
            'default' => byo_theme_mod_default('byo-font-nav-menu'),
    //            'transport' => 'postMessage',
            'transport' => 'refresh',
        ) );

    $wp_customize->add_control( 'byo-font-nav-menu', array(
            'label' => 'Navigation Menu',
            'section' => 'byo-font-setting',
            'settings'   => 'byo-font-nav-menu',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

    $wp_customize->add_setting( 'byo-font-title', array(
            'type'    => 'theme_mod',
            'default' => byo_theme_mod_default('byo-font-title'),
            //            'transport' => 'postMessage',
                        'transport' => 'refresh',
        ) );

    $wp_customize->add_control( 'byo-font-title', array(
            'label' => 'Title',
            'section' => 'byo-font-setting',
            'settings'   => 'byo-font-title',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

    $wp_customize->add_setting( 'byo-font-heading', array(
            'type'    => 'theme_mod',
            'default' => byo_theme_mod_default('byo-font-heading'),
            //            'transport' => 'postMessage',
                        'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'byo-font-heading', array(
            'label' => 'Heading',
            'section' => 'byo-font-setting',
            'settings'   => 'byo-font-heading',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

    $wp_customize->add_setting( 'byo-font-body-text', array(
            'type'    => 'theme_mod',
            'default' => byo_theme_mod_default('byo-font-body-text'),
            //            'transport' => 'postMessage',
                        'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'byo-font-body-text', array(
            'label' => 'Body Text',
            'section' => 'byo-font-setting',
            'settings'   => 'byo-font-body-text',
            'type'    => 'select',
            'choices' => $fontListBodyText,
    ) );

    $wp_customize->add_setting( 'byo-font-button', array(
            'type'    => 'theme_mod',
            'default' => byo_theme_mod_default('byo-font-button'),
            'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'byo-font-button', array(
            'label' => 'Button',
            'section' => 'byo-font-setting',
            'settings'   => 'byo-font-button',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

    $wp_customize->add_setting( 'byo-font-caption', array(
            'type'    => 'theme_mod',
            'default' => byo_theme_mod_default('byo-font-caption'),
            'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'byo-font-caption', array(
            'label' => 'Caption',
            'section' => 'byo-font-setting',
            'settings'   => 'byo-font-caption',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

    $wp_customize->add_setting( 'byo-font-footer', array(
            'type'    => 'theme_mod',
            'default' => byo_theme_mod_default('byo-font-footer'),
            'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'byo-font-footer', array(
            'label' => 'Footer',
            'section' => 'byo-font-setting',
            'settings'   => 'byo-font-footer',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );


    /*
    $wp_customize->add_setting( 'byo-font-xxx', array(
            'type'    => 'theme_mod',
            'default' => 'arial,helvetica',
            'transport' => 'postMessage',
    ) );

    $wp_customize->add_control( 'byo-font-xxx', array(
            'label' => 'Button Font',
            'section' => 'byo-font-setting',
            'settings'   => 'byo-font-xxx',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

    */


    ///////////////////////////////////////////////////////////////////////////
    // Social Media
    $wp_customize->add_section( 'byo-social-media-settings', array(
        'title'          => 'Social Media Settings',
        'priority'       => 160,
        'description' => '<b>' . __( 'Social media links.' ) .'</b>' ,
    ) );

    $wp_customize->add_setting( 'byo-company-name', array(
            'type'    => 'theme_mod',
            'default' => 'test',
            'transport' => 'postMessage',
            'sanitize_callback' => 'byo_sanitize_text',
        ) );
    $wp_customize->add_control( 'byo-company-name', array(
            'label' => 'Company Name',
            'section' => 'byo-copyright-message-settings',
            'settings'   => 'byo-company-name',
            'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'byo-facebook', array(
        'type'    => 'theme_mod',
        'default' => '',
        'transport' => 'postMessage',
        'sanitize_callback' => 'byo_sanitize_text',
    ) );
    $wp_customize->add_control( 'byo-facebook', array(
        'label' => __( 'Facebook URL', 'loco_textdomain' ),
        'section' => 'byo-social-media-settings',
        'settings'   => 'byo-facebook',
        'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'byo-instagram', array(
        'type'    => 'theme_mod',
        'default' => '',
        'transport' => 'postMessage',
        'sanitize_callback' => 'byo_sanitize_text',
    ) );
    $wp_customize->add_control( 'byo-instagram', array(
        'label' => __( 'Instagram URL', 'loco_textdomain' ),
        'section' => 'byo-social-media-settings',
        'settings'   => 'byo-instagram',
        'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'byo-yelp', array(
        'type'    => 'theme_mod',
        'default' => '',
        'transport' => 'postMessage',
        'sanitize_callback' => 'byo_sanitize_text',
    ) );
    $wp_customize->add_control( 'byo-yelp', array(
        'label' => __( 'Yelp URL', 'loco_textdomain' ),
        'section' => 'byo-social-media-settings',
        'settings'   => 'byo-yelp',
        'type'    => 'text',
    ) );


    ///////////////////////////////////////////////////////////////////////////
    // Footer

    $wp_customize->add_section( 'byo-copyright-message-settings', array(
            'title'          => 'Contact Information',
            'priority'       => 200,
    ) );


    $wp_customize->add_setting( 'byo-address', array(
            'type'    => 'theme_mod',
            'default' => '',
            'transport' => 'postMessage',
            'sanitize_callback' => 'byo_sanitize_text',
        ) );
    $wp_customize->add_control( 'byo-address', array(
            'label' => 'Address',
            'section' => 'byo-copyright-message-settings',
            'settings'   => 'byo-address',
            'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'byo-telephone', array(
            'type'    => 'theme_mod',
            'default' => byo_theme_mod_default('byo-telephone'),
            'transport' => 'postMessage',
            'sanitize_callback' => 'byo_sanitize_text',
    ) );
    $wp_customize->add_control( 'byo-telephone', array(
            'label' => 'Telephone',
            'section' => 'byo-copyright-message-settings',
            'settings'   => 'byo-telephone',
            'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'byo-copyright-message', array(
            'type'    => 'theme_mod',
            'default' => byo_theme_mod_default('byo-copyright-message'),
            'transport' => 'postMessage',
            'sanitize_callback' => 'byo_sanitize_text',
        ) );
    $wp_customize->add_control( 'byo-copyright-message', array(
            'label' => 'Copyright Text',
            'section' => 'byo-copyright-message-settings',
            'settings'   => 'byo-copyright-message',
            'type'    => 'text',
    ) );


}
add_action( 'customize_register', 'byo_customize_register' );


function byo_customize_css()
{
    // add customizer CSS to <head>
    ?>


    <style type="text/css">

    a, a:visited, .site-main a, .site-main a:visited, .site-main a:active, .site-main a:hover, .site-main a:focus {
      /*color: <?php echo byo_get_theme_mod( 'byo-color-text-link', '#aaa' ); ?>;*/
    }

        .navbar-default .navbar-nav > li > a {
          color: <?php echo byo_get_theme_mod( 'byo-color-nav-link', '#fff' ); ?>;
          font-family: <?php echo byo_font_family( byo_get_theme_mod( 'byo-font-nav-menu', 'arial,helvetica')); ?>;
        }
        .navbar-default .navbar-nav > li > a:hover {
            color: <?php echo byo_get_theme_mod( 'byo-color-text-link', '#aaa' ); ?>;
        }


        .navbar-default .navbar-nav .current-menu-item a {
            color: <?php echo byo_get_theme_mod( 'byo-color-nav-link', '#aaa' ); ?>;
        }

        .navbar .navbar-header button .icon-bar {
          /*background-color: <?php echo byo_get_theme_mod( 'byo-color-text-link', '#aaa' ); ?>;*/
        }

        .navbar .navbar-header button .icon-title {
          color: <?php echo byo_get_theme_mod( 'byo-color-text-link', '#aaa' ); ?>;
        }
    /*
        .wp-caption {
          font-family: <?php echo byo_font_family( byo_get_theme_mod( 'byo-font-caption', 'arial,helvetica')); ?>;
        }

        input[type="submit"] {
            font-family: <?php echo byo_font_family( byo_get_theme_mod( 'byo-font-nav-menu', 'arial,helvetica')); ?>;
        }

        body, #main {
            color: <?php echo byo_get_theme_mod( 'byo-color-text-body', '#000' ); ?>;
            font-family: <?php echo byo_font_family( byo_get_theme_mod( 'byo-font-body-text', 'arial,helvetica')); ?>;
        }

        .site-header, .site-main {
          font-family: <?php echo byo_font_family( byo_get_theme_mod( 'byo-font-body-text', 'arial,helvetica')); ?>;
        }

        a, a:visited, .site-main a, .site-main a:visited, .site-main a:active, .site-main a:hover, .site-main a:focus {
          color: <?php echo byo_get_theme_mod( 'byo-color-text-link', '#aaa' ); ?>;
        }

        .page .container .call-to-action {
          background-color: <?php echo byo_get_theme_mod( 'byo-color-call-to-action'); ?>;
        }
        h1, h2, h3, h4, h5, h6 {
            font-family: <?php echo byo_font_family( byo_get_theme_mod( 'byo-font-heading', 'arial,helvetica')); ?>;
        }
        hr {
          border-color: <?php echo byo_get_theme_mod( 'byo-color-rule-lines', '#aaa' ); ?>;
        }
        .button {
            font-family: <?php echo byo_font_family( byo_get_theme_mod( 'byo-font-button', 'arial,helvetica')); ?>;
        }

        .caption {
            font-family: <?php echo byo_font_family( byo_get_theme_mod( 'byo-font-caption', 'arial,helvetica')); ?>;
        }

        .site-footer {
            color: <?php echo byo_get_theme_mod( 'byo-color-text-footer'); ?>;
        }

        .site-footer a, .site-footer a .fa {
          color: <?php echo byo_get_theme_mod( 'byo-color-text-footer-link'); ?>;
        }


        .site-footer a:focus, .site-footer a:hover, .site-footer a:active,
        .site-footer a .fa:focus, .site-footer a .fa:hover, .site-footer a .fa:active {
          color: <?php echo byo_get_theme_mod( 'byo-color-text-footer-rollover'); ?>;
        }

        .site-footer {
            font-family: <?php echo byo_font_family( byo_get_theme_mod( 'byo-font-footer', 'arial,helvetica')); ?>;
        }
        .site-footer h3 {
          font-family: <?php echo byo_font_family( byo_get_theme_mod( 'byo-font-footer-heading', 'arial,helvetica')); ?>;
          font-weight: 400;
        }

        .site-footer .nav > li > a {
          font-family: <?php echo byo_font_family( byo_get_theme_mod( 'byo-font-footer', 'arial,helvetica')); ?>;
        }

*/
    </style>

    <?php

}
add_action( 'wp_head', 'byo_customize_css');


// change editor_style CSS dynamically for the admin editor
function byo_theme_editor_dynamic_styles( $mceInit ) {
  $styles = " \
  body.mce-content-body {  \
    background-color: ".byo_get_theme_mod( 'byo-color-background', '#fff' )."; \
    color:".byo_get_theme_mod( 'byo-color-body-text', '#000' )."; \
    font-family:".byo_font_family( byo_get_theme_mod( 'byo-font-body-text', 'arial,helvetica'))."; \
  } \
  body.mce-content-body h1, h2,  h4,  h6 { \
    font-family: 'Ultra', serif; \
  } \
  body.mce-content-body  h3, h5 { \
    font-family: 'PT Sans', serif; \
  } \
  .wp-caption-dd { \
    font-family:".byo_font_family( byo_get_theme_mod( 'byo-font-caption', 'arial,helvetica'))."; \
    color:".byo_get_theme_mod( 'byo-body-textcolor', '#000' )."; \
  } \
  ";

    if ( isset( $mceInit['content_style'] ) ) {
        $mceInit['content_style'] .= ' ' . $styles . ' ';
    } else {
        $mceInit['content_style'] = $styles . ' ';
    }
    return $mceInit;
}
add_filter('tiny_mce_before_init','byo_theme_editor_dynamic_styles');


/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function byo_customize_preview_js() {
    wp_enqueue_script( 'byo_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '4'.date( '.YmdGi'), true );
}

add_action( 'customize_preview_init', 'byo_customize_preview_js' );
