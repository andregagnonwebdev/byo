<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package byo
 */

if ( ! function_exists( 'byo_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function byo_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		/* translators: %s: post date. */
		esc_html_x( 'Posted on %s', 'post date', 'byo' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$byline = sprintf(
		/* translators: %s: post author. */
		esc_html_x( 'by %s', 'post author', 'byo' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);
	$byline = '';

	echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

}
endif;

if ( ! function_exists( 'byo_custom_type_terms' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function byo_custom_type_terms() {

	global $post;

	if ($post && !empty($post->ID)) {
  	echo get_the_term_list( $post->ID, 'topic', 'Topic(s): ', ', ' , '<br>');
  	echo get_the_term_list( $post->ID, 'beer-style', 'Beer Style(s): ', ', '  , '<br>');
  	echo get_the_term_list( $post->ID, 'recipe-type', 'Recipe Type(s): ', ', '  , '<br>');
  }

	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_terms( esc_html__( ', ', 'byo' ) );
		if ( $categories_list ) {
			/* translators: 1: list of categories. */
			printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'byo' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}
	}
}
endif;

if ( ! function_exists( 'byo_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function byo_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'byo' ) );
		if ( $categories_list ) {
			/* translators: 1: list of categories. */
			printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'byo' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'byo' ) );
		if ( $tags_list ) {
			/* translators: 1: list of tags. */
			printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'byo' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		comments_popup_link(
			sprintf(
				wp_kses(
					/* translators: %s: post title */
					__( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'byo' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			)
		);
		echo '</span>';
	}

	edit_post_link(
		sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Edit <span class="screen-reader-text">%s</span>', 'byo' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		),
		'<span class="edit-link">',
		'</span>'
	);
}
endif;

function byo_restricted_tag_text( $post_id=null, $show_lock=false) {
	$lock_icon =  ( $show_lock || ( byo_is_restricted( $post_id) )) ? ('<span class="lock-content">MEMBERS ONLY</span>') : ('<span class="unlock-content">FREE</span>');

	$s =  ' <span class="restriction">'.$lock_icon.'</span>';
	$s = ( get_post_type( $post_id) == 'product') ? '' : $s;


	// $s =  ' <div class="text-center">'.$lock_icon.'</div>';
	return( $s);
}


function byo_restricted_tag( $post_id=null, $show_lock=false) {
	$lock_icon =  ( $show_lock || byo_is_restricted( $post_id)) ? ('lock') : ('unlock');
	$s =  ' <i class="fa fa-'.$lock_icon.'"></i>';
	return( $s);
}

function byo_eyebrow_image_tag( $ID=null, $before='') {
	$obj = get_post_type_object( get_post_type( $ID) );
	if( $obj):
	?>
		<div class="eyebrow-image">
			<span class="eyebrow"><?php echo $before; ?><?php echo $obj->labels->singular_name; ?></span>
		</div>
	<?php
	endif;

}
