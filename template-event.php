<?php
/**
 * Template Name: Event
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package byo
 */

get_header(); ?>

<div  class="col-xs-12">
	<div  class="row">
		<?php get_template_part( 'template-parts/fragment', 'ad-units-sponsor-newbrew'); ?>
	</div>
</div>

	<div id="primary" class="content-area">

		<main id="main" class="site-main col-xs-12 col-md-9">
			<div class="row">
				<?php 	if ( have_posts() ) : the_post(); ?>
					<div class="col-xs-12">
						<h1><?php the_title() ?></h1>
						<?php the_content(); ?>
					</div>
				<?php endif; ?>
			</div>
		</main><!-- #main -->

		<div class="col-xs-12 col-md-3">
			<div class="ad-units">
				<?php get_template_part( 'template-parts/fragment', 'ad-units-right-rail'); ?>
			</div>
		</div>
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
