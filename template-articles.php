<?php
/**
 * Template Name: Articles
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package byo
 */

$post_type = 'article';
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main col-xs-12">
			<div class="row">
				<div class="col-xs-12">
					<span class="eyebrow">Articles</span>
					<h1>Articles</h1>
				</div>
				<div class="col-xs-12">
  				<?php include(locate_template('searchform-all.php')); ?>
				</div>
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'articles' );

			endwhile; // End of the loop.
			?>
		</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
